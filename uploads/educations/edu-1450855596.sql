-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2015 at 07:42 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `giga`
--

-- --------------------------------------------------------

--
-- Table structure for table `giga_users`
--

CREATE TABLE IF NOT EXISTS `giga_users` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `view_report` int(11) NOT NULL DEFAULT '0' COMMENT '0 for no,1 for yes',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '0 for Inactive,1 for Active',
  `added_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `giga_users`
--

INSERT INTO `giga_users` (`user_id`, `first_name`, `last_name`, `username`, `password`, `view_report`, `status`, `added_date`) VALUES
(1, 'Giga', 'Lab', 'giga.lab', 'e82fbe92a8fe7fa91d80ed73d3afcb4e', 0, 1, '2015-10-20'),
(2, 'Mukesh', 'Kurmi', 'mukesh.kurmi', 'e82fbe92a8fe7fa91d80ed73d3afcb4e', 1, 1, '2015-10-21'),
(3, 'Test1', 'Giga', 'test1.giga', '237ef1688fb8f35c7304644e9984c222', 0, 1, '2015-10-23'),
(4, 'Test3', 'giga1', 'test3.giga', '237ef1688fb8f35c7304644e9984c222', 0, 1, '2015-10-23'),
(5, 'Test2', 'giga2', 'test2.giga', '237ef1688fb8f35c7304644e9984c222', 0, 1, '2015-10-23');

-- --------------------------------------------------------

--
-- Table structure for table `giga_user_login_details`
--

CREATE TABLE IF NOT EXISTS `giga_user_login_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `logged_in_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `giga_user_tracking`
--

CREATE TABLE IF NOT EXISTS `giga_user_tracking` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pages` varchar(255) NOT NULL,
  `hit_count` int(11) NOT NULL,
  `time_spent` time DEFAULT NULL,
  `hit_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `giga_users`
--
ALTER TABLE `giga_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `giga_user_login_details`
--
ALTER TABLE `giga_user_login_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `giga_user_tracking`
--
ALTER TABLE `giga_user_tracking`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `giga_users`
--
ALTER TABLE `giga_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `giga_user_login_details`
--
ALTER TABLE `giga_user_login_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `giga_user_tracking`
--
ALTER TABLE `giga_user_tracking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
