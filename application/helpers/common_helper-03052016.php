<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 10/20/2015
 *
 */
date_default_timezone_set('Asia/Kolkata');
/*
 * load front views
 */
function load_front_view($middle,$arrData = array(), $template = "main"){
    $CI =& get_instance();
    $CI->load->model('events_model');
    $CI->load->model('othervideos_model');
    $arrData['middle'] = "front/".$middle;
    $arrData['event_details']=$CI->events_model->get_home_records();
    $arrData['other_video']=$CI->othervideos_model->get_home_records();
    $CI->load->view("front/layout/".$template,$arrData);
}
/*
 * load admin views
 */

function load_admin_view($middle,$arrData = array(), $template = "main"){
    $CI =& get_instance();
    $arrData['middle'] = "admin/".$middle;
    $CI->load->view("admin/layout/".$template,$arrData);
}

/*
 * load admin login views
 */
function load_login_view($middle,$arrData=array())
{
    $CI =& get_instance();
    $CI->load->view($middle,$arrData);

}
/*
 * message for reset password - forgot password
 */
function reset_password_msg($username,$pwd)
{
    $message="Dear $username,";
    $message.="<br><br>Your password has been reset successfully. Kindly use the new password to access your account.<br><br>New Password : ".$pwd;
    $message.="<br><br>Thanks & Regards,<br>Esurge Team";
    $message.="<br><br><img width='100px' src='http://esurge.in/assets/front/images/logo.png'>";
    return $message;
}
/*
 * message for activate account - Registration
 */
function active_account_msg($username,$url)
{
    $message="Dear ".ucwords($username).",";
    $message.="<br><br>Thank you for registering on Esurge Website.";
    $message.="<br>Please activate your Account by clicking the following link :<br><br>".$url;
    $message.="<br><br>In case the above link does not work, please copy the above URL in your browser and press enter key.";
    $message.="<br><br>Thanks & Regards,<br>Esurge Team";
    $message.="<br><br><img width='100px' src='http://esurge.in/assets/front/images/logo.png'>";
    return $message;
}
/*
 * message for contactus - Contactus Admin
 */
function contactus_msg($name,$email,$msg)
{
    $message="Hello Admin,";
    $message.="<br><br>You have received a new message. Kindly find the details below.";
    $message.="<br><br>Name : ".ucwords($name);
    $message.="<br>Email Id : ".$email;
    $message.="<br>Message : ".$msg;
    $message.="<br><br>Thanks & Regards,<br>Esurge Team";
    $message.="<br><br><img width='100px' src='http://esurge.in/assets/front/images/logo.png'>";
    return $message;
}
/*
 * message for contactus - Contactus User
 */
function contactus_msg_user($name)
{
    $message="Dear ".ucwords($name).",";
    $message.="<br><br>Thank you for the message. Our support team will reply you shortly.";
    $message.="<br><br>Thanks & Regards,<br>Esurge Team";
    $message.="<br><br><img width='100px' src='http://esurge.in/assets/front/images/logo.png'>";
    return $message;
}
/*
 * send email function via smtp
 */
function send_email($to,$subject,$message)
{

    //$user_email="sunpharmakiosk@gmail.com";
//    $password="kiosk123";
//
    $config['protocol'] = 'smtp';
    $config['smtp_host'] = 'SPLLCLOUD.sunpharma.com';
    $config['smtp_port'] = '25';
//    $config['smtp_user'] = $user_email;
//    $config['smtp_pass'] = $password;
    $config['validate'] = 'FALSE';
    $config['mailtype'] = 'html';
    $config['charset'] = 'utf-8';
    $config['wordwrap'] = TRUE;
    $config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard


    $CI =& get_instance();

    $CI->load->model('emailid_model');
    $sender_email=$CI->emailid_model->get_sender_emailid();
    $user_email=$sender_email->email_id;

    $CI->load->library('email', $config);
    $CI->email->initialize($config);

    $CI->email->from($user_email, 'Esurge');
    $CI->email->to($to);
    $CI->email->subject($subject);
    $CI->email->message($message);

    if($CI->email->send())
    {
        return 1;
    }else {
        echo $CI->email->print_debugger();
        exit;
    }
}

function get_scoller_details($main_id,$banner_source)
{
    $CI =& get_instance();
    $CI->load->model('news_model');
    $CI->load->model('journals_model');
    $CI->load->model('images_model');
    $CI->load->model('videos_model');
    if($banner_source=='journals'){
        $journal_details=$CI->journals_model->get_journal_records($main_id);
        $details['id']=$journal_details[0]->journal_id;
        $details['title']=$journal_details[0]->journal_title;
        $details['desc']=$journal_details[0]->journal_desc;
        $details['img_path']=$journal_details[0]->journal_img_path;
        $details['banner_source']='journals';
    }elseif($banner_source=='news'){
        $news_details=$CI->news_model->get_news_records($main_id);
        $details['id']=$news_details[0]->news_id;
        $details['title']=$news_details[0]->news_title;
        $details['desc']=substr($news_details[0]->news_desc,0,250)."...";
        $details['img_path']=$news_details[0]->news_img_path;
        $details['banner_source']='news';
    }elseif($banner_source=='images'){
        $images_details=$CI->images_model->get_image_records($main_id);
        $details['id']=$images_details[0]->img_id;
        $details['title']=$images_details[0]->img_title;
        $details['desc']=$images_details[0]->img_desc;
        $details['img_path']=$images_details[0]->img_path;
        $details['banner_source']='images';
    }elseif($banner_source=='videos'){
        $videos_details=$CI->videos_model->get_video_records($main_id);
        $details['id']=$videos_details[0]->vid_id;
        $details['title']=$videos_details[0]->vid_title;
        $details['desc']=$videos_details[0]->vid_desc;
        $details['img_path']=$videos_details[0]->img_path;
        $details['vid_path']=$videos_details[0]->vid_path;
        $details['banner_source']='videos';
    }
    return $details;
}
/**
 * Paggination
 *
 * Returns Html of paggination
 *
 */
if ( ! function_exists('zebra_paggination'))
{
    function zebra_paggination($total_records,$record_perpage)
    {
        $CI =& get_instance();

        $CI->load->library('zebra_pagination');

        $CI->zebra_pagination->records($total_records);
        $CI->zebra_pagination->records_per_page($record_perpage);
        return $CI->zebra_pagination->render(TRUE);
    }
}

function reciever_emailid()
{
    $CI =& get_instance();

    $CI->load->model('emailid_model');
    $sender_email=$CI->emailid_model->get_reciever_emailid();
    $user_email=$sender_email->email_id;
    return $user_email;
}

function get_total_videos($cat_id)
{
    $CI =& get_instance();

    $CI->load->model('videos_model');
    $total_count=count($CI->videos_model->get_cat_videos($cat_id));
    return $total_count;
}

function is_login_required($menu)
{
    $CI =& get_instance();
    $CI->load->model('menu_model');
    $result=$CI->menu_model->is_login_required($menu);
    if(!empty($result)) {
        return $result->is_login_required;
    }else{
        return false;
    }
}
function access_by($menu)
{
    $CI =& get_instance();
    $CI->load->model('menu_model');
    $result=$CI->menu_model->access_by($menu);
    if(!empty($result)) {
        return $result->access_by;
    }else{
        return false;
    }
}
function get_seo_details($menu)
{
    $CI =& get_instance();
    $CI->load->model('menu_model');
    $result=$CI->menu_model->get_seo_details($menu);
    return $result;
}
function clean($string) {
    $string = strtolower(str_replace(' ', '-', $string)); // Replaces all spaces with hyphens.
    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
function get_active_menu()
{
    $CI =& get_instance();
    $CI->load->model('menu_model');
    $result=$CI->menu_model->get_active_menu();
    return $result;
}
function user_tracking($page_source,$page_title)
{
    $CI =& get_instance();
    $CI->load->model('tracking_model');
    $data = array(
        'user_id' => $CI->session->userdata('user_id'),
        'view_page' => $page_source,
        'page_details' => $page_title,
        'view_date' => date('Y-m-d'),
        'view_time' => date('H:i:s'),
    );
    $result=$CI->tracking_model->insert_view_details($data);
    return $result;
}
function get_news_seo_details($id)
{
    $CI =& get_instance();
    $CI->load->model('news_model');
    $result=$CI->news_model->get_seo_details($id);
    return $result;
}