<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 10/20/2015
 *
 */
date_default_timezone_set('Asia/Kolkata');
/*
 * load front views
 */
function load_front_view($middle,$arrData = array(), $template = "main"){
    $CI =& get_instance();
    $CI->load->model('events_model');
    $arrData['middle'] = "front/".$middle;
    $arrData['event_details']=$CI->events_model->get_home_records();
    $CI->load->view("front/layout/".$template,$arrData);
}
/*
 * load admin views
 */

function load_admin_view($middle,$arrData = array(), $template = "main"){
    $CI =& get_instance();
    $arrData['middle'] = "admin/".$middle;
    $CI->load->view("admin/layout/".$template,$arrData);
}

/*
 * load admin login views
 */
function load_login_view($middle,$arrData=array())
{
    $CI =& get_instance();
    $CI->load->view($middle,$arrData);

}
/*
 * message for reset password - forgot password
 */
function reset_password_msg($pwd)
{
    $message="Hello User,";
    $message.="<br>Your password is reset successfully.New password is given below.<br><br>Password : ".$pwd;
    $message.="<br><br>Thank you.";
    return $message;
}
/*
 * message for activate account - Registration
 */
function active_account_msg($username,$url)
{
    $message="Hello ".$username;
    $message.="<br>Please click on given url to activate your account on eSurge.<br><br>".$url;
    $message.="<br><br>Thank you.";
    return $message;
}
/*
 * message for contactus - Contactus
 */
function contactus_msg($name,$email,$msg)
{
    $message="Hello<br>";
    $message.="<table border=1>";
    $message.="<tr><td>Name</td><td>".$name."</td></tr>";
    $message.="<tr><td>Email Id</td><td>".$email."</td></tr>";
    $message.="<tr><td>Meassage</td><td>".$msg."</td></tr>";
    $message.="</table>";
    $message.="<br><br>Thank you.";
    return $message;
}
/*
 * send email function via smtp
 */
function send_email($to,$subject,$message)
{
    $user_email="sunpharmakiosk@gmail.com";
    $password="kiosk123";

    $config['protocol'] = 'smtp';
    $config['smtp_host'] = 'ssl://smtp.gmail.com';
    $config['smtp_port'] = '465';
    $config['smtp_user'] = $user_email;
    $config['smtp_pass'] = $password;
    $config['mailtype'] = 'html';
    $config['charset'] = 'iso-8859-1';
    $config['wordwrap'] = TRUE;
    $config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard

    $CI =& get_instance();
    $CI->load->library('email', $config);
    $CI->email->initialize($config);

    $CI->email->from($user_email, 'eSurge');
    $CI->email->to($to);
    $CI->email->subject($subject);
    $CI->email->message($message);

    if($CI->email->send())
    {
        return 1;
    }else {
        echo $CI->email->print_debugger();
        exit;
    }
}

function get_scoller_details($main_id,$banner_source)
{
    $CI =& get_instance();
    $CI->load->model('news_model');
    $CI->load->model('journals_model');
    $CI->load->model('images_model');
    $CI->load->model('videos_model');
    if($banner_source=='journals'){
        $journal_details=$CI->journals_model->get_journal_records($main_id);
        $details['id']=$journal_details[0]->journal_id;
        $details['title']=$journal_details[0]->journal_title;
        $details['desc']=$journal_details[0]->journal_desc;
        $details['img_path']=$journal_details[0]->journal_img_path;
    }elseif($banner_source=='news'){
        $news_details=$CI->news_model->get_news_records($main_id);
        $details['id']=$news_details[0]->news_id;
        $details['title']=$news_details[0]->news_title;
        $details['desc']=$news_details[0]->news_desc;
        $details['img_path']=$news_details[0]->news_img_path;
    }elseif($banner_source=='images'){
        $images_details=$CI->images_model->get_image_records($main_id);
        $details['id']=$images_details[0]->img_id;
        $details['title']=$images_details[0]->img_title;
        $details['desc']=$images_details[0]->img_desc;
        $details['img_path']=$images_details[0]->img_path;
    }elseif($banner_source=='videos'){
        $videos_details=$CI->videos_model->get_video_records($main_id);
        $details['id']=$videos_details[0]->vid_id;
        $details['title']=$videos_details[0]->vid_title;
        $details['desc']=$videos_details[0]->vid_desc;
        $details['img_path']=$videos_details[0]->img_path;
    }
    return $details;
}