<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/22/2015
 * Time: 9:56 AM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Uro_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records of events / conference
    function insert_record($data)
    {
        $this->db->insert('surge_uro', $data);
        return $this->db->insert_id();
    }

    // update record of  of events / conference
    function update_record($id, $data)
    {
        $where = array(
            'uro_id' => $id,
        );
        $this->db->where($where);
        $this->db->update('surge_uro', $data);
    }

    //get all records
    function get_all_records()
    {
        $sql = "select * from surge_uro WHERE is_deleted=0 ORDER BY uro_id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get  records
    function get_records($id)
    {
        $sql = "select * from surge_uro WHERE uro_id=".$id;
        $query = $this->db->query($sql);
        return $query->result();

    }
    //get all records
    function all_records()
    {
        $sql = "select * from surge_uro WHERE is_deleted=0 AND status=1 ORDER BY uro_id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get all records
    function all_records_page($start = 0,$limit = 5)
    {
        $sql = "select * from surge_uro WHERE is_deleted=0 AND status=1 ORDER BY uro_id DESC LIMIT $start,$limit";
        $query = $this->db->query($sql);
        return $query->result();
    }
//get category records
    function get_cat_uros($cat_id)
    {
        if($cat_id=='all'){
            $sql = "select * from surge_uro WHERE is_deleted=0 AND status=1 ORDER BY uro_id DESC";
        }else {
            $sql = "select * from surge_uro WHERE is_deleted=0 AND status=1 AND FIND_IN_SET($cat_id,cat_id) ORDER BY uro_id DESC";
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get category records
    function get_cat_uros_page($cat_id,$start = 0,$limit = 5)
    {
        if($cat_id=='all'){
            $sql = "select * from surge_uro WHERE is_deleted=0 AND status=1 ORDER BY uro_id DESC LIMIT $start,$limit";
        }else {
            $sql = "select * from surge_uro WHERE is_deleted=0 AND status=1 AND FIND_IN_SET($cat_id,cat_id) ORDER BY uro_id DESC LIMIT $start,$limit";
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

}