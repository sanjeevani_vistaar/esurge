<?php
/**
 * Login module ... User authorisation and loggedin details
 * User: Mukesh
 * Date: 10/20/2015
 *
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tracking_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    // save loggedin date time
    function insert_loggedin_datetime($data)
    {
        $this->db->insert('surge_login_details', $data);
        return $this->db->insert_id();
    }
    // save page view details
    function insert_view_details($data)
    {
        $this->db->insert('surge_tracking_details', $data);
        return $this->db->insert_id();
    }
    //get login details
    function get_login_details($user_id)
    {
        $sql = "select login_date,login_time from surge_login_details WHERE user_id = $user_id ORDER BY login_date DESC,login_time DESC LIMIT 1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get tracking details
    function get_tracking_details($user_id,$last_login_date)
    {
        $sql = "select count(*) as hit,view_page as source,page_details as title,user_id from surge_tracking_details WHERE user_id = $user_id AND view_date='".$last_login_date."' GROUP BY page_details ORDER BY view_page ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get all tracking details
    function get_all_tracking_details($user_id)
    {
        $sql = "select count(*) as hit,view_page as source,page_details as title,view_date from surge_tracking_details WHERE user_id = $user_id GROUP BY page_details,view_date ORDER BY view_date DESC,view_page ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get user records
    function get_user_details($user_id)
    {
        $sql = "select user_id,first_name,last_name from surge_users WHERE user_id=".$user_id;
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0];
        }
    }

    //get all hit no.
    function get_total_hit($page_source)
    {
        $sql = "select count(*) as hit from surge_tracking_details WHERE view_page='".$page_source."'";
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0]->hit;
        }
    }
    //get all tracking details
    function get_page_tracking_details($page_source)
    {
        $sql = "select count(*) as hit,page_details as title from surge_tracking_details WHERE view_page ='".$page_source."' GROUP BY page_details ORDER BY hit DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }
}