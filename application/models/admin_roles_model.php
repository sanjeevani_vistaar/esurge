<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:37 AM
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_roles_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records
    function insert_role($data)
    {
        $this->db->insert('surge_admin_role', $data);
        return $this->db->insert_id();
    }

    // update registration code
    function update_role($id,$data)
    {
        $where=array(
            'role_id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_admin_role', $data);
    }

    //get all records
    function get_all_records()
    {
        $sql = "select role_id,role from surge_admin_role WHERE status=1 AND is_deleted=0";
        $query = $this->db->query($sql);
        return $query->result();

    }

}