<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:43 AM
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Puzzle_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records of puzzle
    function insert_puzzle($data)
    {
        $this->db->insert('surge_puzzle', $data);
        return $this->db->insert_id();
    }

    // update record of puzzle
    function update_puzzle($id,$data)
    {
        $where=array(
            'id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_puzzle', $data);
    }

    //get all puzzle records
    function get_all_puzzles_admin()
    {
        $sql = "select * from surge_puzzle WHERE is_deleted=0 ORDER BY id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get all puzzle records
    function get_all_puzzles($cat_id,$no)
    {
        $sql = "select * from surge_puzzle WHERE cat_id=".$cat_id." AND is_deleted=0 AND status=1 ORDER BY id DESC LIMIT $no,1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get  puzzle records
    function get_puzzle($id)
    {
        $sql = "select * from surge_puzzle WHERE is_deleted=0 AND id=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get all puzzle records
    function count_all_puzzles($cat_id)
    {
        $sql = "select count(*) as cnt from surge_puzzle WHERE cat_id=".$cat_id." AND is_deleted=0 AND status=1";
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0]->cnt;
        }
    }

    //get  puzzle answer
    function get_answer($id)
    {
        $sql = "select correct_answer as corr_answer from surge_puzzle WHERE is_deleted=0 AND id=".$id;
        $query = $this->db->query($sql);
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0]->corr_answer;
        }
    }
    //save records of puzzle report
    function insert_puzzle_report($data)
    {
        $this->db->insert('surge_puzzle_report', $data);
        return $this->db->insert_id();
    }

    //tracking report
    function get_puzzle_tracking($id)
    {
        $sql = "SELECT u.first_name,u.last_name,u.email_id,u.mobile_no,pr.played_date FROM surge_puzzle p JOIN surge_puzzle_report pr ON p.id=pr.puzzle_id LEFT JOIN surge_users u ON u.user_id=pr.user_id WHERE p.id=".$id;
        $query = $this->db->query($sql);
        $data=$query->result();
        return $data;
    }
    //save records
    function insert_category($data)
    {
        $this->db->insert('surge_puzzle_category', $data);
        return $this->db->insert_id();
    }

    // update record
    function update_category($id,$data)
    {
        $where=array(
            'cat_id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_puzzle_category', $data);
    }

    //get all records
    function get_all_records()
    {
        $sql = "select * from surge_puzzle_category where is_deleted=0 ORDER BY cat_name ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get records
    function get_cat_records($id)
    {
        $sql = "select * from surge_puzzle_category WHERE cat_id=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }

    //check puzzle category already exsists or not
    function check_exsisting_puzzle_category($cat)
    {
        $sql = "select cat_id from surge_puzzle_category WHERE cat_name='".$cat."' AND is_deleted=0";
        $query = $this->db->query($sql);
        return $query->result();
    }
//get all records
    function get_all_cat()
    {
        $sql = "select * from surge_puzzle_category where is_deleted=0 ORDER BY created_on DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get home records
    function get_home_records()
    {
        $sql = "select * from surge_puzzle_category where is_deleted=0 ORDER BY created_on DESC LIMIT 1";
        $query = $this->db->query($sql);
        return $query->result();
    }
}