<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:43 AM
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records of admin user
    function insert_user($data)
    {
        $this->db->insert('surge_users', $data);
        return $this->db->insert_id();
    }

    // update record of  user
    function update_user($id,$data)
    {
        $where=array(
            'user_id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_users', $data);
    }

    // update password of user
    function update_password($email,$data)
    {
        $where=array(
            'email_id'=>$email,
        );
        $this->db->where($where);
        $this->db->update('surge_users', $data);
        return $this->db->affected_rows();
    }
// update status by email verification
    function update_acc_status($id,$token,$data)
    {
        $where=array(
            'user_id'=>$id,
            'token'=>$token,
        );
        $this->db->where($where);
        $this->db->update('surge_users', $data);
        return $this->db->affected_rows();
    }

    //get all users records
    function get_all_users()
    {
        $sql = "select * from surge_users WHERE is_deleted=0 ORDER BY first_name ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get all active users records
    function get_active_users()
    {
        $sql = "select * from surge_users WHERE is_deleted=0 AND status=1 ORDER BY first_name ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get all deactive users records
    function get_deactive_users()
    {
        $sql = "select * from surge_users WHERE is_deleted=0 AND status=0 ORDER BY first_name ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get user records
    function get_user_records($id)
    {
        $sql = "select * from surge_users WHERE user_id=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }

    //check emai id already exsists or not
    function check_exsisting_email($email)
    {
        $sql = "select user_id from surge_users WHERE email_id='".$email."' AND is_deleted=0";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get user status
    function get_user_status($id)
    {
        $sql = "select status from surge_users WHERE user_id=".$id;
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0];
        }
    }
    //get details by email id
    function get_details_by_email($user_email)
    {
        $sql = "select first_name,last_name from surge_users WHERE email_id='".$user_email."' AND is_deleted=0";
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0];
        }
    }

    //get all users records
    function get_all_users_admin($state_id)
    {
        if($state_id) {
            $sql = "select u.*,s.state_name from surge_users u JOIN surge_state s ON u.state_id = s.state_id WHERE u.state_id=$state_id AND u.is_deleted=0 ORDER BY u.first_name ASC";
        }else{
            $sql = "select u.*,s.state_name from surge_users u JOIN surge_state s ON u.state_id = s.state_id WHERE u.is_deleted=0 ORDER BY u.first_name ASC";
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get all users records
    function get_all_users_by_date($to_date,$from_date)
    {
        $sql = "select u.*,s.state_name from surge_users u JOIN surge_state s ON u.state_id = s.state_id WHERE u.created_on>='".$to_date."' AND u.created_on <='".$from_date."' AND u.is_deleted=0 ORDER BY u.first_name ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

}