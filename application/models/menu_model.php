<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:43 AM
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records of admin user
    function insert_menu($data)
    {
        $this->db->insert('surge_menu', $data);
        return $this->db->insert_id();
    }

    // update record of admin user
    function update_menu($id,$data)
    {
        $where=array(
            'id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_menu', $data);
    }

    //get all records
    function get_all_records()
    {
        $sql = "select * from surge_menu WHERE is_deleted=0 AND status=1 ORDER BY menu_title ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get all records
    function get_all_records_admin()
    {
        $sql = "select * from surge_menu WHERE is_deleted=0 ORDER BY menu_title ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get menu records
    function get_menu_records($id)
    {
        $sql = "select * from surge_menu WHERE id=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }

//check menu already exsists or not
    function check_exsisting_menu($menu)
    {
        $sql = "select id from surge_menu WHERE menu_title='".$menu."' AND is_deleted=0";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //check is_login_required or not
    function is_login_required($menu)
    {
        $sql = "select is_login_required from surge_menu WHERE menu_title='".$menu."' AND is_deleted=0";
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0];
        }
    }
    //check is_login_required or not
    function access_by($menu)
    {
        $sql = "select access_by from surge_menu WHERE menu_title='".$menu."' AND is_deleted=0";
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0];
        }
    }
    //get seo details
    function get_seo_details($menu)
    {
        $sql = "select * from surge_menu WHERE menu_title='".$menu."' AND is_deleted=0";
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
            $data=$query->result();
        }

        if(!empty($data)){
            return $data[0];
        }
    }
    //get active menu  details
    function get_active_menu()
    {
        $sql = "select * from surge_menu WHERE status=1 AND is_deleted=0 ORDER BY priority ASC";
        $query = $this->db->query($sql);
        //return $query;
        //return $query->result();
        if($query->num_rows() > 0){
            return $query->result();
        }
    }
    //get all records
    function get_all_records_admin_priority()
    {
        $sql = "select * from surge_menu WHERE is_deleted=0 ORDER BY priority ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    // update record of admin user
    function update_priority($oldpriority,$priority)
    {
        if($oldpriority > $priority)
        {
            $sql = "UPDATE surge_menu SET priority= priority + 1 WHERE priority >" . $priority . " AND priority <" . $oldpriority;
        }else {
            $sql = "UPDATE surge_menu SET priority= priority - 1 WHERE priority <=" . $priority . " AND priority >" . $oldpriority;
        }
        $query = $this->db->query($sql);
        //return $query->result();
    }
    // update record of admin user
    function add_priority($priority)
    {
        $sql = "UPDATE surge_menu SET priority= priority + 1 WHERE priority >" . $priority;
        $query = $this->db->query($sql);
        //return $query->result();
    }

}