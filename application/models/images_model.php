<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:43 AM
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Images_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records
    function insert_image($data)
    {
        $this->db->insert('surge_images', $data);
        return $this->db->insert_id();
    }

    // update record
    function update_image($id,$data)
    {
        $where=array(
            'img_id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_images', $data);
    }

    //get all records
    function get_all_images()
    {
        $sql = "select * from surge_images where is_deleted=0 AND status=1 ORDER BY img_id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get all records
    function get_all_images_page($start = 0,$limit = 5)
    {
        $sql = "select * from surge_images where is_deleted=0 AND status=1 ORDER BY img_id DESC LIMIT $start,$limit";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get all records
    function get_all_images_admin()
    {
        $sql = "select * from surge_images where is_deleted=0 ORDER BY img_id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get all records
    function get_all_images_banner()
    {
        $sql = "select * from surge_images where is_deleted=0 AND is_banner < 1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get records
    function get_image_records($id)
    {
        $sql = "select * from surge_images WHERE img_id=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }
}