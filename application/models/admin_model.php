<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:43 AM
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records of admin user
    function insert_admin($data)
    {
        $this->db->insert('surge_admin', $data);
        return $this->db->insert_id();
    }

    // update record of admin user
    function update_admin($id,$data)
    {
        $where=array(
            'admin_id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_admin', $data);
    }

    // update password of admin user
    function update_password($email,$data)
    {
        $where=array(
            'email_id'=>$email,
        );
        $this->db->where($where);
        $this->db->update('surge_admin', $data);
        return $this->db->affected_rows();
    }

    //get all admin records
    function get_all_admin()
    {
        $sql = "select a.admin_id,a.first_name,a.last_name,a.email_id,a.mobile_no,a.status,r.role from surge_admin a JOIN surge_admin_role r ON r.role_id=a.role_id WHERE a.is_deleted=0 ORDER BY first_name ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get admin records
    function get_admin_records($id)
    {
        $sql = "select admin_id,first_name,last_name,email_id,mobile_no,status,role_id,password from surge_admin WHERE admin_id=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }
    //check emai id already exsists or not
    function check_exsisting_email($email)
    {
        $sql = "select admin_id from surge_admin WHERE email_id='".$email."' AND is_deleted=0";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get details by email id
    function get_details_by_email($admin_email)
    {
        $sql = "select first_name,last_name from surge_admin WHERE email_id='".$admin_email."' AND is_deleted=0";
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0];
        }
    }
}