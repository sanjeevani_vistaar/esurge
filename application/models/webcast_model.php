<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:43 AM
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webcast_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records
    function insert_video($data)
    {
        $this->db->insert('surge_webcast_new', $data);
        return $this->db->insert_id();
    }

    // update record
    function update_video($id,$data)
    {
        $where=array(
            'vid_id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_webcast_new', $data);
    }

    //get all records
    function get_all_videos()
    {
        $sql = "select * from surge_webcast_new where is_deleted=0 AND status=1 ORDER BY vid_id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get all records
    function get_all_videos_page($start = 0,$limit = 5)
    {
        $sql = "select * from surge_webcast_new where is_deleted=0 AND status=1 ORDER BY vid_id DESC LIMIT $start,$limit";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get all records
    function get_all_videos_admin()
    {
        $sql = "select * from surge_webcast_new where is_deleted=0 ORDER BY vid_id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get all records
    function get_all_videos_banner()
    {
        $sql = "select * from surge_webcast_new where is_deleted=0 AND status=1 AND is_banner < 1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get records
    function get_video_records($id)
    {
        $sql = "select * from surge_webcast_new WHERE vid_id=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get category records
    function get_cat_videos($cat_id)
    {
        if($cat_id=='all'){
            $sql = "select * from surge_webcast_new where is_deleted=0 AND status=1 ORDER BY vid_id DESC";
        }else {
            $sql = "select * from surge_webcast_new where FIND_IN_SET($cat_id,cat_id) AND is_deleted=0 AND status=1 ORDER BY vid_id DESC";
        }
            $query = $this->db->query($sql);
        return $query->result();
    }
    //get category records
    function get_cat_videos_page($cat_id,$start = 0,$limit = 5)
    {
        if($cat_id=='all'){
            $sql = "select * from surge_webcast_new where is_deleted=0 AND status=1 ORDER BY vid_id DESC LIMIT $start,$limit";
        }else {
            $sql = "select * from surge_webcast_new where FIND_IN_SET($cat_id,cat_id) AND is_deleted=0 AND status=1 ORDER BY vid_id DESC LIMIT $start,$limit";
        }
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get records
    function get_video_emb_code($vid_id)
    {
        $sql = "select vid_emb_code from surge_webcast_new WHERE vid_id=".$vid_id;
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0];
        }
    }
}