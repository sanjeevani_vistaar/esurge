<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/22/2015
 * Time: 9:56 AM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Education_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records of events / conference
    function insert_record($data)
    {
        $this->db->insert('surge_education', $data);
        return $this->db->insert_id();
    }

    // update record of  of events / conference
    function update_record($id, $data)
    {
        $where = array(
            'edu_id' => $id,
        );
        $this->db->where($where);
        $this->db->update('surge_education', $data);
    }

    //get all records
    function get_all_records()
    {
        $sql = "select * from surge_education WHERE is_deleted=0";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get  records
    function get_records($id)
    {
        $sql = "select * from surge_education WHERE edu_id=".$id;
        $query = $this->db->query($sql);
        return $query->result();

    }

}