<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:43 AM
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class City_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records of admin user
    function insert_city($data)
    {
        $this->db->insert('surge_city', $data);
        return $this->db->insert_id();
    }

    // update record of admin user
    function update_city($id,$data)
    {
        $where=array(
            'city_id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_city', $data);
    }

    //get all records
    function get_all_records()
    {
        $sql = "select * from surge_city WHERE is_deleted=0  ORDER BY city_name ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get city records
    function get_city_records($id)
    {
        $sql = "select * from surge_city WHERE city_id=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }
//get all records by state id
    function get_city($state_id)
    {
        $sql = "select city_id,city_name from surge_city WHERE state_id=".$state_id." AND is_deleted=0  ORDER BY city_name ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get city records
    function get_city_records_byname($name)
    {
        $sql = "select city_id,state_id from surge_city WHERE city_name='".$name."'";
        $query = $this->db->query($sql);
        return $query->result();
    }
}