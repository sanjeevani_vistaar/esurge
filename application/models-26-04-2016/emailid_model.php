<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:43 AM
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emailid_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records of admin user
    function insert_emailid($data)
    {
        $this->db->insert('surge_emailid', $data);
        return $this->db->insert_id();
    }

    // update record of admin user
    function update_emailid($id,$data)
    {
        $where=array(
            'id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_emailid', $data);
    }

    //get all records
    function get_all_records()
    {
        $sql = "select * from surge_emailid WHERE is_deleted=0 ORDER BY email_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get emailid records
    function get_emailid_records($id)
    {
        $sql = "select * from surge_emailid WHERE id=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get sender emailid records
    function get_sender_emailid()
    {
        $sql = "select email_id from surge_emailid WHERE type='Sender'";
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0];
        }
    }
//get reciever emailid records
    function get_reciever_emailid()
    {
        $sql = "select email_id from surge_emailid WHERE type='Reciever'";
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0];
        }
    }


}