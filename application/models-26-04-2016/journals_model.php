<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:43 AM
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Journals_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records
    function insert_journal($data)
    {
        $this->db->insert('surge_journals', $data);
        return $this->db->insert_id();
    }

    // update record
    function update_journal($id,$data)
    {
        $where=array(
            'journal_id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_journals', $data);
    }

    //get all records
    function get_all_journals()
    {
        $sql = "select * from surge_journals where is_deleted=0 AND status=1 ORDER BY journal_id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get all records
    function get_all_journals_page($start = 0,$limit = 5)
    {
        $sql = "select * from surge_journals where is_deleted=0 AND status=1 ORDER BY journal_id DESC LIMIT $start,$limit";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get all records
    function get_all_journals_admin()
    {
        $sql = "select * from surge_journals where is_deleted=0 ORDER BY journal_id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get all records
    function get_all_journals_banner()
    {
        $sql = "select * from surge_journals where is_deleted=0 AND status=1 AND is_banner < 1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get records
    function get_journal_records($id)
    {
        $sql = "select * from surge_journals WHERE journal_id=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }
}