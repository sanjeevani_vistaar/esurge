<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:43 AM
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitecontents_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records
    function insert_term_of_use($data)
    {
        $this->db->insert('surge_term_of_use', $data);
        return $this->db->insert_id();
    }

    // update record
    function update_term_of_use($id,$data)
    {
        $where=array(
            'id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_term_of_use', $data);
    }

    //get all records
    function get_term_of_use()
    {
        $sql = "select * from surge_term_of_use";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //save records
    function insert_privacy_policy($data)
    {
        $this->db->insert('surge_privacy_policy', $data);
        return $this->db->insert_id();
    }

    // update record
    function update_privacy_policy($id,$data)
    {
        $where=array(
            'id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_term_of_use', $data);
    }

    //get all records
    function get_privacy_policy()
    {
        $sql = "select * from surge_privacy_policy";
        $query = $this->db->query($sql);
        return $query->result();
    }

}