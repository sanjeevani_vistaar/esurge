<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:43 AM
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records
    function insert_category($data)
    {
        $this->db->insert('surge_gallery_category', $data);
        return $this->db->insert_id();
    }

    // update record
    function update_category($id,$data)
    {
        $where=array(
            'cat_id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_gallery_category', $data);
    }

    //get all records
    function get_all_records()
    {
        $sql = "select * from surge_gallery_category where is_deleted=0 ORDER BY cat_name ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get records
    function get_cat_records($id)
    {
        $sql = "select * from surge_gallery_category WHERE cat_id=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }
//get all uro records
    function get_uros_records()
    {
        $sql = "select * from surge_uro_categories WHERE is_deleted=0 ORDER BY cat_name ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //save records
    function insert_uro_category($data)
    {
        $this->db->insert('surge_uro_categories', $data);
        return $this->db->insert_id();
    }

    // update record
    function update_uro_category($id,$data)
    {
        $where=array(
            'cat_id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_uro_categories', $data);
    }


    //get records
    function get_cat_uro_records($id)
    {
        $sql = "select * from surge_uro_categories WHERE cat_id=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }
    //check gallery category already exsists or not
    function check_exsisting_gallery_category($cat)
    {
        $sql = "select cat_id from surge_gallery_category WHERE cat_name='".$cat."' AND is_deleted=0";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //check uro category already exsists or not
    function check_exsisting_uro_category($cat)
    {
        $sql = "select cat_id from surge_uro_categories WHERE cat_name='".$cat."' AND is_deleted=0";
        $query = $this->db->query($sql);
        return $query->result();
    }
}