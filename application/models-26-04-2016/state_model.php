<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:43 AM
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class State_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records of admin user
    function insert_state($data)
    {
        $this->db->insert('surge_state', $data);
        return $this->db->insert_id();
    }

    // update record of admin user
    function update_state($id,$data)
    {
        $where=array(
            'state_id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_state', $data);
    }

    //get all records
    function get_all_records()
    {
        $sql = "select * from surge_state WHERE is_deleted=0 ORDER BY state_name ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get state records
    function get_state_records($id)
    {
        $sql = "select * from surge_state WHERE state_id=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }


}