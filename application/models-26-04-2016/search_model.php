<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/22/2015
 * Time: 9:56 AM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    //get search records
    function search_records($table_name,$tags)
    {
        //$sql = "select * from surge_".$table_name." WHERE is_deleted=0 AND status=1 AND FIND_IN_SET('$tags',tags)";
        $sql = "select * from surge_".$table_name." WHERE is_deleted=0 AND status=1 AND tags LIKE '%$tags%'";
        $query = $this->db->query($sql);
        return $query->result();
    }
}