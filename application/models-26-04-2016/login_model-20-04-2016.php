<?php
/**
 * Login module ... User authorisation and loggedin details
 * User: Mukesh
 * Date: 10/20/2015
 *
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //Authenticate user
    function get_user($usr, $pwd)
    {
        $sql = "select user_id,first_name,last_name,email_id,user_type from surge_users where email_id = '" . $usr . "' and password = '" . md5(sha1($pwd)) . "' and status = 1";
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0];
        }
    }

    //Authenticate Admin
    function get_admin_user($usr, $pwd)
    {
        $sql = "select a.admin_id,a.first_name,a.last_name,a.role_id,r.role from surge_admin a JOIN surge_admin_role r ON r.role_id=a.role_id where a.email_id = '" . $usr . "' and a.password = '" . md5(sha1($pwd)) . "' and a.status = 1";
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0];
        }
    }

}