<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/28/2015
 * Time: 9:52 AM
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records of banner
    function insert_record($data)
    {
        $this->db->insert('surge_banner', $data);
        return $this->db->insert_id();
    }

    // update record of  banner
    function update_record($id, $data)
    {
        $where = array(
            'banner_id' => $id,
        );
        $this->db->where($where);
        $this->db->update('surge_banner', $data);
    }
    // update record of  banner from
    function update_record_from($from, $data)
    {
        $where = array(
            'banner_from' => $from,
        );
        $this->db->where($where);
        $this->db->update('surge_banner', $data);
    }
    //get all records
    function get_all_banners()
    {
        $sql = "select * from surge_banner where is_deleted=0 AND status=1 ORDER BY banner_id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get all records
    function get_all_banners_admin()
    {
        $sql = "select * from surge_banner where is_deleted=0 ORDER BY banner_id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get banner status
    function get_banner_details($id)
    {
        $sql = "select status,main_id,banner_from from surge_banner WHERE banner_id=".$id;
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0];
        }
    }
}