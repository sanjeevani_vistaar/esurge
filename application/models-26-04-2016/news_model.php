<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:43 AM
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records
    function insert_news($data)
    {
        $this->db->insert('surge_news', $data);
        return $this->db->insert_id();
    }

    // update record
    function update_news($id,$data)
    {
        $where=array(
            'news_id'=>$id,
        );
        $this->db->where($where);
        $this->db->update('surge_news', $data);
    }

    //get all records
    function get_all_news()
    {
        $sql = "select * from surge_news where is_deleted=0 AND status=1 ORDER BY news_id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get all records
    function get_all_news_page($start = 0,$limit = 5)
    {
        $sql = "select * from surge_news where is_deleted=0 AND status=1 ORDER BY news_id DESC LIMIT $start,$limit";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get all records
    function get_all_news_banner()
    {
        $sql = "select * from surge_news where is_deleted=0  AND status=1 AND is_banner < 1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get records
    function get_news_records($id)
    {
        $sql = "select * from surge_news WHERE news_id=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get 3 records for home page
    function get_home_news()
    {
        $sql = "select * from surge_news where is_deleted=0 AND status=1 ORDER BY news_id DESC LIMIT 3";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get all records
    function get_all_news_admin()
    {
        $sql = "select * from surge_news where is_deleted=0 ORDER BY news_id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }
}