<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/14/2015
 * Time: 5:46 PM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrationcode_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records
    function insert_registration_code($data)
    {
        $this->db->insert('surge_registration_code', $data);
        return $this->db->insert_id();
    }

    // update registration code
    function update_registration_code($id,$data)
    {
        $where=array(
            'registration_code_id'=>$id,
            );
        $this->db->where($where);
        $this->db->update('surge_registration_code', $data);
    }

    //get all regisration_codes
    function get_all_regisration_codes()
    {
        $sql = "select full_name,email_id,mobile_no,registration_code,status from surge_registration_code ORDER BY registration_code_id DESC";
        $query = $this->db->query($sql);
        return $query->result();

    }

    //verify registration code
    function verify_code($email,$mobile,$code)
    {
        $sql = "select registration_code_id from surge_registration_code where email_id = '" . $email . "' and mobile_no = '" . $mobile . "' and registration_code='".$code."'";
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0];
        }
    }

    // update email id and mobile no for code
    function update_user_details($registration_code,$data)
    {
        $where=array(
            'registration_code'=>$registration_code,
        );
        $this->db->where($where);
        $this->db->update('surge_registration_code', $data);
    }


}