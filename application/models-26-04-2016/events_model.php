<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/22/2015
 * Time: 9:56 AM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Events_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //save records of events / conference
    function insert_record($data)
    {
        $this->db->insert('surge_events', $data);
        return $this->db->insert_id();
    }

    // update record of  of events / conference
    function update_record($id, $data)
    {
        $where = array(
            'event_id' => $id,
        );
        $this->db->where($where);
        $this->db->update('surge_events', $data);
    }

    //get all records
    function get_all_records()
    {
        $sql = "select event_id,title,start,end,view,allDay,location,link from surge_events WHERE is_deleted=0 ORDER BY start ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get all records
    function get_all_records_page($start = 0,$limit = 5)
    {
        $sql = "select event_id,title,start,end,view,allDay,location,link from surge_events WHERE start >='".date('Y-m-d 00:00:00')."' AND is_deleted=0 ORDER BY start ASC LIMIT $start,$limit";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get 3 records for home
    function get_home_records()
    {
        $sql = "select event_id,title,start,end,view,allDay,location,link from surge_events WHERE start >='".date('Y-m-d 00:00:00')."' AND is_deleted=0 ORDER BY start ASC LIMIT 3";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //get event records
    function get_event_records($id)
    {
        $sql = "select event_id,title,start,end,view,allDay,location,link from surge_events WHERE event_id=".$id;
        $query = $this->db->query($sql);
        $data=$query->result();
        if(!empty($data)){
            return $data[0];
        }
    }
    //get all records for year and month
    function get_all_records_year_month($year,$month)
    {
        $sql = "select event_id,title,start,end,view,allDay,location,link from surge_events WHERE start >='".date('Y-m-d 00:00:00')."' AND YEAR(start) =".$year." AND MONTH(start) =".$month." AND is_deleted=0 ORDER BY start ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get all records for year and month
    function get_all_records_year_month_page($year,$month,$start,$limit)
    {
        $sql = "select event_id,title,start,end,view,allDay,location,link from surge_events WHERE start >='".date('Y-m-d 00:00:00')."' AND YEAR(start) =".$year." AND MONTH(start) =".$month." AND is_deleted=0 ORDER BY start ASC LIMIT $start,$limit";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get all records for year and month
    function get_all_records_year_month_filter($year,$month)
    {
        $sql = "select event_id,title,start,end,view,allDay,location,link from surge_events WHERE YEAR(start) =".$year." AND MONTH(start) =".$month." AND is_deleted=0 ORDER BY start ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //get all records for year and month
    function get_all_records_year_month_page_filter($year,$month,$start,$limit)
    {
        $sql = "select event_id,title,start,end,view,allDay,location,link from surge_events WHERE YEAR(start) =".$year." AND MONTH(start) =".$month." AND is_deleted=0 ORDER BY start ASC LIMIT $start,$limit";
        $query = $this->db->query($sql);
        return $query->result();
    }

}