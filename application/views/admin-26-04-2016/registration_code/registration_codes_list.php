<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/14/2015
 * Time: 6:01 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="col-md-12">
<h4>Registration Codes List</h4>
<div class="pull-right"><a href="<?php echo base_url('index.php/admin/registration_code/gererate_registration_code');?>">Generate New Code</a> </div>
    <?php if($this->session->flashdata('succ_msg')){?>
        <div class="alert alert-success text-center">
            <?php echo $this->session->flashdata('succ_msg'); ?>
        </div>
    <?php }?>
<table id="records_pagination" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>

    <tr class="active">
        <th> Fullname</th>
        <th> Email id </th>
        <th> Mobile No </th>
        <th> Registration Code </th>
        <th> Status </th>
    </tr>
    </thead>
    <tbody>
    <?php if(!empty($arrData)){
        foreach($arrData as $row)
        {
            ?>
            <tr>
                <td align="center"><?php echo $row->full_name;?></td>
                <td align="center"><?php echo $row->email_id;?></td>
                <td align="center"><?php echo $row->mobile_no;?></td>
                <td align="center"><?php echo $row->registration_code;?></td>
                <td align="center"><?php if($row->status==1){echo "Active";}else{echo "Deactive";}?></td>
            </tr>
            <?php
        }
    }
    else{?>
        <tr><th colspan="4"> No Records Found</th></tr>
    <?php }?>
    </tbody>
</table>
</div>