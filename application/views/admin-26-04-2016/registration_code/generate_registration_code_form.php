<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/14/2015
 * Time: 4:30 PM
 */
?>
<div class="col-md-6">
    <form action="<?php echo base_url('index.php/admin/registration_code/gererate_registration_code');?>" method="post">
        <h4> Generate Registration Code</h4>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="text" placeholder="Full Name" class="form-control" name="fullname" required>
        </div>
        <br>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            <input type="email" placeholder="Email" class="form-control" name="email" required>
        </div>
        <br>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
            <input type="mobile" placeholder="Mobile" class="form-control" name="mobile" required>
        </div>
        <br>
        <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Generate</button>
    </form>
</div>