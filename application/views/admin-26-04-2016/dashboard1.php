<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/14/2015
 * Time: 4:12 PM
 */?>
<br>
<div class="col-lg-3 col-xs-6">
<!-- small box -->
    <div class="small-box bg-yellow">
        <div class="inner">
            <h3><?php echo count($all_users);?></h3>
            <p>Doctors Registered</p>
        </div>
        <div class="icon">
            <i class="ion ion-person-add"></i>
        </div>
        <a class="small-box-footer" href="<?php echo base_url('admin/front_users/all_list');?>" >More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green">
        <div class="inner">
            <h3><?php echo count($all_videos);?></h3>
            <p><?php if(count($all_videos) > 1) echo "Videos";else echo "Video";?></p>
        </div>
        <div class="icon">
            <i class="ion ion-ios7-pricetag-outline"></i>
        </div>
        <a class="small-box-footer" href="<?php echo base_url('admin/gallery/videos_list');?>" >More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-red">
        <div class="inner">
            <h3><?php echo count($all_pg_icon);?></h3>
            <p><?php if(count($all_pg_icon) > 1) echo "PG Videos";else echo "PG Video";?></p>
        </div>
        <div class="icon">
            <i class="ion ion-ios7-pricetag-outline"></i>
        </div>
        <a class="small-box-footer" href="<?php echo base_url('admin/pg_icon/videos_list');?>" >More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-aqua">
        <div class="inner">
            <h3><?php echo count($all_uro);?></h3>
            <p><?php if(count($all_uro) > 1) echo "Uro Slides";else echo "Uro Slide";?></p>
        </div>
        <div class="icon">
            <i class="ion ion-ios7-pricetag-outline"></i>
        </div>
        <a class="small-box-footer" href="<?php echo base_url('admin/uro/all_list');?>" >More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-purple">
        <div class="inner">
            <h3><?php echo count($all_journals);?></h3>
            <p><?php if(count($all_journals) > 1) echo "Journals";else echo "Journal";?></p>
        </div>
        <div class="icon">
            <i class="ion ion-ios7-pricetag-outline"></i>
        </div>
        <a class="small-box-footer" href="<?php echo base_url('admin/journals/all_list');?>" >More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>