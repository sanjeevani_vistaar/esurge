<!DOCTYPE html>
<html class="bg-black">
<head>
    <meta charset="UTF-8">
    <title>eSurge | Forgot password</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="<?php echo base_url('assets/common/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="<?php echo base_url('assets/admin/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url('assets/admin/css/AdminLTE.css');?>" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url('assets/common/js/html5shiv.min.js');?>"></script>
    <script src="<?php echo base_url('assets/common/js/respond.min.js');?>"></script>
    <![endif]-->
</head>
<body class="bg-black">

<div class="form-box" id="login-box">
    <div class="header">Forgot Password</div>
    <form action="<?php echo base_url('index.php/admin/login/forgot_password');?>" method="post">
        <div class="body bg-gray">
            <?php if($this->session->flashdata('err_msg')){?>
            <div class="alert alert-danger text-center">
             <?php echo $this->session->flashdata('err_msg'); ?>
            </div>
            <?php }?>
<?php if($this->session->flashdata('succ_msg')){?>
            <div class="alert alert-success text-center">
             <?php echo $this->session->flashdata('succ_msg'); ?>
            </div>
            <?php }?>
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email Id" required/>
            </div>

        </div>
        <div class="footer">
            <button type="submit" name="submit" value="Login" class="btn bg-olive btn-block">Submit</button>


        </div>
    </form>


</div>


<!-- jQuery 2.0.2 -->
<script src="<?php echo base_url('assets/common/js/jquery.min.js');?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/common/js/bootstrap.min.js');?>" type="text/javascript"></script>

</body>
</html>