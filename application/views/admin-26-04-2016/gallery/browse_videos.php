<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 2/4/2016
 * Time: 10:43 AM
 */
?>
<div style="height:500px;overflow-x:scroll;width:100%;">
    <?php
    if(!empty($videos_list)){
        arsort($videos_list);
        foreach($videos_list as $key => $file):
            ?>
            <div style="float:left;width:26%; margin:10px 5px;" class="div_video">
                <video width="100%" src="<?php echo base_url('/uploads/gallery/videos/'.$key);?>" controls></video>
                <a href="javascript:void(0);" class="video_name" onclick='$.FileSourcePath="<?php echo '/uploads/tmp/'.$key;?>";$.OnSelectFile(this)'><?php echo $key;?></a>
            </div>
        <?php endforeach;?>

        <div style="clear:both;"></div>
    <?php }else
    {
        echo "<font color='red'>No Videos Found</font>";
    } ?>
</div>
<!--<a href="javascript:void(0);" onclick='$.OnSelectedFile()' class="pull-right">OK</a>-->
