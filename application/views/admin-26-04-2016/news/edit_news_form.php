<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:08 AM
 */
?>
<link href="<?php echo base_url('/assets/tiny_mce/themes/advanced/skins/default/ui.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/tiny_mce/plugins/inlinepopups/skins/clearlooks2/window.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/tiny_mce/themes/advanced/skins/o2k7/ui.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/tiny_mce/themes/advanced/skins/o2k7/ui_silver.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/tiny_mce/themes/advanced/skins/o2k7/ui_black.css') ?>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url('/assets/tiny_mce/tiny_mce.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/admin/js/tinymce.js') ?>"></script>
<div class="col-md-8">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="box-title">Update News</h4>
        </div><!-- /.box-header -->
        <!-- form start -->
        <?php if($this->session->flashdata('err_msg')){?>
            <div class="alert alert-danger text-center">
                <?php echo $this->session->flashdata('err_msg'); ?>
            </div>
        <?php }?>
        <form ng-app="" role="form" name="myForm" action="<?php echo base_url('admin/news/edit_news/'.$news_details[0]->news_id);?>" method="post" enctype="multipart/form-data">
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Title</label>
                    <input ng-model="title" type="text" placeholder="Enter Title" name="title" class="form-control" required value="<?php echo $news_details[0]->news_title;?>">
                </div>
                <div class="form-group">
                    <label>Short Description</label>
                    <textarea ng-model="desc" placeholder="Enter Short Description" rows="3" class="form-control" name="short_desc" required><?php echo $news_details[0]->short_desc;?></textarea>
                </div>
                <div class="form-group">
                    <label>Brief Description</label>
                    <textarea ng-model="desc" placeholder="Enter Brief Description" rows="3" class="form-control tiny_mce" name="desc"><?php echo $news_details[0]->news_desc;?></textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">Current Image</label>
                    <img src="<?php echo base_url($news_details[0]->news_img_path);?>">

                </div>
                <div class="form-group">
                    <label for="exampleInputFile">Select Image</label>
                    <input type="file" name="img_path" id="exampleInputFile">
                    <p style="color: #990000">Only .jpg | .jpeg | .png file & Image dimension : 284 * 148 allowed</p>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tags</label>
                    <input ng-model="link" type="text" placeholder="Enter Tags" name="tags" class="form-control" value="<?php echo $news_details[0]->tags;?>">
                </div>

                <div class="form-group">
                    <label>Status</label>
                    <div class="radio">
                        <label class="">
                            <div class="iradio_minimal checked" style="position: relative;" aria-checked="false" aria-disabled="false"><input type="radio" <?php if($news_details[0]->status==1)echo "checked";?> value="1" id="optionsRadios1" name="status" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                            Active
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <div class="iradio_minimal" style="position: relative;" aria-checked="false" aria-disabled="false"><input type="radio" <?php if($news_details[0]->status==0) echo "checked";?> value="0" id="optionsRadios2" name="status" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                            Deactive
                        </label>
                    </div>

                </div>

            </div><!-- /.box-body -->

            <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Update</button>
        </form>
    </div>
</div>