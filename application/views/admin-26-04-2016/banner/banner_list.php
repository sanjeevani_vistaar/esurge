<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/28/2015
 * Time: 11:53 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<div class="col-md-12">
    <h4>Banners List</h4>
    <?php if(count($arrData) < CONTENTS_SCROLLER_LIMIT){?>
    <div class="pull-right"><a href="<?php echo base_url('admin/banner/source_links');?>">Add New Banner</a> </div>
    <?php } ?>
    <?php if(!empty($this->session->flashdata('succ_msg'))){?>
        <div class="alert alert-success text-center">
            <?php echo $this->session->flashdata('succ_msg'); ?>
        </div>
    <?php }?>
    <?php if(!empty($this->session->flashdata('err_msg'))){?>
        <div class="alert alert-danger text-center">
            <?php echo $this->session->flashdata('err_msg'); ?>
        </div>
    <?php }?>
    <table id="records_pagination" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>

        <tr class="active">
            <th> Banner From </th>
            <th> View Details </th>
            <th> Status </th>
            <th> Action </th>

        </tr>
        </thead>
        <tbody>
        <?php if(!empty($arrData)){
            foreach($arrData as $row)
            {
                ?>
                <tr>
                    <td align="center"><?php echo ucwords($row->banner_from);?></td>
                    <td align="center">
                        <?php if($row->banner_from=='journals'){?>
                            <a href='javascript:void(0)' onclick='$.bannerViewLink="<?php echo base_url('admin/banner/journal_details/'.$row->main_id);?>";$.OnBannerViewDialog()'>View</a>
                        <?php }if($row->banner_from=='news'){?>
                            <a href='javascript:void(0)' onclick='$.bannerViewLink="<?php echo base_url('admin/banner/news_details/'.$row->main_id);?>";$.OnBannerViewDialog()'>View</a>
                        <?php }if($row->banner_from=='images'){?>
                            <a href='javascript:void(0)' onclick='$.bannerViewLink="<?php echo base_url('admin/banner/image_details/'.$row->main_id);?>";$.OnBannerViewDialog()'>View</a>
                        <?php }if($row->banner_from=='videos'){?>
                            <a href='javascript:void(0)' onclick='$.bannerViewLink="<?php echo base_url('admin/banner/video_details/'.$row->main_id);?>";$.OnBannerViewDialog()'>View</a>
                        <?php } ?>
                    </td>
                    <td align="center"><?php if($row->status==1){echo "Active";}else{echo "Deactive";}?></td>
                    <td align="center"><a href='javascript:void(0)' onclick='$.statusChangeLink="<?php echo base_url('admin/banner/change_status/'.$row->banner_id);?>";$.OnStatuschangeDialog()'>Change Status</a> | <a href='javascript:void(0)' onclick='$.deleteLink="<?php echo base_url('admin/banner/delete_banner/'.$row->banner_id);?>";$.OnDeleteDialog()'><i class="fa fa-trash-o"></i></a></td>

                </tr>
                <?php
            }
        }
        else{?>
            <tr><th colspan="4"> No Records Found</th></tr>
        <?php }?>
        </tbody>
    </table>
</div>