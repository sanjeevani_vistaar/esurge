<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/28/2015
 * Time: 1:09 PM
 */
if(!empty($journal_details)){
?>
<div class="box-body">
    <div class="form-group">
        <label for="exampleInputEmail1">Title</label>
        <input ng-model="title" type="text" readonly  name="title" class="form-control" required value="<?php echo $journal_details[0]->journal_title;?>">
    </div>
    <div class="form-group">
        <label>Description</label>
        <textarea ng-model="desc" readonly rows="3" class="form-control" name="desc" required><?php echo $journal_details[0]->journal_desc;?></textarea>
    </div>
    <div class="form-group">
        <label for="exampleInputFile">Image</label>
        <img src="<?php echo base_url($journal_details[0]->journal_img_path);?>">
    </div>
</div>
<?php }
if(!empty($news_details)){
    ?>
    <div class="box-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Title</label>
            <input ng-model="title" type="text" readonly  name="title" class="form-control" required value="<?php echo $news_details[0]->news_title;?>">
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea ng-model="desc" readonly rows="3" class="form-control" name="desc" required><?php echo $news_details[0]->news_desc;?></textarea>
        </div>
        <div class="form-group">
            <label for="exampleInputFile">Image</label>
            <img src="<?php echo base_url($news_details[0]->news_img_path);?>">
        </div>
    </div>
<?php }
if(!empty($image_details)){
    ?>
    <div class="box-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Title</label>
            <input ng-model="title" type="text" readonly  name="title" class="form-control" required value="<?php echo $image_details[0]->img_title;?>">
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea ng-model="desc" readonly rows="3" class="form-control" name="desc" required><?php echo $image_details[0]->img_desc;?></textarea>
        </div>
        <div class="form-group">
            <label for="exampleInputFile">Image</label>
            <img src="<?php echo base_url($image_details[0]->img_path);?>">
        </div>
    </div>
<?php }
if(!empty($video_details)){
    ?>
    <div class="box-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Title</label>
            <input ng-model="title" type="text" readonly  name="title" class="form-control" required value="<?php echo $video_details[0]->vid_title;?>">
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea ng-model="desc" readonly rows="3" class="form-control" name="desc" required><?php echo $video_details[0]->vid_desc;?></textarea>
        </div>
        <div class="form-group">
            <label for="exampleInputFile">Image</label>
            <img src="<?php echo base_url($video_details[0]->img_path);?>">
        </div>
        <div class="form-group">
            <label for="exampleInputFile">Video</label>
            <video width="35%" src="<?php echo base_url($video_details[0]->vid_path);?>" controls></video>
        </div>
    </div>
<?php }