<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 2/4/2016
 * Time: 10:43 AM
 */
?>
<div style="height:500px;overflow-x:scroll;width:100%;">
    <?php
    if(!empty($videos_list)){
        arsort($videos_list);
        foreach($videos_list as $key => $file):
            ?>
            <div style="float:left;width:26%; margin:10px 5px;" class="div_video">
                <a href="javascript:void(0);" style="word-wrap: break-word;" onclick='$.FileSourcePath="<?php echo '/uploads/tmp/videos/'.$key;?>";$.OnSelectFile(this)'>
                <video width="100%" src="<?php echo base_url('/uploads/tmp/videos/'.$key);?>" controls></video>
                <?php echo $key;?></a>
            </div>
        <?php endforeach;?>

        <div style="clear:both;"></div>
    <?php }else
    {
        echo "<br><br><font color='red'>Please login to FTP and upload the files on given path : </font>";
        echo "<br><font color='red'>esurge->uploads->tmp->videos</font>";
    } ?>
</div>
<!--<a href="javascript:void(0);" onclick='$.OnSelectedFile()' class="pull-right">OK</a>-->
