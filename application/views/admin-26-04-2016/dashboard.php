<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/14/2015
 * Time: 4:12 PM
 */?>
<br>
<div class="col-lg-3 col-xs-6">
<!-- small box -->
    <div class="small-box bg-yellow">
        <div class="inner">
            <h3><?php echo count($all_users);?></h3>
            <p> User Registrations</p>
        </div>
        <div class="icon">
            <i class="ion ion-person-add"></i>
        </div>
        <a class="small-box-footer" href="<?php echo base_url('admin/front_users/all_list');?>" >More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green">
        <div class="inner">
            <h3><?php echo count($all_active_users);?></h3>
            <p><?php if(count($all_active_users) > 1) echo "Active Users";else echo "Active User";?></p>
        </div>
        <div class="icon">
            <i class="ion ion-person-add"></i>
        </div>
        <a class="small-box-footer" href="<?php echo base_url('admin/front_users/all_list');?>" >More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-red">
        <div class="inner">
            <h3><?php echo count($all_deactive_users);?></h3>
            <p><?php if(count($all_deactive_users) > 1) echo "Deactive Users";else echo "Deactive User";?></p>
        </div>
        <div class="icon">
            <i class="ion ion-person-add"></i>
        </div>
        <a class="small-box-footer" href="<?php echo base_url('admin/front_users/all_list');?>" >More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-aqua">
        <div class="inner">
            <h3><?php echo count($all_admin_users);?></h3>
            <p>Admin Registrations</p>
        </div>
        <div class="icon">
            <i class="ion ion-person-add"></i>
        </div>
        <a class="small-box-footer" href="<?php echo base_url('admin/admin_users/all_list');?>" >More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>