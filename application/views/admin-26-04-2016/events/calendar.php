<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-body no-padding">
            <!-- THE CALENDAR -->
            <div id="calendar"></div>
        </div>
    </div>
</div>

<!-- Page specific script -->
<script type="text/javascript">
    $(function() {

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear();
        $('#calendar').fullCalendar({
            //defaultView: 'agendaWeek',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            buttonText: {//This is to add icons to the visible buttons
                prev: "<span class='fa fa-caret-left'></span>",
                next: "<span class='fa fa-caret-right'></span>",
                today: 'today',
                month: 'month',
                week: 'week',
                day: 'day'
            },
            events: "<?php echo base_url('admin/calendar/all_events');?>",
            eventRender: function(event, element, view) {
                if (event.allDay === 'true') {
                    event.allDay = true;
                } else {
                    event.allDay = false;
                }
            },
            eventClick: function(event) {
                // opens events in a popup window
//                window.open('https://calendar.google.com/calendar/', 'gcalevent', 'width=700,height=600');
//                return false;
                $.ajax({
                    url: "<?php echo base_url('admin/calendar/event_desc');?>",
                    data: 'event_id='+ event.event_id,
                    type: "POST",
                    success: function(data) {
                        BootstrapDialog.show({
                            title:"Event Description",
                            message: data,
//                            onhide: function(){
//                                   return false;
//                            },
                        });

                    }
                });
            },
            editable: false,
            selectable: true,
            select: function(start, end, allDay) {
                var check = $.fullCalendar.formatDate(start,'yyyy-MM-dd');
                var today = $.fullCalendar.formatDate(new Date(),'yyyy-MM-dd');
                if(check < today)
                {
                    alert('You can not select past date');
                }
                else
                {
                endtime = $.fullCalendar.formatDate(end, "yyyy-MM-dd HH:mm:ss");
                starttime = $.fullCalendar.formatDate(start, "yyyy-MM-dd HH:mm:ss");
                $.ajax({
                    url: "<?php echo base_url('admin/calendar/add_event');?>",
                    data: 'start=' + starttime + '&end=' + endtime,
                    type: "POST",
                    success: function (data) {
                        BootstrapDialog.show({
                            title: "Add Event",
                            message: data,
//                            onhide: function(){
//                                   return false;
//                            },
                        });

                    }
                });
            }
            },
        });
    });
</script>
