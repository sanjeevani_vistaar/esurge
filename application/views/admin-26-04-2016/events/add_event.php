<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/22/2015
 * Time: 11:13 AM
 */
?>
<form role="form" action="<?php echo base_url('admin/calendar/add_event');?>" method="post">
    <div class="box-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Start</label>
            <input ng-model="start" type="text"  name="start" class="form-control" readonly value="<?php echo $start;?>">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">End</label>
            <input ng-model="end" type="text"  name="end" class="form-control" readonly value="<?php echo $end;?>">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Title</label>
            <input ng-model="title" type="text" placeholder="Enter Title" name="title" class="form-control" required>
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea ng-model="desc" placeholder="Enter Short Description" rows="3" class="form-control" name="desc" required></textarea>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Link</label>
            <input ng-model="title" type="text" placeholder="Enter Link" name="link" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Location</label>
            <input ng-model="title" type="text" placeholder="Enter Location" name="location" class="form-control" required>
        </div>
<!--        <div class="form-group">-->
<!--            <label for="exampleInputEmail1">Color</label>-->
<!--            <input type="text" name="color" id="color" class="form-control" />-->
<!--        </div>-->
        <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Save</button>
    </div>

</form>

