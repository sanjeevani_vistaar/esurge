<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:08 AM
 */
?>
<div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Update Menu</h4>
            </div><!-- /.box-header -->
            <!-- form start -->
            <?php if($this->session->flashdata('err_msg')){?>
                <div class="alert alert-danger text-center">
                    <?php echo $this->session->flashdata('err_msg'); ?>
                </div>
            <?php }?>
            <form role="form" action="<?php echo base_url('admin/menu/edit_menu/'.$menu_details[0]->id);?>" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Menu Title</label>
                        <?php  if($menu_details[0]->menu_title=='pg_icon'){
                            $title='Meet The Icon';
                        }elseif($menu_details[0]->menu_title=='pg_contri'){
                            $title="Academicians Contribution";
                        }else{
                            $title=$menu_details[0]->menu_title;
                        } ?>
                        <input type="text" placeholder="Enter Title" readonly name="title" class="form-control" required value="<?php echo $title;?>">
                    </div>
<!--                    <div class="form-group">-->
<!--                        <label>Status</label>-->
<!--                        <div class="radio">-->
<!--                            <label class="">-->
<!--                                <div class="iradio_minimal"><input type="radio" --><?php //if($menu_details[0]->status==1)echo "checked";?><!-- value="1" id="optionsRadios1" name="status" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>-->
<!--                                Active-->
<!--                            </label>-->
<!--                        </div>-->
<!--                        <div class="radio">-->
<!--                            <label>-->
<!--                                <div class="iradio_minimal"><input type="radio" --><?php //if($menu_details[0]->status==0)echo "checked";?><!-- value="0" id="optionsRadios2" name="status" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>-->
<!--                                Deactive-->
<!--                            </label>-->
<!--                        </div>-->
<!--                    </div>-->
                    <div class="form-group">
                        <label>Is Login Required</label>
                        <div class="radio">
                            <label class="">
                                <div class="iradio_minimal"><input type="radio" value="1" id="optionsRadios1" <?php if($menu_details[0]->is_login_required==1)echo "checked";?> name="is_login_required" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                Yes
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <div class="iradio_minimal"><input type="radio" value="0" id="optionsRadios2" <?php if($menu_details[0]->is_login_required==0)echo "checked";?> name="is_login_required" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                No
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Access By </label>
                        <?php $arr=explode(',',$menu_details[0]->access_by);?>
                        <div class="radio">
                            <label class="">
                                <input name="access_by[]" type="checkbox" value="Consultant" <?php if(in_array('Consultant',$arr)){echo "checked";};?>>
                                Consultant
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input name="access_by[]" type="checkbox" value="Post graduates" <?php if(in_array('Post graduates',$arr)){echo "checked";};?>>
                                Post Graduates
                            </label>
                        </div>

                    </div>
<!--                    <div class="form-group">-->
<!--                        <label for="exampleInputEmail1">SEO Title</label>-->
<!--                        <input type="text" placeholder="Enter Page Title " name="page_title" class="form-control" required value="--><?php //echo $menu_details[0]->seo_title;?><!--">-->
<!--                    </div>-->
<!--                    <div class="form-group">-->
<!--                        <label for="exampleInputEmail1">SEO Keywords</label>-->
<!--                        <input type="text" placeholder="Enter Keywords" name="page_keywords" class="form-control" required value="--><?php //echo $menu_details[0]->seo_keywords;?><!--">-->
<!--                    </div>-->
<!--                    <div class="form-group">-->
<!--                        <label for="exampleInputEmail1">SEO Description</label>-->
<!--                        <input type="text" placeholder="Enter Description for SEO" name="page_desc" class="form-control" required value="--><?php //echo $menu_details[0]->seo_desc;?><!--">-->
<!--                    </div>-->
                </div>

                <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Update</button>
            </form>
        </div>
</div>