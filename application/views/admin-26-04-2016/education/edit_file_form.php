<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:08 AM
 */
?>

<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="box-title">Update File</h4>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form ng-app="" role="form" name="myForm" action="<?php echo base_url('admin/education/edit_file/'.$file_details[0]->edu_id);?>" method="post" enctype="multipart/form-data">
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Title</label>
                    <input ng-model="title" type="text" placeholder="Enter Title" name="title" class="form-control" required value="<?php echo $file_details[0]->edu_title;?>">
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea ng-model="desc" placeholder="Enter Short Description" rows="3" class="form-control" name="desc" required><?php echo $file_details[0]->edu_desc;?></textarea>
                </div>

                <div class="form-group">
                    <label for="exampleInputFile">Select File</label>
                    <input type="file" name="file_path" id="exampleInputFile">

                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tags</label>
                    <input ng-model="tags" type="text" placeholder="Enter Tags" name="tags" class="form-control" value="<?php echo $file_details[0]->tags;?>">
                </div>

                <div class="form-group">
                    <label>Status</label>
                    <div class="radio">
                        <label class="">
                            <div class="iradio_minimal checked" style="position: relative;" aria-checked="false" aria-disabled="false"><input type="radio" <?php if($file_details[0]->status==1)echo "checked";?> value="1" id="optionsRadios1" name="status" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                            Active
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <div class="iradio_minimal" style="position: relative;" aria-checked="false" aria-disabled="false"><input type="radio" <?php if($file_details[0]->status==0) echo "checked";?> value="0" id="optionsRadios2" name="status" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                            Deactive
                        </label>
                    </div>

                </div>

            </div><!-- /.box-body -->

            <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Update</button>
        </form>
    </div>
</div>