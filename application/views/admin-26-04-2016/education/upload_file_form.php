<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:08 AM
 */
?>

<div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Upload File</h4>
            </div><!-- /.box-header -->
            <!-- form start -->
            <?php if(!empty($this->session->flashdata('err_msg'))){?>
                <div class="alert alert-danger text-center">
                    <?php echo $this->session->flashdata('err_msg'); ?>
                </div>
            <?php }?>
            <form ng-app="" role="form" name="myForm" action="<?php echo base_url('admin/education/upload_file');?>" method="post" enctype="multipart/form-data">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Title</label>
                        <input ng-model="title" type="text" placeholder="Enter Title" name="title" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea ng-model="desc" placeholder="Enter Short Description" rows="3" class="form-control" name="desc" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Select File</label>
                        <input type="file" name="file_path" id="exampleInputFile" required>

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tags</label>
                        <input ng-model="tags" type="text" placeholder="Enter tags" name="tags" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <div class="radio">
                            <label class="">
                                <div class="iradio_minimal checked" style="position: relative;" aria-checked="false" aria-disabled="false"><input type="radio" checked="" value="1" id="optionsRadios1" name="status" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                Active
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <div class="iradio_minimal" style="position: relative;" aria-checked="false" aria-disabled="false"><input type="radio" value="0" id="optionsRadios2" name="status" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                Deactive
                            </label>
                        </div>

                    </div>

                </div><!-- /.box-body -->

                <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Upload</button>
            </form>
        </div>
</div>