<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:08 AM
 */
?>
<div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Update Profile</h4>
            </div><!-- /.box-header -->
            <!-- form start -->
            <?php if($this->session->flashdata('succ_msg')){?>
                <div class="alert alert-success text-center">
                    <?php echo $this->session->flashdata('succ_msg'); ?>
                </div>
            <?php }?>
            <form role="form" action="<?php echo base_url('admin/admin_users/update_profile/'.$admin_details[0]->admin_id);?>" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">First Name</label>
                        <input type="text" placeholder="Enter First Name" name="firstname" class="form-control" required value="<?php echo $admin_details[0]->first_name;?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Last Name</label>
                        <input type="text" placeholder="Enter Last Name" name="lastname" class="form-control" required value="<?php echo $admin_details[0]->last_name;?>">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Email Id</label>
                        <input type="email" readonly placeholder="Enter email" name="email" class="form-control" required value="<?php echo $admin_details[0]->email_id;?>">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Mobile Number</label>
                        <input type="text" placeholder="Enter Mobile Number" name="mobile" class="form-control" required value="<?php echo $admin_details[0]->mobile_no;?>">
                    </div>


                </div><!-- /.box-body -->

                <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Update</button>
            </form>
        </div>
</div>
<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="box-title">Change Password</h4>
        </div><!-- /.box-header -->
        <!-- form start -->
        <?php if(!empty($this->session->flashdata('succ_msg1'))){?>
            <div class="alert alert-success text-center">
                <?php echo $this->session->flashdata('succ_msg1'); ?>
            </div>
        <?php }?>
        <?php if(!empty($this->session->flashdata('err_msg'))){?>
            <div class="alert alert-danger text-center">
                <?php echo $this->session->flashdata('err_msg'); ?>
            </div>
        <?php }?>
        <form role="form" action="<?php echo base_url('admin/admin_users/update_password/'.$admin_details[0]->admin_id);?>" method="post">
            <div class="box-body">


                <div class="form-group">
                    <label for="exampleInputPassword1">New Password</label>
                    <input type="password" placeholder="Password" name="password" class="form-control" required >
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Confirm Password</label>
                    <input type="password" placeholder="Confirm Password" name="conf-password" class="form-control" required >
                </div>

                </div>

            </div><!-- /.box-body -->

            <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Update</button>
        </form>
    </div>
</div>