<!--<script type="text/javascript" src="<?php echo base_url('assets/front/js/homevideo.js');?>"></script>-->
<div class="col-md-9 total-news">
    <?php if(!empty($content_scroller)){?>
        <div class="slider">

            <div class="conference-slider">
                <!-- Slideshow 3 -->
                <ul class="conference-rslide" id="conference-slider">
                    <?php
                    $i=1;
                        foreach($content_scroller as $row){
                            $details=get_scoller_details($row->main_id,$row->banner_from);
                            $login_required = is_login_required($details['banner_source']);
                            $img[$i]=$details['img_path'];
                            $img_alt[$i]=$details['title'];
                             ?>
                            <li>
                            <?php
                            if($login_required) {
                                if (!$this->session->userdata('user_id')) {
                                    // for journals
                                    if ($details['banner_source'] == 'journals') {
                                        ?>
                                        <a href="<?php echo base_url('journals'); ?>">
                                            <img src="<?php echo base_url($details['img_path']); ?>" width="284" height="148" alt="<?php echo clean($details['title']);?>">
                                        </a>
                                    <?php }
                                    // for news
                                    if ($details['banner_source'] == 'news') {
                                        ?>
                                        <a href="<?php echo base_url('news'); ?>">
                                            <img src="<?php echo base_url($details['img_path']); ?>" width="284" height="148" alt="<?php echo clean($details['title']);?>">
                                        </a>
                                    <?php }
                                    // for videos
                                    if ($details['banner_source'] == 'videos') {
                                        ?>
                                        <a href="<?php echo base_url('videos'); ?>">
                                            <img src="<?php echo base_url($details['img_path']); ?>" width="262" height="152" alt="<?php echo clean($details['title']);?>">
                                        </a>
                                    <?php }

                                } else {
                                    // for journals
                                    if ($details['banner_source'] == 'journals') {
                                        ?>
                                        <a target="_blank" class="gotosingle"
                                           href="<?php echo base_url('journals/wiley_page/' . $details['id']) ?>">
                                            <img src="<?php echo base_url($details['img_path']); ?>" width="284" height="148" alt="<?php echo clean($details['title']);?>">
                                        </a>
                                    <?php }
                                    // for news
                                    if ($details['banner_source'] == 'news') {
                                        ?>
                                        <a href="<?php echo base_url('news/news_details/' . $details['id'].'/'.clean($details['title'])); ?>">
                                            <img src="<?php echo base_url($details['img_path']); ?>" width="284" height="148" alt="<?php echo clean($details['title']);?>">
                                        </a>
                                    <?php }
                                    // for videos
                                    if ($details['banner_source'] == 'videos') {
                                        ?>
                                        <a href="javascript:void(0);" alt="<?php echo $details['title']; ?>"
                                           onclick='$.videoLink="<?php echo base_url($details['vid_path']); ?>";$.videoTitle="<?php echo $details['title']; ?>";$.VideoPlay()'>
                                            <img src="<?php echo base_url($details['img_path']); ?>" width="262" height="152" alt="<?php echo clean($details['title']);?>">
                                        </a>
                                    <?php }

                                }
                            }else {
                                // for journals
                                if ($details['banner_source'] == 'journals') {
                                    ?>
                                    <a target="_blank" class="gotosingle"
                                       href="<?php echo base_url('journals/wiley_page/' . $details['id']) ?>">
                                        <img src="<?php echo base_url($details['img_path']); ?>" width="284" height="148" alt="<?php echo clean($details['title']);?>">
                                    </a>
                                <?php }
                                // for news
                                if ($details['banner_source'] == 'news') {
                                    ?>
                                    <a href="<?php echo base_url('news/news_details/' . $details['id'].'/'.clean($details['title'])); ?>">
                                        <img src="<?php echo base_url($details['img_path']); ?>" width="284" height="148" alt="<?php echo clean($details['title']);?>">
                                    </a>
                                <?php }
                                // for videos
                                if ($details['banner_source'] == 'videos') {
                                    ?>
                                    <a href="javascript:void(0);" alt="<?php echo $details['title']; ?>"
                                       onclick='$.videoLink="<?php echo base_url($details['vid_path']); ?>";$.videoTitle="<?php echo $details['title']; ?>";$.VideoPlay()'>
                                        <img src="<?php echo base_url($details['img_path']); ?>" width="262" height="152" alt="<?php echo clean($details['title']);?>">
                                    </a>
                                <?php }

                            }?>
                                <div class="conference-title">
                                    <h4>
                                        <?php
                                        if($login_required) {
                                            if (!$this->session->userdata('user_id')) {
                                                // for journals
                                                if ($details['banner_source'] == 'journals') {
                                                    ?>
                                                    <a href="<?php echo base_url('journals'); ?>">
                                                        <?php echo $details['title']; ?>
                                                    </a>
                                                <?php }
                                                // for news
                                                if ($details['banner_source'] == 'news') {
                                                    ?>
                                                    <a href="<?php echo base_url('news'); ?>">
                                                        <?php echo $details['title']; ?>
                                                    </a>
                                                <?php }
                                                // for videos
                                                if ($details['banner_source'] == 'videos') {
                                                    ?>
                                                    <a href="<?php echo base_url('videos'); ?>">
                                                        <?php echo $details['title']; ?>
                                                    </a>
                                                <?php }
                                            } else {
                                                // for journals
                                                if ($details['banner_source'] == 'journals') { ?>
                                                    <a target="_blank" class="gotosingle"
                                                       href="<?php echo base_url('journals/wiley_page/' . $details['id']) ?>">
                                                        <?php echo $details['title']; ?>
                                                    </a>
                                                <?php }
                                                // for news
                                                if ($details['banner_source'] == 'news') {
                                                    ?>
                                                    <a href="<?php echo base_url('news/news_details/' . $details['id'].'/'.clean($details['title'])); ?>">
                                                        <?php echo $details['title']; ?>
                                                    </a>
                                                <?php }
                                                // for videos
                                                if ($details['banner_source'] == 'videos') {
                                                    ?>
                                                    <a href="javascript:void(0);" alt="<?php echo $details['title']; ?>"
                                                       onclick='$.videoLink="<?php echo base_url($details['vid_path']); ?>";$.videoTitle="<?php echo $details['title']; ?>";$.VideoPlay()'>
                                                        <?php echo $details['title']; ?>
                                                    </a>
                                                <?php }
                                            }
                                        }else {
                                            // for journals
                                            if ($details['banner_source'] == 'journals') { ?>
                                                <a target="_blank" class="gotosingle"
                                                   href="<?php echo base_url('journals/wiley_page/' . $details['id']) ?>">
                                                    <?php echo $details['title']; ?>
                                                </a>
                                            <?php }
                                            // for news
                                            if ($details['banner_source'] == 'news') {
                                                ?>
                                                <a href="<?php echo base_url('news/news_details/' . $details['id'].'/'.clean($details['title'])); ?>">
                                                    <?php echo $details['title']; ?>
                                                </a>
                                            <?php }
                                            // for videos
                                            if ($details['banner_source'] == 'videos') {
                                                ?>
                                                <a href="javascript:void(0);" alt="<?php echo $details['title']; ?>"
                                                   onclick='$.videoLink="<?php echo base_url($details['vid_path']); ?>";$.videoTitle="<?php echo $details['title']; ?>";$.VideoPlay()'>
                                                    <?php echo $details['title']; ?>
                                                </a>
                                            <?php }
                                        }?>
                                        </h4>
                                    <p><?php echo $details['desc'];?></p>
                                </div>
                            </li>
                        <?php $i++;
                        }
            ?>
                </ul>
                <!-- Slideshow 3 Pager -->
                <?php if(count($img)>1){?>
                <ul id="slider3-pager">
                    <?php for($im=1;$im<=count($img);$im++){?>
						<li><a href="#"><img src="<?php echo base_url($img[$im]);?>" width="284" height="148" alt="<?php echo $img_alt[$im];?>"></a></li>
                    <?php }?>
                </ul>
                <?php }?>
            </div>

        </div>
    <?php } ?>
        <div class="posts">
            <div class="left-posts">
                <div class="world-news">
                    <div class="main-title-head">
                        <h3>Videos</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="world-news-grids">
                        <?php
                        $login_required = is_login_required('videos');
                        foreach($home_videos as $row)
                        {
                        ?>
                        <div class="video-wrapper floatL webcast-margin" id="video-wrapper">
                            <?php
                            if($login_required){
                            if (!$this->session->userdata('user_id')){
                            ?>
                            <a href="<?php echo base_url('videos'); ?>">
                                <?php }else{
                                ?>
                                <a href="javascript:void(0);" alt="<?php echo $row->vid_title; ?>"
                                   onclick='$.videoLink="<?php echo base_url($row->vid_path); ?>";$.videoTitle="<?php echo $row->vid_title; ?>";$.VideoPlay()'>
                                    <?php }
                                    }else{
                                    ?>
                                    <a href="javascript:void(0);" alt="<?php echo $row->vid_title; ?>"
                                       onclick='$.videoLink="<?php echo base_url($row->vid_path); ?>";$.videoTitle="<?php echo $row->vid_title; ?>";$.VideoPlay()'>
                                        <?php
                                    }?>
                                <img src="<?php echo base_url($row->img_path);?>" width="262" height="152" alt="<?php echo clean($row->vid_title);?>"/>
                                <h4>
									<?php 
									$title = substr($row->vid_title,0,50);
									echo $title."...";?>
								</h4>
                            </a>
                        </div>
                        <?php } ?>

                        <div class="clearfix"></div>

                    </div>
                </div>
                <div class="latest-articles">
                    <div class="main-title-head">
                        <h3>latest  news</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="world-news-grids">
                        <?php
                        $login_required = is_login_required('news');
                        if(!empty($home_news)){
                            foreach($home_news as $row){
                        ?>
                            <div class="world-news-grid">
                                <?php if($login_required) {
                                    if (!$this->session->userdata('user_id')) { ?>
                                    <a href="<?php echo base_url('news');?>">
                                        <?php
                                    } else { ?>
                                        <a href="<?php echo base_url('news/news_details/'.$row->news_id.'/'.clean($row->news_title));?>">
                                    <?php }
                                }else{?>
                                    <a href="<?php echo base_url('news/news_details/'.$row->news_id.'/'.clean($row->news_title));?>">
                               <?php }?>

                                    <img src="<?php echo base_url($row->news_img_path);?>" width="284" height="148" alt="<?php echo clean($row->news_title);?>" />
                                </a>

                                <?php if($login_required) {
                                if (!$this->session->userdata('user_id')) { ?>
                                <a href="<?php echo base_url('news');?>" class="title">
                                <?php
                                } else { ?>
                                <a href="<?php echo base_url('news/news_details/'.$row->news_id.'/'.clean($row->news_title));?>" class="title">
                                <?php }
                                }else{?>
                                <a href="<?php echo base_url('news/news_details/'.$row->news_id.'/'.clean($row->news_title));?>" class="title">
                                <?php }?>

                                
                                    <?php echo $row->news_title;?>
                                </a>

                                <p><?php echo $row->short_desc;?></p><!--<a href="">Read More</a>-->
                            </div>
                        <?php }
                        }else{
                            echo "No Records Found";
                        }?>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>