<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/16/2015
 * Time: 1:25 PM
 */
?>
<script>
$(document).ready(function(){
        $("#state").change(function(){
            var state_id=$("#state").val();
        if(state_id) {
            $.ajax({
                url: "<?php echo base_url('index.php/registration/get_cities');?>",
                type: "POST",
                data:{id:state_id},
                success: function(data) {
                    $("#city").html(data);
                }
            });
        }else{
                $("#city").html('<select name="city" id="" tabindex="8" required class="form-select "><option value="">Select City</option></select>')
            }
        })
});
</script>
<div class="col-md-9 total-news">
        <div class="regi-content">
    <div class="main-title-head">
        <h3>Registration</h3>
        <div class="clearfix"></div>
    </div>
    <div class="row">

      
        <form name="registration_frm" action="<?php echo base_url('index.php/registration');?>" method="post" class="regisform">
            <div class="form-left floatL">
                <input type="text" name="name" Placeholder="Name *" tabindex="1" required>
                <input type="email" name="email" Placeholder="Email ID *" tabindex="2" required>
                <!--<input type="text" name="registration_code" Placeholder="Login Code *" tabindex="3" required>-->
                <input type="password" name="password" Placeholder="Password *" tabindex="4" required>
                <input type="password" name="conf-password" Placeholder="Confirm Password *" tabindex="5" required>
                <input type="tel" name="mobile" Placeholder="Mobile Number *" tabindex="6" required>
                <input type="text" name="dob" Placeholder="Birth Date" tabindex="12" id="datepicker">
                
            </div>
            <div class="form-left floatL">
                <div class="form_arrow">
                <select name="state" id="state" tabindex="7" required class="form-select ">
                    <option value="" >Select Your State *</option>
                    <?php foreach($states as $row)
                    {
                        ?>
                        <option value="<?php echo $row->state_id;?>"><?php echo ucwords(strtolower($row->state_name));?></option>
                    <?php } ?>
                </select>
                    </div>
                <div class="form_arrow" id="city">

                <select name="city" id="" tabindex="8" required class="form-select ">
                    <option value="">Select Your City *</option>
                </select>
                </div>
                <div class="form_arrow">
                <select name="user_type" id="" tabindex="9" required class="form-select ">
                    <option value="">Select Your Status *</option>
                    <option value="Consultant">Consultant</option>
                    <option value="Post graduates">Post graduates</option>
                </select>
                </div>
                <input type="text" name="registration_no" Placeholder="Registration Number *" tabindex="10" required>
                <input type="text" name="usi_no" Placeholder="USI Number" tabindex="11">
                <div class="submit-btn clearfix">
                    <input type="reset" class="floatL" id="reset">
                <input type="submit" name="submit" class="floatL" Value="Submit">
                </div>

            </div>

        </form>
    </div>
</div>
        </div>