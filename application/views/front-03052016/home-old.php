<script type="text/javascript" src="<?php echo base_url('assets/front/js/homevideo.js');?>"></script>
<div class="col-md-9 total-news">
        <div class="slider">

            <div class="conference-slider">
                <!-- Slideshow 3 -->
                <ul class="conference-rslide" id="conference-slider">
                    <?php if(!empty($content_scroller)){
                        $i=1;
                        foreach($content_scroller as $row){
                            $details=get_scoller_details($row->main_id,$row->banner_from);
                            $img[$i]=$details['img_path'];
                             ?>
                            <li>
                                <a target="_blank" class="gotosingle" href="<?php echo base_url('index.php/journals/wiley_page/'.$details['id']) ?>">
                                    <img src="<?php echo base_url($details['img_path']);?>" alt="">
                                </a>
                                <div class="conference-title" >
                                    <h4>
                                        <a target="_blank" class="gotosingle" href="<?php echo base_url('index.php/journals/wiley_page/'.$details['id']) ?>">
                                          <?php echo $details['title'];?></h4>
                                        </a>
                                    <p><?php echo $details['desc'];?></p>
                                </div>
                            </li>
                        <?php $i++;
                        }

                    }?>
                </ul>
                <!-- Slideshow 3 Pager -->
                <ul id="slider3-pager">
                    <?php for($im=1;$im<=count($img);$im++){?>
                    <li><a href="#"><img src="<?php echo base_url($img[$im]);?>" alt=""></a></li>
                    <?php }?>
                </ul>

            </div>
        </div>
        <div class="posts">
            <div class="left-posts">
                <div class="world-news">
                    <div class="main-title-head">
                        <h3>Video</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="world-news-grids">
                        <div class="video-wrapper floatL " id="video-wrapper">
                            <a href="#" data-toggle="modal" data-target="#modalvideo1" data-backdrop="static" data-keyboard="false">
                                <img src="<?php echo base_url('uploads/gallery/videos/images/V9-04-Thumb.jpg');?>" alt="homevideo1"/>
                                <h4>Direct Visualization Percutaneous Nephrolithotomy Without Fluoroscopy    Roger Li (14-1592)</h4>
                            </a>
                        </div>
                        <!-- Modal HTML -->
                        <div class="modal fade" id="modalvideo1">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" data-backdrop="static" data-keyboard="false">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 class="modal-title">Direct Visualization Percutaneous Nephrolithotomy Without Fluoroscopy    Roger Li (14-1592)</h4>
                                    </div>

                                    <div class="modal-body">

                                        <video width="100%" id="video1" controls>
                                            <source src="<?php echo base_url('uploads/gallery/videos/v904.mp4');?>" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="video-wrapper floatL" id="video-wrapper2">
                            <a href="#" data-toggle="modal" data-target="#modalvideo2" data-backdrop="static" data-keyboard="false">
                                <img src="<?php echo base_url('uploads/gallery/videos/images/V7-08-Thumb.jpg');?>" alt="homevideo2"/>
                                <h4>Multidisciplinary Reconstruction of An Amputated Penis    J. Francis Scott (14-25)</h4>
                            </a>
                        </div>
                        <!-- Modal HTML -->
                        <div class="modal fade" id="modalvideo2">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" data-backdrop="static" data-keyboard="false">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 class="modal-title">Multidisciplinary Reconstruction of An Amputated Penis    J. Francis Scott (14-25)</h4>
                                    </div>

                                    <div class="modal-body">
                                        <video width="100%" id="video2" controls>
                                            <source src="<?php echo base_url('uploads/gallery/videos/v708.mp4');?>" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                    </div>
                </div>
                <div class="latest-articles">
                    <div class="main-title-head">
                        <h3>latest  news</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="world-news-grids">
                        <?php if(!empty($home_news)){
                            foreach($home_news as $row){
                        ?>
                            <div class="world-news-grid">
                                <a href="<?php echo base_url('index.php/news/news_details/'.$row->news_id);?>"><img src="<?php echo base_url($row->news_img_path);?>" alt="" /></a>
                                <a href="<?php echo base_url('index.php/news/news_details/'.$row->news_id);?>" class="title"><?php echo $row->news_title;?></a>
                                <p><?php echo $row->short_desc;?></p><!--<a href="">Read More</a>-->
                            </div>
                        <?php }
                        }else{
                            echo "No Records Found";
                        }?>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>