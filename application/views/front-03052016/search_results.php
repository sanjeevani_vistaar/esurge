<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 10:41 AM
 */
?>

<div class="col-md-9 total-blog">
<?php $record_availabe=0;?>
	 <div class="content search_margin">
<?php if(!empty($news)){
                $record_availabe=1;
                ?>
        <div class="grids">
            
                <div class="main-title-head">
                    <h3>Search Result - News</h3>
                    <div class="clearfix"></div>
                </div>
                <?php
                $login_required = is_login_required('news');
                foreach($news as $row)
                {
                    ?>
                    <div class="grid box">
                        <div class="grid-header">
                            <?php if($login_required)
                            {
                                if (!$this->session->userdata('user_id'))
                                {	?>
                                    <a href="javascript:void(0);" onclick='$.ChkLogin()' class="title"><?php echo $row->news_title;?></a>
                                    <?php
                                }
                                else {?>
                                    <a href="<?php echo base_url('news/news_details/'.$row->news_id.'/'.clean($row->news_title));?>" class="title"><?php echo $row->news_title;?></a>
                                <?php }
                            }
                            else{ ?>
                                <a href="<?php echo base_url('news/news_details/'.$row->news_id.'/'.clean($row->news_title));?>" class="title"><?php echo $row->news_title;?></a>
                            <?php }?>
                        </div>
                        <div class="grid-img-content">

                            <?php if($login_required)
                            {
                            if (!$this->session->userdata('user_id'))
                            {	?>
                            <a href="javascript:void(0);" onclick='$.ChkLogin()' class="title">
                                <?php
                                }
                                else {?>
                                <a href="<?php echo base_url('news/news_details/'.$row->news_id.'/'.clean($row->news_title));?>">
                                    <?php }
                                    }
                                    else{ ?>
                                    <a href="<?php echo base_url('news/news_details/'.$row->news_id.'/'.clean($row->news_title));?>">
                                        <?php }?>

                                        <img class="blog" src="<?php echo base_url($row->news_img_path);?>" alt="<?php echo clean($row->news_title);?>" />

                                    </a>

                            <p><?php echo $row->short_desc;?></p>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                <?php }?>
</div>
<?php
            }
            ?>
         <?php if(!empty($videos)){
                $record_availabe=1;
                ?>
        <div class="clearfix"></div>
        <div class="video-grid" id="cat_video" style="margin-bottom: 5px;">
           
                <div class="main-title-head">
                    <h3>Search Result - Videos</h3>
                    <div class="clearfix"></div>
                </div>
                <?php
                $login_required = is_login_required('videos');
                foreach($videos as $row)
                {
                    ?>
                    <div class="video-main-container floatL" id="video-main-container3">
                        <?php if($login_required) {
                        if (!$this->session->userdata('user_id')) {
                        ?>
                        <a href="javascript:void(0);" onclick='$.ChkLogin()' >
                            <?php } else {
                            ?>
                            <a href="javascript:void(0);" onclick='$.videoLink="<?php echo base_url($row->vid_path); ?>";$.videoTitle="<?php echo $row->vid_title; ?>";$.VideoPlay()'>
                                <?php }
                                }else{ ?>
                                <a href="javascript:void(0);" onclick='$.videoLink="<?php echo base_url($row->vid_path); ?>";$.videoTitle="<?php echo $row->vid_title; ?>";$.VideoPlay()'>
                                    <?php }?>
                                <img src="<?php echo base_url($row->img_path);?>" alt="<?php echo clean($row->vid_title);?>">
                                <h4>
                                    <?php
                                    $title = substr($row->vid_title,0,45);
                                    echo $title."...";?>
                                </h4>
                            </a>
                    </div>
                <?php }?>
</div>
<?php
            }?>
         <?php if(!empty($pg_icon)){
                $record_availabe=1;
                ?>
        <div class="clearfix"></div>
        <div class="video-grid" id="cat_video"  style="margin-bottom: 5px;">
           
                <div class="main-title-head">
                    <h3>Search Result - PG / Meet The Icon</h3>
                    <div class="clearfix"></div>
                </div>
                <?php
                $login_required = is_login_required('pg_icon');
                foreach($webcast as $row)
                {
                    ?>
                    <div class="video-main-container floatL webcast-margin" id="video-main-container3">
                        <?php if($login_required) {
                        if (!$this->session->userdata('user_id')) {
                        ?>
                        <a href="javascript:void(0);" onclick='$.ChkLogin()'>
                            <?php } else {
                            ?>
                            <a href="javascript:void(0);" alt="<?php echo $row->vid_title; ?>" onclick='$.videoLink="<?php echo base_url($row->vid_path); ?>";$.videoTitle="<?php echo $row->vid_title; ?>";$.VideoPlay()'>
                                <?php }
                                }else {
                                ?>
                                <a href="javascript:void(0);" alt="<?php echo $row->vid_title; ?>" onclick='$.videoLink="<?php echo base_url($row->vid_path); ?>";$.videoTitle="<?php echo $row->vid_title; ?>";$.VideoPlay()'>
                                <?php }
                                    ?>
                                <img src="<?php echo base_url($row->img_path);?>" alt="<?php echo clean($row->vid_title);?>">
                                <h4>
                                    <?php
                                    $title = substr($row->vid_title,0,45);
                                    echo $title."...";?>
                                </h4>
                            </a>
                    </div>
                <?php } ?>
 </div>
          <?php  }?>
        <?php if(!empty($uros)){
                $record_availabe=1;
                ?>
        <div class="clearfix"></div>
        <div class="grids uromargin" id="cat_uros">
           
                <div class="main-title-head">
                    <h3>Search Result - Uro Slides</h3>
                    <div class="clearfix"></div>
                </div>
                <?php
                $login_required = is_login_required('uro');
                foreach($uros as $row)
                {
                    ?>
                    <div class="grid box journals-title searchuro">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <span class="fa  fa-file-powerpoint-o "></span>
                                </div>
                                <div class="grid-img-content">
                                    <h1>
                                        <?php if($login_required)
                                        {
                                            if (!$this->session->userdata('user_id'))
                                            {	?>
                                                <a href="javascript:void(0);" onclick='$.ChkLogin()' ><?php echo $row->uro_title;?></a>
                                                <?php
                                            }
                                            else {?>
                                                <a target="_blank" href="http://docs.google.com/gview?url=<?php echo base_url($row->file_path);?>"><?php echo $row->uro_title;?></a>
                                            <?php }
                                        }
                                        else{ ?>
                                            <a target="_blank" href="http://docs.google.com/gview?url=<?php echo base_url($row->file_path);?>"><?php echo $row->uro_title;?></a>
                                        <?php }?>
                                    </h1>
                                    <h3><?php echo $row->date;?></h3>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
 </div>
        <?php    }?>
		<div class="clearfix"></div>
        </div>
        
   
<?php if( !$record_availabe){?>
            <div class="main-title-head">
                <h3>Search Result</h3>
                <div class="clearfix"></div>
            </div>
                No Records Found
            <?php }?>
</div>