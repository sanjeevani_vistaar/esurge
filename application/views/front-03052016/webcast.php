<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/30/2015
 * Time: 10:38 AM
 */
?>
<script>
	$(document).ready(function(){
		$("#category").change(function(){

			var category_id=$("#category").val();
			window.location.href="<?php echo base_url('webcast/get_videos');?>/"+category_id;
		})

	});
</script>
<div class="col-md-9 total-blog">
	<div class="main-title-head">
		<h3>Webcast</h3>
		<div class="video-filter floatR">
			<ul>
				<li>Category : </li>
				<li>

					<select class="form-select" id="category">
						<option value="all">All Categories</option>
						<?php foreach($categories as $row)
						{
							?>
							<option value="<?php echo $row->cat_id;?>" <?php if($selected_cat==$row->cat_id)echo "selected";?>><?php echo ucwords(strtolower($row->cat_name));?></option>
						<?php } ?>
					</select>

				</li>
			</ul>
		</div>
		<div class="clearfix"></div>
	</div>
	<?php $login_required = is_login_required($this->router->fetch_class());?>

	<div class="video-grid" id="cat_video">

		<?php if(!empty($all_videos)){
			foreach($all_videos as $row)
			{
				?>
				<div class="video-main-container floatL webcast-margin" id="video-main-container3">
					<?php if($login_required) {
				if (!$this->session->userdata('user_id')) {
					?>
					<a href="javascript:void(0);" onclick='$.ChkLogin()' alt="<?php echo $row->vid_title; ?>">
				<?php } else {
					?>
					<a href="javascript:void(0);" alt="<?php echo $row->vid_title; ?>" onclick='$.videoLink="<?php echo base_url($row->vid_path); ?>";$.videoTitle="<?php echo $row->vid_title; ?>";$.VideoPlay()'>
				<?php }
			}else {
					?>
						<a href="javascript:void(0);" alt="<?php echo $row->vid_title; ?>" onclick='$.videoLink="<?php echo base_url($row->vid_path); ?>";$.videoTitle="<?php echo $row->vid_title; ?>";$.VideoPlay()'>
			<?php }
			?>
							<img src="<?php echo base_url($row->img_path);?>" alt="">
							<h4>
								<?php 
								$title = substr($row->vid_title,0,45);
								echo $title."...";?>
							</h4>
						</a>
				</div>
			<?php }?>
			<div class="clearfix"></div>
			<div class="clearfix btvinpagination">

				<?php echo $ui_pagging; ?>

			</div>
			<div class="clearfix"></div>
           <?php }else{
            echo "No Records Found";
        }
        ?>
	</div>

</div>