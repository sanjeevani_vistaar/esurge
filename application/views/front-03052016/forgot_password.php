<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/16/2015
 * Time: 1:25 PM
 */
?>
<div class="main-title-head">
        <h3>Forgot Password</h3>
        <div class="clearfix"></div>
    </div>
<div class="regi-content">
    <div class="row">

        
        <form name="registration_frm" action="<?php echo base_url('login/forgot_password');?>" method="post" class="regisform">
            <div class="col-md-4">
<div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-envelope"></span>
                            </div>
                           <input type="email" name="email" Placeholder="Email ID" tabindex="2" required>
                        </div>
                    </div>
                 <div class="submit-btn clearfix">
                        <input type="reset" class="floatL" id="reset">
                        <input type="submit" name="submit"class="floatL" Value="Submit">
                    </div>
                
            </div>


        </form>
    </div>
</div>
