<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/23/2016
 * Time: 1:22 PM
 */
 
?>

<script>
	$(document).ready(function() {
		$("#eve_year").change(function () {
			var year = $("#eve_year").val();
			var month = $("#eve_month").val();
			window.location.href="<?php echo base_url('events/events_list_filter');?>/"+year+"/"+month;
//			$.ajax({
//				url: "<?php //echo base_url('events/filter_events');?>//",
//				type: "POST",
//				data: {year: year, month: month},
//				success: function (data) {
//					$("#eve_list").html(data);
//				}
//			});
		})
		$("#eve_month").change(function () {
			var year = $("#eve_year").val();
			var month = $("#eve_month").val();
			window.location.href="<?php echo base_url('events/events_list_filter');?>/"+year+"/"+month;
//			$.ajax({
//				url: "<?php //echo base_url('events/filter_events');?>//",
//				type: "POST",
//				data: {year: year, month: month},
//				success: function (data) {
//					$("#eve_list").html(data);
//				}
//			});
		})
	})

</script>
<div class="col-md-9 total-news">
	<div class="Events-mainwrapper">
		<div class="main-title-head">
			<h3>Events</h3>
			<div class="clearfix"></div>
		</div>
		<div class="events-widget clearfix">
			<div class="event-wrapper">
				<div class="calendar-widget floatL clearfix">
					<div class="calendar-icon floatL">
						<i class="fa fa-calendar"></i>
					</div>
					<select name="eve_year" id="eve_year" class="floatL">
						<?php
						for($i=date('Y');$i<date('Y')+5;$i++)
						{?>
						<option value="<?php echo $i;?>" <?php if($selected_year==$i)echo "selected";?>><?php echo "Year - ".$i;?></option>
						<?php } ?>
					</select>
				</div>
				<div class="calendar-widget floatL">
					<?php
						$month=array();
						$month=array('January','February','March','April','May','June','July','August','September','October','November','December');
					?>
					<div class="calendar-icon floatL">
						<i class="fa fa-calendar"></i>
					</div>
					<select name="eve_month floatL" id="eve_month" class="floatL">
						<?php
						for($i=0;$i<count($month);$i++)
						{?>
						<option value="<?php echo $month[$i];?>" <?php if($selected_month==$month[$i])echo "selected";?>><?php echo $month[$i];?></option>
						<?php } ?>
					</select>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php $login_required = is_login_required($this->router->fetch_class());?>

			<div id="eve_list">

				<?php
				if($all_events){
				foreach($all_events as $row){
				$date_split=explode(' ',$row->start);
				$date=$date_split[0];
				$time=strtotime($date);
				$year=date("Y",$time);
				$month=date("F",$time);
				$day=date("j",$time);
				$end_date_split=explode(' ',$row->end);
				$end_date=$end_date_split[0];
				$end_time=strtotime($end_date);
				$end_year=date("Y",$end_time);
				$end_month=date("M",$end_time);
				$end_day=date("j",$end_time);
				?>
			
				<div class="events-wrap floatL">
					<div class="inner clearfix">
						<div class="first-wrap floatL">
							<time>
								<div class="date"><?php echo $day." - ".$end_day;?></div>
								<div class="month"><?php echo $month;?></div>
								<div class="year"><?php echo $year;?></div>
							</time>

						</div>
						<div class="events-details floatL">
							<?php $url = parse_url($row->link);
							if(in_array("http", $url)) {
								$link=$row->link;
							}elseif(in_array("https", $url)) {
								$link=$row->link;
							}else{
								$link="http://".$row->link;
							}?>
							<header>
								<h3>
									<?php if($login_required)
									{
										if (!$this->session->userdata('user_id'))
										{	?>
										<a href="javascript:void(0);" onclick='$.ChkLogin()' ><?php echo $row->title;?></a>
										<?php
										}
										else {?>
											<a target="_blank" href="<?php echo $link;?>"><?php echo $row->title;?></a>
										<?php }
									}
									else{ ?>
										<a target="_blank" href="<?php echo $link;?>"><?php echo $row->title;?></a>
									<?php }?>
								</h3>
							</header>
							<div class="place">
								<p><?php echo $row->location;?></p>
							</div>
						</div>
					</div>
				</div>
				<?php }?>
				<div class="clearfix"></div>
				<div class="clearfix btvinpagination">

					<?php echo $ui_pagging; ?>

				</div>
				<?php } else{
					echo "No Records found";
				} ?>
			</div>

		</div>
	</div>
</div>
