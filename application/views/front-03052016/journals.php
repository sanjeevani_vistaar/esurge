<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/30/2015
 * Time: 10:45 AM
 */
?>
<div class="col-md-9 total-blog">
    <div class="main-title-head">
        <h3>Journals</h3>
        <div class="clearfix"></div>
    </div>
    <div class="content">

        <div class="grids" id="journals_list">

            <?php $login_required = is_login_required($this->router->fetch_class());?>
            <?php if(!empty($all_journals)){
            foreach($all_journals as $row)
            {
            ?>
                <div class="grid box">
                    <div class="grid-header">
                <?php if($login_required) {
                    if (!$this->session->userdata('user_id')) {
                        ?>
                        <a href="javascript:void(0);" onclick='$.ChkLogin()'><?php echo $row->journal_title; ?></a>
                    <?php } else {
                        ?>
                        <a target='_blank' class="gotosingle"
                           href="<?php echo base_url('/journals/wiley_page/' . $row->journal_id) ?>"><?php echo $row->journal_title; ?></a>
                    <?php }
                }else{
                    ?>
                    <a target='_blank' class="gotosingle"
                       href="<?php echo base_url('/journals/wiley_page/' . $row->journal_id) ?>"><?php echo $row->journal_title; ?></a>
                    <?php
                }?>
                    </div>
                    <div class="grid-img-content">
                <?php if(!$this->session->userdata('user_id')){?>
                <a href="javascript:void(0);" onclick='$.ChkLogin()'><img src="<?php echo base_url($row->journal_img_path);?>" class="blog" alt="<?php echo clean($row->journal_title);?>" /></a>
            <?php }else{?>
                        <a target='_blank' href="<?php echo base_url('/journals/wiley_page/'.$row->journal_id) ?>"><img src="<?php echo base_url($row->journal_img_path);?>" class="blog" alt="<?php echo clean($row->journal_title);?>" /></a>
                <?php }?>
                <p><?php echo $row->journal_desc;?></p>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            <?php }?>
                <div class="clearfix"></div>
                <div class="clearfix btvinpagination">

                    <?php echo $ui_pagging; ?>

                </div>
			<div class="clearfix"></div>
           <?php }
            else{
                echo "No Records Found";
            }?>
        </div>

    </div>
</div>