<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 11:09 AM
 */
?>
<div class="col-md-9 total-blog">
    <div class="main-title-head">
        <h3>Disclaimer</h3>
        <div class="clearfix"></div>
    </div>
    <div class="disclaimer-wrapper">
        <p><a href="http://www.esurge.in">www.esurge.in</a> is a urology education initiative by Sun Pharma Laboratories Ltd.<br><br>

                All of the information available through the website is intended for adults who are licensed healthcare professionals. While we hope you find the website helpful, you should remember that it is not meant to serve as a substitute for your own readings and clinical judgment as a healthcare professional. The website does not provide medical advice. If you are a consumer who chooses to use the professional level information available through this website you should not rely on that information as professional medical advice or use it to replace any relationship with your physician or other qualified healthcare professional.<br><br>

                For medical concerns, including decisions about medications and other treatments, consumers should always consult their physician. We follow standard sets of editorial procedures for information that we make available through the Website. However, we don't offer you any warranty or guarantee related to the content available on the website. If you are a healthcare professional, you should exercise your professional judgment in evaluating any information, and we encourage you to confirm the information made available or otherwise obtained through the website with other sources before undertaking any treatment based on it.<br><br>

                Views and opinions expressed by professional experts are their own and Sun Pharma Laboratories Ltd will not be held responsible for the same..</p>

    </div>
    <div class="main-title-head">
        <h3>Terms of use</h3>
        <div class="clearfix"></div>
    </div>
    <div class="disclaimer-wrapper">
        <p>By accessing or using <a href="http://www.esurge.in">www.esurge.in</a>, you agree to comply with and be bound by these Terms and Conditions of Use (Terms of Use). This portal also contains views and opinions expressed by professional experts (Uro Share Slides, Videos and Webcast). Hence, it becomes imperative for users to refrain from using content available on this website for any purpose without written consent from Sun Pharma Laboratories Ltd and respective professional experts.</p>
    </div>
</div>
