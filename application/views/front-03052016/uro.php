<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/4/2016
 * Time: 4:31 PM
 */
?>
<script>
    $(document).ready(function(){
        $("#category").change(function(){
            var category_id=$("#category").val();
            window.location.href="<?php echo base_url('uro/get_uros');?>/"+category_id;
        })

    });
</script>
<div class="col-md-9 total-blog">
    
        <div class="main-title-head uroslide_width">
            <h3>Uro Slide Share</h3>
            <div class="video-filter floatR">
            <ul>
                <li>Category : </li>
                <li>

                    <select class="form-select" id="category">
                        <option value="all">All Categories</option>
                        <?php foreach($categories as $row)
                        {
                            if($row->cat_name!='OTHERS'){
                            ?>
                            <option value="<?php echo $row->cat_id;?>" <?php if($selected_cat==$row->cat_id)echo "selected";?>><?php echo $row->cat_name;?></option>
                        <?php }else{
                                $catid=$row->cat_id;
                                $cat_name=$row->cat_name;
                            }} ?>
                        <option value="<?php echo $catid;?>" <?php if($selected_cat==$catid)echo "selected";?>><?php echo $cat_name;?></option>
                    </select>

                </li>
            </ul>
        </div>
            <div class="clearfix"></div>
        </div>
    <?php $login_required = is_login_required($this->router->fetch_class());?>

        <div class="grids" id="cat_uros">

            <?php if(!empty($all_uros)){
            foreach($all_uros as $row)
            {
            ?>
                <div class="grid box journals-title">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span class="fa  fa-file-powerpoint-o "></span>
                            </div>
                            <div class="grid-img-content">
                                <h1>

                                    <?php if($login_required)
                                    {
                                        if (!$this->session->userdata('user_id'))
                                        {	?>
                                            <a href="javascript:void(0);" onclick='$.ChkLogin()' ><?php echo $row->uro_title;?></a>
                                            <?php
                                        }
                                        else {?>
                                            <a target="_blank" href="http://docs.google.com/gview?url=<?php echo base_url($row->file_path);?>"><?php echo $row->uro_title;?></a>
                                        <?php }
                                    }
                                    else{ ?>
                                        <a target="_blank" href="http://docs.google.com/gview?url=<?php echo base_url($row->file_path);?>"><?php echo $row->uro_title;?></a>
                                    <?php }?>

                                </h1>
                                <h3><?php echo $row->date;?></h3>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }?>
            <div class="clearfix"></div>
            <div class="clearfix btvinpagination">

                <?php echo $ui_pagging; ?>

            </div>
            <?php
            }else{
                echo "No Records Found";
            }
            ?>
        </div>

</div>
