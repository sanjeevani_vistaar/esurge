<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 10:41 AM
 */
?>
<div class="col-md-9 total-blog">
<div class="main-title-head">
        <h3>News</h3>
        <div class="clearfix"></div>
    </div>
    <div class="content">

        <div class="grids" id="news_list">

            <?php $login_required = is_login_required($this->router->fetch_class());?>
            <?php if(!empty($all_news)){
            foreach($all_news as $row)
            {
            ?>
                <div class="grid box">
                    <div class="grid-header">
                        <?php if($login_required)
                        {
                            if (!$this->session->userdata('user_id'))
                            {	?>
                                <a href="javascript:void(0);" onclick='$.ChkLogin()' class="title"><?php echo $row->news_title;?></a>
                                <?php
                            }
                            else {?>
                                <a href="<?php echo base_url('news/news_details/'.$row->news_id.'/'.clean($row->news_title));?>" class="title"><?php echo $row->news_title;?></a>
                            <?php }
                        }
                        else{ ?>
                            <a href="<?php echo base_url('news/news_details/'.$row->news_id.'/'.clean($row->news_title));?>" class="title"><?php echo $row->news_title;?></a>
                        <?php }?>

                    </div>
                    <div class="grid-img-content">

                        <?php if($login_required)
                        {
                            if (!$this->session->userdata('user_id'))
                            {	?>
                                <a href="javascript:void(0);" onclick='$.ChkLogin()' class="title">
                                <?php
                            }
                            else {?>
                        <a href="<?php echo base_url('news/news_details/'.$row->news_id.'/'.clean($row->news_title));?>">
                            <?php }
                        }
                        else{ ?>
                            <a href="<?php echo base_url('news/news_details/'.$row->news_id.'/'.clean($row->news_title));?>">
                        <?php }?>

                            <img class="blog" src="<?php echo base_url($row->news_img_path);?>" alt="<?php echo clean($row->news_title);?>" />

                        </a>
                        <p><?php echo $row->short_desc;?></p>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            <?php }?>
                <div class="clearfix"></div>
                <div class="clearfix btvinpagination">

                    <?php echo $ui_pagging; ?>

                </div>
			<div class="clearfix"></div>
           <?php }
            else{
                echo "No Records Found";
            }?>
        </div>

    </div>
</div>

