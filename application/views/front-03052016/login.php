<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 2:59 PM
 */
?>
<div class="container">
    <div class="row">
      <div class="formWrapper">
	  <div class="grid_6">
        <h3>Login</h3>
      </div>

			<div class="grid_6">
				<div class="logmod__form">
                    <?php if(!empty($this->session->flashdata('err_msg'))){?>
                        <div class="alert alert-danger text-center">
                            <?php echo $this->session->flashdata('err_msg'); ?>
                        </div>
                    <?php }?>
					<form accept-charset="utf-8" action="<?php echo base_url('login');?>" method="post" class="simform">
						<div class="sminputs">
						  <div class="input full">
							<label class="string optional" for="user-name">Email Id *</label>
							<input class="string optional" maxlength="255" name="email_id" id="user-email" placeholder="Email Id" type="email" size="50" required="" />
						  </div>
						</div>
						<div class="sminputs">
						  <div class="input string optional">
							<label class="string optional" for="user-pw">Password *</label>
							<input class="string optional" maxlength="255" name="password" id="user-pw" placeholder="Password" type="text" size="50" required=""/>
						 </div>
						</div>
						<div class="forgetpassword">
							<a href="">Forgot password?</a>
						</div>
						<div class="simform__actions">
							<input type="submit" class="sumbit submit-btn"name="submit" value="Login"/>
							<input class="sumbit register-btn" name="commit" type="sumbit" value="Register" />
							<span class="simform__actions-sidetext">By creating an account you agree to our <a class="special" href="#" target="_blank" role="link">Terms & Privacy</a></span>
						</div>
					</form>
				</div>
			</div>
		</div>
    </div>
</div>