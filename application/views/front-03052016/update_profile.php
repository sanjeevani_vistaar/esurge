<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/21/2015
 * Time: 3:49 PM
 */
?>
<script>
    $(document).ready(function(){
        var state_id=$("#state").val();

            $.ajax({
                url: "<?php echo base_url('registration/get_cities');?>",
                type: "POST",
                data: {id: state_id},
                success: function (data) {
                    $("#city").html(data);
                }
            });
        $("#state").change(function(){
            var state_id=$("#state").val();
            if(state_id) {
                $.ajax({
                    url: "<?php echo base_url('registration/get_cities');?>",
                    type: "POST",
                    data: {id: state_id},
                    success: function (data) {
                        $("#city").html(data);
                    }
                });
            }else{
                $("#city").html('<select name="city" id="" tabindex="8" required class="form-select "><option value="">Select Your City *</option></select>')
            }
        })
$("#reset").click(function(){
$.ajax({
                url: "<?php echo base_url('profile/clear_session');?>",
                type: "POST",
                data:{},
                success: function(data) {
                    window.location.href = "<?php echo base_url('profile/update_profile');?>";
                }
            });

})
    });
</script>
<div class="col-md-9 total-news">
    <div class="main-title-head">
        <h3>Update Profile</h3>
        <div class="clearfix"></div>
    </div>
    <div class="regi-content">
        <div class="row">

            <form name="registration_frm" action="<?php echo base_url('profile/update_profile');?>" method="post" class="regisform">

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-user"></span>
                            </div>
                            <input type="text" name="first_name" Placeholder="First Name *" tabindex="1" value="<?php if($this->session->userdata('ufirst_name')){echo $this->session->userdata('ufirst_name');}else{echo $user_details[0]->first_name;}?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-user"></span>
                            </div>
                            <input type="text" name="last_name" Placeholder="Last Name *" tabindex="1" value="<?php if($this->session->userdata('ulast_name')){echo $this->session->userdata('ulast_name');}else{echo $user_details[0]->last_name;}?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-envelope"></span>
                            </div>
                            <input type="email" name="email" Placeholder="Email ID *" tabindex="2" value="<?php if($this->session->userdata('uemail')){echo $this->session->userdata('uemail');}else{echo $user_details[0]->email_id;}?>" required>
                        </div>
                    </div>
                    <!--<div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-envelope"></span>
                            </div>
                            <input type="text" name="registration_code" Placeholder="Login Code *" tabindex="3" required>
                        </div>
                    </div>-->

                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-earphone"></span>
                            </div>
                            <input type="tel" name="mobile" Placeholder="Mobile Number *" maxlength="10" tabindex="6" value="<?php if($this->session->userdata('umobile')){echo $this->session->userdata('umobile');}else{echo $user_details[0]->mobile_no;}?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span class="fa fa-calendar"></span>
                            </div>
                            <?php if($user_details[0]->dob!='0000-00-00'){
                                $bdate=$user_details[0]->dob;
                            }else{
                                $bdate="";
                            }
                            ?>
                            <input type="text" name="dob" Placeholder="Birth Date" tabindex="12" value="<?php if($this->session->userdata('udob')){echo $this->session->userdata('udob');}else{echo $bdate;}?>" id="datepicker">
                        </div>
                    </div>



                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-map-marker"></span>
                            </div>
                            <div class="form_arrow">
                                <select name="state" id="state" tabindex="7" required class="form-select ">
                                    <option value="" >Select Your State *</option>
                                    <?php foreach($states as $row)
                                    {
                                        ?>
                                        <option value="<?php echo $row->state_id;?>" <?php
                                            if($this->session->userdata('ustate')){if($row->state_id==$this->session->userdata('ustate')){echo "selected";}}else{
                                                 if($row->state_id==$user_details[0]->state_id){echo "selected";}}
                                            ?> ><?php echo ucwords(strtolower($row->state_name));?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-map-marker"></span>
                            </div>
                            <div class="form_arrow" id="city">
                            <?php  
if(!$this->session->userdata('city_id')){
                              $sessiondata = array(
                                'city_id' => $user_details[0]->city_id
                            );
                            $this->session->set_userdata($sessiondata);
}?>
                                <select name="city" id="" tabindex="8" required class="form-select ">
                                    <option value="">Select Your City *</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-check"></span>
                            </div>
                            <div class="form_arrow">
                                <select name="user_type" id="" tabindex="9" required class="form-select ">
                                    <option value="">Select Your Status *</option>
                                    <option value="Consultant" <?php if($this->session->userdata('uuser_type')){if($this->session->userdata('uuser_type')=='Consultant'){echo "selected";}}else{if($user_details[0]->user_type=='Consultant'){echo "selected";}}?>>Consultant</option>
                                    <option value="Post graduates" <?php if($this->session->userdata('uuser_type')){if($this->session->userdata('uuser_type')=='Post graduates'){echo "selected";}}else{if($user_details[0]->user_type=='Post graduates'){echo "selected";}}?>>Post graduates</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span class="fa fa-edit"></span>
                            </div>
                            <input type="text" name="registration_no" Placeholder="Registration Number *" tabindex="10" value="<?php if($this->session->userdata('uregistration_no')){echo $this->session->userdata('uregistration_no');}else{echo $user_details[0]->registration_no;}?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                            <input type="text" name="usi_no" Placeholder="USI Number" value="<?php if($this->session->userdata('uusi_no')){echo $this->session->userdata('uusi_no');}else{echo $user_details[0]->usi_no;}?>" tabindex="11">
                        </div>
                    </div>


                    <div class="submit-btn clearfix">
                        <input type="reset" class="floatL" id="reset">
                        <input type="submit" name="submit" class="floatL" Value="Submit">
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>