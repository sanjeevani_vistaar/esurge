<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 10:37 AM
 */
?>
<script type="text/javascript" src="<?php echo base_url('assets/front/js/webcastvideo.js');?>"></script>
<div class="col-md-9 total-news">	
<div class="main-title-head">
        <h3>Webcast</h3>
        <div class="clearfix"></div>
    </div>			
					<div class="video-webcast floatL " id="video-webcast1">						
						<a href="#" data-toggle="modal" data-target="#webcastvideo1" data-backdrop="static" data-keyboard="false">
							<img src="<?php echo base_url('uploads/webcast/images/DrVijayKulkarni-30thOct-2015.jpg');?>" alt="">
							<h4>Dr. Vijay Kulkarni 30-Oct-2015</h4>
						</a>								
					</div>
					<!-- Modal HTML -->
					<div class="modal fade" id="webcastvideo1">
						<div class="modal-dialog">
							<div class="modal-content">
							  
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" >
								  <span aria-hidden="true">&times;</span>
								</button>
								<h4 class="modal-title">Dr. Vijay Kulkarni 30-Oct-2015</h4>
							  </div>
							  
							  <div class="modal-body">											
								<video width="100%" id="webcastmodalvideo1" controls>
									<source src="<?php echo base_url('uploads/webcast/DrVijayKulkarni-30thOct 2015.mp4');?>" type="video/mp4">
								</video>												
							  </div>
							</div>
						</div>
					</div>	
					
					<div class="video-webcast floatL " id="video-webcast2">						
						<a href="#" data-toggle="modal" data-target="#webcastvideo2" data-backdrop="static" data-keyboard="false">
							<img src="<?php echo base_url('uploads/webcast/images/DrHemantPathak-12thJune2015.jpg');?>" alt="">
							<h4>Dr. Hemant Pathak 12-June-2015</h4>
						</a>
					</div>
					<!-- Modal HTML -->
					<div class="modal fade" id="webcastvideo2">
						<div class="modal-dialog">
							<div class="modal-content">
							  
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" >
								  <span aria-hidden="true">&times;</span>
								</button>
								<h4 class="modal-title">Dr. Hemant Pathak 12-June-2015</h4>
							  </div>
							  
							  <div class="modal-body">											
								<video width="100%" id="webcastmodalvideo2" controls>
									<source src="<?php echo base_url('uploads/webcast/DrHemantPathak-12thJune2015.mp4');?>" type="video/mp4">
								</video>												
							  </div>
							</div>
						</div>
					</div>
					
					
				
				</div>
