<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/30/2015
 * Time: 10:38 AM
 */
?>
<script>
    $(document).ready(function(){
        $("#category").change(function(){
            var category_id=$("#category").val();
                $.ajax({
                    url: "<?php echo base_url('videos/get_videos');?>",
                    type: "POST",
                    data: {id: category_id},
                    success: function (data) {
                        $("#cat_video").html(data);
                    }
                });
        })
    });
</script>
<div class="col-md-9 total-blog">
    <div class="main-title-head">
        <h3>Videos</h3>
<div class="video-filter floatR">
            <ul>
                <li>Category : </li>
                <li>

                    <select class="form-select" id="category">
                        <option value="all">All Categories</option>
                        <?php foreach($categories as $row)
                        {
                            ?>
                            <option value="<?php echo $row->cat_id;?>"><?php echo ucwords(strtolower($row->cat_name));?></option>
                        <?php } ?>
                    </select>

                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>

<div class="video-grid" id="cat_video">
        <?php if(!empty($all_videos)){
        foreach($all_videos as $row)
        {
        ?>
        <div class="video-main-container floatL" id="video-main-container3">
           <?php if(!$this->session->userdata('user_id')){?>
            <a href="javascript:void(0);" onclick='$.ChkLogin()' >
            <?php }else{?>
            <a href="javascript:void(0);" onclick='$.videoLink="<?php echo base_url($row->vid_path);?>";$.videoTitle="<?php echo $row->vid_title;?>";$.VideoPlay()'>
            <?php }?>
                <img src="<?php echo base_url($row->img_path);?>" alt="">
                <h4>
					<?php 
					$title = substr($row->vid_title,0,45);
					echo $title."...";?>
				</h4>
            </a>
        </div>
        <?php }?>
<!--            <div class="clearfix"></div>-->
<!--            <div class="clearfix btvinpagination">-->
<!---->
<!--                --><?php //echo $ui_pagging; ?>
<!---->
<!--            </div>-->
			<div class="clearfix"></div>
           <?php }else{
            echo "No Records Found";
        }
        ?>
    </div>
        
</div>