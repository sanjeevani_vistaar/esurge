<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/21/2015
 * Time: 3:49 PM
 */
?>
<div class="col-md-9 total-news">
<div class="main-title-head">
        <h3>Change Password</h3>
        <div class="clearfix"></div>
    </div>
<div class="regi-content">
    
    <div class="row">

       
        <form name="registration_frm" action="<?php echo base_url('index.php/profile/update_password');?>" method="post" class="regisform">
            <div class="col-md-12">
                  <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-lock"></span>
                            </div>
                            <input type="password" name="password" Placeholder="Password" tabindex="4" required>
                        </div>
                    </div>
               <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-lock"></span>
                            </div>
                           <input type="password" name="conf-password" Placeholder="Confirm Password" tabindex="5" required>
                        </div>
                    </div>
                <div class="submit-btn clearfix">
                        <input type="reset" class="floatL" id="reset">
                        <input type="submit" name="submit" class="floatL" Value="Submit">
                    </div>
                
            </div>
        </form>
    </div>
</div>
</div>
