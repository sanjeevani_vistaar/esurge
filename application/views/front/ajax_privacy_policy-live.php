<div style="line-height: 1.5; margin: 11px 10px 40px;text-align: justify;">
    <p>We at <a href="http://www.esurge.in">www.esurge.in</a> (a urology education initiative by Sun Pharma Laboratories Ltd.),
        understand & value your personal information. This privacy policy reflects the kind of information
        we may want to collect & how it may be used for various purposes.<br>
        There are two types of information that we may collect:<br><br>
        A. Personal Information: This information includes Name, Email id, State, City, Birthdate, Mobile
        Number and USI Number etc.<br>
        B. Non-Personal Information: This information is related to user activities while surfing
        information on Net & can be captured using some analytics codes, cookies etc.<br><br>
        In our constant effort to provide best possible user experience, we will have to capture some
        information, which is non-personal in nature. E.g. Total visitors to website, total time spent on
        website, flow of user navigation, etc. This information is essential to understanding user
        behavior on our website which will culminate in an attempt to make the website more user
        friendly. We may share this info with any third party with sole intention of analysis.<br>
        We may place cookies which will further help us in tracking user activity. It is a common practice
        to use cookies by majority of websites. User can decide about deleting these cookies.<br>
        Tracking-non personalized data depends on the website objectives & may defer from site to
        site. From time-to-time, we may announce changes to the above policy with the intent to
        improve user experience.<br>
        Should you have any concerns, please write to us at <a href="mailto:privacy@sunpharma.com">privacy@sunpharma.com</a><br><br>
        <b>By registering with us, you agree to receive an SMS to verify your mobile number.</b>
    </p>

</div>