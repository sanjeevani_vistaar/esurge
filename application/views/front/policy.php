<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 11:09 AM
 */
?>
<div class="col-md-9 total-blog">
    <div class="main-title-head">
        <h3>Privacy Policy</h3>
        <div class="clearfix"></div>
    </div>
    <div class="disclaimer-wrapper">
        <?php echo $privacy_policy[0]->content_text;?>
    </div>
</div>
