<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/30/2015
 * Time: 10:38 AM
 */
?>
<link rel='stylesheet prefetch' href="<?php echo base_url('assets/front/css/photoswipe.css');?>">
<link rel='stylesheet prefetch' href="<?php echo base_url('assets/front/css/default-skin.css');?>">

<style>
    .my-gallery {
        width: 100%;
        float: left;
    }
    .my-gallery img {
        width: 100%;
        height: auto;
    }
    .my-gallery figure {
        display: block;
        float: left;
        margin: 0 5px 5px 0;
        width: 150px;
    }
    .my-gallery figcaption {
        display: none;
    }
</style>
<div class="col-md-9 total-blog">
    <div class="main-title-head">
        <h3>Images</h3>
        <div class="clearfix"></div>
    </div>
    <?php $login_required = is_login_required($this->router->fetch_class());?>



<div class="my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
    <?php if(!empty($all_images)) {
        foreach ($all_images as $row) {
            ?>
            <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                <a href="<?php echo base_url($row->img_path);?>" itemprop="contentUrl" data-size="1024x1024">
                    <img src="<?php echo base_url($row->img_path);?>" itemprop="thumbnail" alt="Image description"/>
                </a>
                <figcaption itemprop="caption description">
                    <?php
                    $title = substr($row->img_title,0,45);
                    echo $title."...";?>
                </figcaption>

            </figure>
        <?php }
    }else{
        echo "No Records Found";
    }?>
</div>
    </div>
<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe.
         It's a separate element, as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
        <!-- don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>
</div>
<script src="<?php echo base_url('assets/front/js/photoswipe.min.js');?>"></script>
<script src="<?php echo base_url('assets/front/js/photoswipe-ui-default.min.js');?>"></script>
<script src="<?php echo base_url('assets/front/js/gall.js');?>"></script>