<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 1:55 PM
 */
?>
<!DOCTYPE html>
<html>
<head>
<?php header("Content-Type: text/html; charset=utf-8");?>
<?php
$seo_details=get_seo_details($this->router->fetch_class());
if($this->router->fetch_method()=='news_details')
{
    $seo_news_details=get_news_seo_details($this->uri->segment('3'));
}else{
    $seo_news_details=0;
}
if($seo_news_details){
    if($seo_news_details->seo_title){
        $seo_title=$seo_news_details->seo_title;
    }else{
       $seo_title=$seo_details->seo_title;
    }
    if($seo_news_details->seo_keywords){
        $seo_keywords=$seo_news_details->seo_keywords;
    }else{
        $seo_keywords=$seo_details->seo_keywords;
    }
    if($seo_news_details->seo_desc){
        $seo_desc=$seo_news_details->seo_desc;
    }else{
        $seo_desc=$seo_details->seo_desc;
    }
    ?>
<title><?php echo $seo_title;?></title>
<meta name="keywords" content="<?php echo $seo_keywords;?>" />
<meta name="description" content="<?php echo $seo_desc;?>" />
<?php
}
else if($seo_details){?>
<title><?php echo $seo_details->seo_title;?></title>
<meta name="keywords" content="<?php echo $seo_details->seo_keywords;?>" />
<meta name="description" content="<?php echo $seo_details->seo_desc;?>" />
<?php }else{ ?>
<title>Esurge - Urology education initiative by Sun Pharma</title>
<meta name="keywords" content="esurge,Urology education,Urology,education,webcast,videos,uro,uro slides" />
<meta name="description" content="Esurge - Urology education initiative by Sun Pharma" />
<?php }?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?php echo base_url('assets/front/css/bootstrap.css');?>" rel='stylesheet' type='text/css' />
<link href="<?php echo base_url('assets/front/css/main.css');?>" rel='stylesheet' type='text/css' />
<script src="<?php echo base_url('assets/front/js/jquery.min.js');?>"></script>
</head>
<body>
<div class="container">
<div class="news-paper">
<div class="header">
<div class="header-left">
<div class="logo">
<a href="<?php echo base_url();?>">
<img src="<?php echo base_url('assets/front/images/logo.png');?>" alt="esurge" width="268" height="65" class="img-responsive"/>
</a>
</div>
</div>
<div class="clearfix"></div>
<div class="header-right">
<?php if($this->router->fetch_class()!='search'){$this->session->unset_userdata('tags');}?>
<div class="search">
<form action="<?php echo base_url('search');?>" method="post">
<input placeholder="Search..." type="text"  name="tags" value="<?php echo $this->session->userdata('tags');?>" required/>
<input type="submit" value="" name="submit">
</form>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
<div id="menuzord" class="menuzord red">
<!--<a href="javascript:void(0)" class="menuzord-brand">Menu</a>-->
<ul class="menuzord-menu">
<?php if (!$this->session->userdata('user_id')) {
?>
<li class="hideLogin"><a href="javascript:void(0);" onclick='$.ChkLogin()'>Log&nbsp;&nbsp;&nbsp In</a></li>
<?php }else{?>
<li class="hideLogin">
<a href="<?php echo base_url('profile/update_profile'); ?>">Update&nbsp;&nbsp;&nbsp Profile</a>
</li>
<li class="hideLogin">
<a href="<?php echo base_url('profile/update_password'); ?>">Change &nbsp;&nbsp;&nbspPassword</a>
</li>
<li class="hideLogin"><a href="<?php echo base_url('profile/logout'); ?>">Logout</a></li>

<?php }?>
<li><a href="<?php echo base_url('index.php/home');?>">Home</a></li>
<?php $active_menu=get_active_menu();
$pg=0;
$pg_icon=0;
$pg_contri=0;
foreach($active_menu as $menu){
if($menu->menu_title=='pg_icon'){
$pg_icon=1;
$pg=1;
}elseif($menu->menu_title=='pg_contri'){
$pg_contri=1;
$pg=1;
}else{
$menuTitle=$menu->menu_title;
if($menuTitle=='uro'){
$menuTitle='Uro&nbsp;&nbsp;Slides&nbsp;&nbsp;Share';
}
?>
<li><a href="<?php echo base_url($menu->menu_title);?>"> <?php echo ucwords($menuTitle);?></a></li>
<?php }
}
if($pg==1){?>
<li><a href="#">PG</a>
<ul class="dropdown">
<?php if($pg_icon){?>
<li><a href="<?php echo base_url('pg_icon');?>">Meet&nbsp;&nbsp;&nbsp;the&nbsp;&nbsp;&nbsp;Icon</a></li>
<?php }?>
<?php if($pg_contri){?>
<li><a href="<?php echo base_url('pg_contri');?>"> Academician's&nbsp;&nbsp;&nbsp;Contribution</a></li>
<?php }?>
</ul>
</li>
<?php }
?>
</ul>
</div>
<div class="clearfix"></div>
<?php if(validation_errors()){?>
<div class="alert alert-danger text-center">
<?php print_r(validation_errors());?>
</div>
<?php } ?>
<?php if ($this->session->flashdata('err_msg')) { ?>
<div class="alert alert-danger text-center">
<?php echo $this->session->flashdata('err_msg'); ?>
</div>
<?php } ?>
<?php if ($this->session->flashdata('succ_msg')) { ?>
<div class="alert alert-success text-center">
<?php echo $this->session->flashdata('succ_msg'); ?>
</div>
<?php } ?>
<div class="main-content">
<?php $this->load->view($middle);
//echo  $this->router->fetch_class();
if($this->router->fetch_method()!='forgot_password') {
    if ($this->router->fetch_method() != 'verify_sms_form') { ?>
        <div class="col-md-3 side-bar">
            <?php if ($this->router->fetch_class() != 'registration') {
                if (!$this->session->userdata('user_id')) {
                    ?>
                    <div class="sign_up">
                        <!--<h3>Login/Register</h3>-->
                        <div class="form">
                            <div class="tab-group">
                                <div class="tab">Log In</div>
                            </div>
                            <div id="login">
                                <form action="<?php echo base_url('login'); ?>" method="post">
                                    <div class="field-wrap">
                                        <input type="email" required name="email_id" autocomplete="off"
                                               placeholder="Email"/>
                                    </div>
                                    <div class="field-wrap">
                                        <input type="password" required name="password" autocomplete="off"
                                               placeholder="Password"/>
                                    </div>
                                    <button type="submit" name="submit" value="Login" class="button button-block"/>
                                    Submit</button>
                                    <p class="forgot"><a href="<?php echo base_url('login/forgot_password'); ?>">Forgot
                                            Password?</a></p>

                                    <div class="register-group">
                                        <div class="tabregister">
                                            <a href="<?php echo base_url('registration'); ?>">New User? Register
                                                Here</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /form -->
                    </div>
                <?php } else {
                    ?>
                    <div class="sign_up logged_in">
                        <!--<h3>Login/Register</h3>-->
                        <div class="form">
                            <div class="tab-group">
                                <div class="tab">Welcome <?php echo $this->session->userdata('user_name'); ?></div>
                            </div>
                            <div id="login">
                                <div class='welcome'>
                                    <a href="<?php echo base_url('profile/update_profile'); ?>"><p
                                            class="welcome-profile">Update Profile</p></a>
                                    <a href="<?php echo base_url('profile/update_password'); ?>"><p
                                            class="welcome-profile">Change Password</p></a>
                                    <a href="<?php echo base_url('profile/logout'); ?>"><p class="welcome-profile">
                                            Logout</p></a>
                                </div>
                            </div>
                        </div>
                        <!-- /form -->
                    </div>
                <?php }
            }
            ?>
            <div class="clearfix"></div>
            <?php if($other_video){?>
            <div class="page-content">
                <!-- BEGIN .content-block -->
                <div class="content-block">
                    <h3 class="block-title">Other Videos </h3>
                    <?php foreach ($other_video as $row) {
                    if (!$this->session->userdata('user_id')) {
                    ?>
                    <a href="javascript:void(0);" onclick='$.ChkLogin()' alt="<?php echo $row->vid_title; ?>">
                        <?php } else {
                        if ($row->vid_path){
                        ?>
                        <a href="javascript:void(0);" alt="<?php echo $row->vid_title; ?>"
                           onclick='$.videoLink="<?php echo base_url($row->vid_path); ?>";$.videoTitle="<?php echo $row->vid_title; ?>";$.trackingLink="<?php echo base_url('tracking'); ?>";$.videoSource="Other video";$.VideoPlay()'>
                            <?php }
                            if ($row->vid_link){
                            ?>
                            <a href="javascript:void(0);" alt="<?php echo $row->vid_title; ?>"
                               onclick='$.videoLink="<?php echo $row->vid_link; ?>";$.videoTitle="<?php echo $row->vid_title; ?>";$.trackingLink="<?php echo base_url('tracking'); ?>";$.videoSource="Other video";$.UrlVideoPlay()'>
                                <?php }
                                }
                                }
                            ?>
                            <img src="<?php echo base_url($row->img_path);?>" width="100%">
                    <div class="readBtn">
                        <a href="<?php echo base_url('othervideos'); ?>">More Videos >></a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <?php }?>
            <div class="page-content">
                <!-- BEGIN .content-block -->
                <div class="content-block">
                    <h3 class="block-title">Events </h3>
                    <?php $login_required = is_login_required('events'); ?>
                    <ul class="event-list">
                        <?php foreach ($event_details as $row) {
                            $date_split = explode(' ', $row->start);
                            $date = $date_split[0];
                            $time = strtotime($date);
                            $year = date("Y", $time);
                            $month = date("M", $time);
                            $day = date("j", $time);
                            $end_date_split = explode(' ', $row->end);
                            $end_date = $end_date_split[0];
                            $end_time = strtotime($end_date);
                            $end_year = date("Y", $end_time);
                            $end_month = date("M", $end_time);
                            $end_day = date("j", $end_time);
                            ?>
                            <!-- BEGIN .event-wrapper -->
                            <li class="event-wrapper clearfix">
                                <div class="event-date">
                                    <div class="event-m"><?php echo $month; ?><br><?php echo $year; ?></div>
                                    <div class="event-d"><?php echo $day . " - " . $end_day; ?></div>
                                </div>
                                <div class="event-info">
                                    <?php $url = parse_url($row->link);
                                    if (in_array("http", $url)) {
                                        $link = $row->link;
                                    } elseif (in_array("https", $url)) {
                                        $link = $row->link;
                                    } else {
                                        $link = "http://" . $row->link;
                                    } ?>
                                    <h4>
                                        <?php if ($login_required) {
                                            if (!$this->session->userdata('user_id')) { ?>
                                                <a href="<?php echo base_url('events'); ?>"><?php echo $row->title; ?></a>
                                                <?php
                                            } else { ?>
                                                <a target="_blank"
                                                   href="<?php echo $link; ?>"><?php echo $row->title; ?></a>
                                            <?php }
                                        } else { ?>
                                            <a target="_blank"
                                               href="<?php echo $link; ?>"><?php echo $row->title; ?></a>
                                        <?php } ?>
                                    </h4>

                                    <p><?php echo $row->location; ?></p>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                    <div class="readBtn">
                        <a href="<?php echo base_url('events'); ?>">Read More >></a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <?php if($quiz_cat){?>
                <div class="page-content">
                    <!-- BEGIN .content-block -->
                    <div class="content-block">
                        <h3 class="block-title">Quiz </h3>
                        <?php foreach ($quiz_cat as $row) {?>
                            <div class="grid box">
                                <div class="grid-header">
                                    <a href="javascript:void(0);" onclick='$.quizLink="<?php echo base_url('puzzle/question/'.$row->cat_id); ?>";$.quizTitle="<?php echo $row->cat_name; ?>";$.QuizPlay()' class="title"><?php echo $row->cat_name;?></a>
                                </div>
                            </div>
                       <?php }?>

                                    <div class="readBtn">
                                        <a href="<?php echo base_url('puzzle'); ?>">More Quiz >></a>
                                    </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            <?php }?>
            <div class="clearfix"></div>
            <?php if($polls_cat){?>
                <div class="page-content">
                    <!-- BEGIN .content-block -->
                    <div class="content-block">
                        <h3 class="block-title">Polls </h3>
                        <?php foreach ($polls_cat as $row) {?>
                            <div class="grid box">
                                <div class="grid-header">
                                    <a href="javascript:void(0);" onclick='$.quizLink="<?php echo base_url('polls/question/'.$row->cat_id); ?>";$.quizTitle="<?php echo $row->cat_name; ?>";$.QuizPlay()' class="title"><?php echo $row->cat_name;?></a>
                                </div>
                            </div>
                        <?php }?>

                        <div class="readBtn">
                            <a href="<?php echo base_url('polls'); ?>">More Polls >></a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            <?php }?>
        </div>
    <?php }
}?>
</div>
</div>
</div>
<!--==============================
<!--=========== Start Footer SECTION ================-->
<footer id="footer">
<!-- Start Footer Top -->
<div class="footer-top">
<div class="container">
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6">
<div class="single-footer-widget">
<div class="section-heading">
<h2>About &nbsp;&nbsp; Us</h2>
<div class="line"></div>
</div>
<p>So what's Esurge all about? Esurge is a dedicated platform for Urologists like you. It's your source to the latest medical updates. It's your access to world-class medical journals. It's your medical video-library. It's your events calendar. <br>
Esurge is all about helping you help your patients.</p>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6">
<div class="bottom-menu">
<ul>
<?php foreach($active_menu as $menu){
$menuTitle=$menu->menu_title;
if($menuTitle=='uro'){
$menuTitle="Uro&nbsp;&nbsp;&nbsp;Slides&nbsp;&nbsp;&nbsp;Share";
}if($menuTitle=='pg_icon'){
$menuTitle="Meet&nbsp;&nbsp;&nbsp;The&nbsp;&nbsp;&nbsp;Icon";
}if($menuTitle=='pg_contri'){
$menuTitle="Academician's&nbsp;&nbsp;&nbsp;Contribution";
}
?>
<li><a href="<?php echo base_url($menu->menu_title);?>"> <?php echo ucwords($menuTitle);?></a></li>
<?php  }?>

<li><a href="<?php echo base_url('contact_us');?>">Contact&nbsp;&nbsp;&nbsp;Us</a></li>
<li><a href="<?php echo base_url('disclaimer');?>">Disclaimer </a></li>
<li><a href="<?php echo base_url('policy');?>"> Privacy&nbsp;&nbsp;&nbsp;Policy </a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<!-- Start Footer Middle-->
<div class="footer-middle">
<div class="container">
<div class="row">
<div class="footerLeft">
<div class="footer-copyright">
<a href="<?php echo base_url();?>">
<img class="img-responsive floatL" src="<?php echo base_url('assets/front/images/sunlogo.png');?>" width="43" height="60" alt="esurge"/>
</a>
<p class="floatL">&copy; Copyright 2015</p>
</div>
</div>
</footer>
<div style="font-size:12px;text-align:center;padding:0 0 10px 0;background-color:#ffffff;color:#000000 !important;">Website is best viewed in IE 9 and onwards version, Chrome, Mozilla Firefox and Safari.</div>
<div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" >
<span aria-hidden="true">&times;</span>
</button>
<h4 class="modal-title"></h4>
</div>
<div class="modal-body">
<video width="100%" id="video" controls="controls">
<source>
</video>
</div>
</div>
</div>
</div>
<!-- embedded -->
<div class="modal fade" id="myEmbModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="youtubeclose close" data-dismiss="modal" >
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="Embmodal-title"></h4>
            </div>
            <div class="Embmodal-body">

            </div>
        </div>
    </div>
</div>
<!-- embedded end -->
<div class="modal fade" id="quizModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" >
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="quizmodal-title"></h4>
            </div>
            <div class="quizmodal-body">

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="LoginModal">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" >
<span aria-hidden="true">&times;</span>
</button>
<h4 class="modal-title"></h4>
</div>
<div class="modal-body" style="height: 210px;">
<div id="login">
<form action="<?php echo base_url('login'); ?>" method="post">
<div class="field-wrap">
<input type="email" required name="email_id" autocomplete="off" placeholder="Email"/>
</div>
<div class="field-wrap">
<input type="password" required name="password" autocomplete="off" placeholder="Password"/>
</div>
<button type="submit" name="submit" value="Login" class="button button-block">Submit</button>
<p class="forgot"><a href="<?php echo base_url('login/forgot_password'); ?>">Forgot Password?</a>
</p>
<div class="register-group">
<div class="tabregister">
<a href="<?php echo base_url('registration'); ?>">New User? Register Here</a>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
<script src="<?php echo base_url('assets/front/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/common/js/bootstrap-dialog.min.js');?>" type="text/javascript"></script>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="<?php echo base_url('assets/common/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url('assets/front/js/responsiveslides.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/front/js/main.js');?>"></script>
<script>$(document).ready(function(){$('.form').find('input, textarea').on('keyup blur focus',function(e){var $this=$(this),label=$this.prev('label');if(e.type==='keyup'){if($this.val()===''){label.removeClass('active highlight');}else{label.addClass('active highlight');}}else if(e.type==='blur'){if($this.val()===''){label.removeClass('active highlight');}else{label.removeClass('highlight');}}else if(e.type==='focus'){if($this.val()===''){label.removeClass('highlight');}
else if($this.val()!==''){label.addClass('highlight');}}});$('.tab a').on('click',function(e){e.preventDefault();$(this).parent().addClass('active');$(this).parent().siblings().removeClass('active');target=$(this).attr('href');$('.tab-content > div').not(target).hide();$(target).fadeIn(600);});
        $.VideoPlay=function(){
            $.ajax({
                url: $.trackingLink,
                type: "POST",
                data: {page_view: $.videoSource,page_url: $.videoTitle},
                success: function (data) {
                }
            })
            $('#myModal').modal('show');
            $('.modal-title').text($.videoTitle);
            $('.modal-body video source').attr('src',$.videoLink);
            $('.modal-body video source').attr('type',"video/mp4");
            $(".modal-body video")[0].load();
            var video=document.getElementById("video");
            video.play();
            video.currentTime=0;
        }
        $.EmbVideoPlay=function(){
            $.ajax({
				url: "<?php echo base_url('webcast/get_emb_code');?>",
				type: "POST",
				data: {id: $.videoLink},
				success: function (data) {
                    $('#myEmbModal').modal('show');
                    $('.Embmodal-title').text($.videoTitle);
                    $('.Embmodal-body').html(data);
                    $("iframe").width(598);
				}
			});
        }
        $.UrlVideoPlay=function(){
            $.ajax({
                url: "<?php echo base_url('webcast/get_url');?>",
                type: "POST",
                data: {id: $.videoLink},
                success: function (data) {
                    $.ajax({
                        url: $.trackingLink,
                        type: "POST",
                        data: {page_view: $.videoSource,page_url: $.videoTitle},
                        success: function (data) {
                        }
                    })
                    $('#myEmbModal').modal('show');
                    $('.Embmodal-title').text($.videoTitle);
                    $('.Embmodal-body').html(data);
                }
            });
        }
        $.QuizPlay=function(){
            $.ajax({
                url: $.quizLink,
                type: "GET",
                data: {},
                success: function (data) {
                    $('#quizModal').modal('show');
                    $('.quizmodal-title').text($.quizTitle);
                    $('.quizmodal-body').html(data);

                }
            })
        }
        $('.close').click(function(){video.pause();});$('.youtubeclose').click(function(){$("iframe").attr("src","");});$('.alert-danger').delay(5000).fadeOut();$('.alert-success').delay(5000).fadeOut();$("#datepicker").datepicker({format:'yyyy-mm-dd'});$.ChkLogin=function(){$('#LoginModal').modal('show');$('.modal-title').text('Log In');}});</script>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create','UA-73152369-1','auto');ga('send','pageview');</script>
</body>
</html>