<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 1:55 PM
 */
?>
<!DOCTYPE html>
<html>
<head>
    <title>Esurge - Urology education initiative by Sun Pharma</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="The News Reporter Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <link href="<?php echo base_url('assets/front/css/bootstrap.css');?>" rel='stylesheet' type='text/css' />
    <link href="<?php echo base_url('assets/front/css/bootstrap-theme.css');?>" rel='stylesheet' type='text/css' />
    <!-- Custom Theme files -->
    <link href="<?php echo base_url('assets/front/css/normalize.css');?>" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo base_url('assets/front/css/menuzord.css');?>" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo base_url('assets/front/css/style.css');?>" rel="stylesheet" type="text/css" media="all" />
    <!-- Custom Theme files -->
    <script src="<?php echo base_url('assets/front/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/front/js/bootstrap.min.js');?>"></script>
 <link href="<?php echo base_url('assets/admin/css/fullcalendar/fullcalendar.css');?>" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url('assets/admin/js/plugins/fullcalendar/fullcalendar.min.js');?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/common/js/bootstrap-dialog.min.js');?>" type="text/javascript"></script>
    
    <!-- Custom Theme files -->

    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfont-->
    <script src="<?php echo base_url('assets/common/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready( function() {
            $('.alert-danger').delay(5000).fadeOut();
            $('.alert-success').delay(5000).fadeOut();
            $("#datepicker").datepicker({
                format: 'dd-mm-yyyy'
            });
        });
    </script>
<style>
.fc-event-time{
   display : none;
}
</style>
</head>
<body>
<!-- header-section-starts -->
<div class="container">
    <div class="news-paper">
        <div class="header">
            <div class="header-left">
                <div class="logo">
                    <a href="<?php echo base_url();?>">
                        <img src="<?php echo base_url('assets/front/images/logo.png');?>" alt="logo"/>
                        <!--<span>Urology education initiative by Sun Pharma</span>-->
                    </a>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="header-right">
                <div class="search">
                    <form>
                        <input placeholder="Search..." type="text" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}"/>
                        <input type="submit" value="">
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div id="menuzord" class="menuzord red">
            <!--<a href="javascript:void(0)" class="menuzord-brand">Menu</a>-->
            <ul class="menuzord-menu">
               
                <li><a href="<?php echo base_url('index.php/journals');?>"> Journals</a></li>
                <li><a href="<?php echo base_url('index.php/videos');?>">Videos</a></li>
               
                <li><a href="<?php echo base_url('index.php/events');?>">Events</a></li>
                <li><a href="#">Gallery</a></li>
                <li><a href="<?php echo base_url('index.php/news');?>">News</a></li>
              
                <li><a href="<?php echo base_url('index.php/webcast');?>">Webcast</a></li>
              
                <li><a href="<?php echo base_url('index.php/uro');?>">Uro&nbsp;&nbsp;Slide&nbsp;&nbsp;Share</a></li>
                <!--<li><a href="#">PG</a>
                    <ul class="dropdown">
                        <li><a href="#">Meet the Icon</a></li>
                        <li><a href="#"> Academicins Contribution</a></li>
                        <li><a href="#"> Quiz</a></li>
                    </ul>
                </li>-->
            </ul>
        </div>
        <div class="clearfix"></div>
<?php if ($this->session->flashdata('err_msg')) { ?>
                                        <div class="alert alert-danger text-center">
                                            <?php echo $this->session->flashdata('err_msg'); ?>
                                        </div>
                                    <?php } ?>
                                     <?php if ($this->session->flashdata('succ_msg')) { ?>
                                        <div class="alert alert-success text-center">
                                            <?php echo $this->session->flashdata('succ_msg'); ?>
                                        </div>
                                    <?php } ?>
        <div class="main-content">
        <?php $this->load->view($middle);

       //echo  $this->router->fetch_class();
        if($this->router->fetch_method()!='forgot_password'){?>
            <div class="col-md-3 side-bar">
                <?php if($this->router->fetch_class()!='registration') {
                    if (!$this->session->userdata('user_id')) {
                        ?>
                        <div class="sign_up">
                            <!--<h3>Login/Register</h3>-->
                            <div class="form">
                                <div class="tab-group">
                                    <div class="tab">Log In</div>
                                </div>

                                <div id="login">
                                    
                                    <form action="<?php echo base_url('index.php/login'); ?>" method="post">
                                        <div class="field-wrap">
                                            <input type="email" required name="email_id" autocomplete="off"
                                                   placeholder="Email"/>
                                        </div>
                                        <div class="field-wrap">
                                            <input type="password" required name="password" autocomplete="off"
                                                   placeholder="Password"/>
                                        </div>
                                        <button type="submit" name="submit" value="Login" class="button button-block"/>
                                        Submit</button>
                                        <p class="forgot"><a href="<?php echo base_url('index.php/login/forgot_password'); ?>">Forgot
                                                Password?</a></p>

                                        <div class="register-group">
                                            <div class="tabregister">
                                                <a href="<?php echo base_url('index.php/registration'); ?>">New User? Register
                                                    Here</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /form -->
                        </div>
                    <?php }else{?>
                        <div class="sign_up">
                            <!--<h3>Login/Register</h3>-->
                            <div class="form">
                                <div class="tab-group">
                                    <div class="tab">Welcome <?php echo $this->session->userdata('user_name');?></div>
                                </div>

                                <div id="login">
                                    
<div class='welcome'>
                                        <p class="welcome-profile"><a href="<?php echo base_url('index.php/profile/update_profile'); ?>">Update Profile</a></p>
                                        <p class="welcome-profile"><a href="<?php echo base_url('index.php/profile/update_password'); ?>">Change Password</a></p>
                                        <p class="welcome-profile"><a href="<?php echo base_url('index.php/profile/logout'); ?>">Logout</a></p>
</div>


                                </div>
                            </div>
                            <!-- /form -->
                        </div>
                    <?php }
                }
                ?>

                <div class="clearfix"></div>
                <div class="page-content">
                    <!-- BEGIN .content-block -->
                    <div class="content-block">

                        <h3 class="block-title">Events </h3>

                        <ul class="event-list">
                            <?php foreach($event_details as $row){
                            $date_split=explode(' ',$row->start);
                            $date=$date_split[0];
                            $time=strtotime($date);
                            $year=date("Y",$time);
                            $month=date("M",$time);
                            $day=date("j",$time);
                            $end_date_split=explode(' ',$row->end);
                            $end_date=$end_date_split[0];
                            $end_time=strtotime($end_date);
                            $end_year=date("Y",$end_time);
                            $end_month=date("M",$end_time);
                            $end_day=date("j",$end_time);
                            ?>
                            <!-- BEGIN .event-wrapper -->
                            <li class="event-wrapper clearfix">

                                <div class="event-date">
                                    <div class="event-m"><?php echo $month;?><br><?php echo $year;?></div>
                                    <div class="event-d"><?php echo $day." - ".$end_day;?></div>
                                </div>

                                <div class="event-info">
                                    <h4><a target="_blank" href="<?php echo $row->link;?>"><?php echo $row->title;?></a></h4>
                                    <p><?php echo $row->location;?></p>

                                </div>
                             </li>
                            <?php }?>

                        </ul>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
</div>
<!--==============================
<!--=========== Start Footer SECTION ================-->
<footer id="footer">
    <!-- Start Footer Top -->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="single-footer-widget">
                        <div class="section-heading">
                            <h2>About Us</h2>
                            <div class="line"></div>
                        </div>
                        <p>So what's E-surge all about? E-Surge is a dedicated platform for Urologists like you. It's your source to the latest medical updates. It's your access to world-class medical journals. It's your medical video-library. It's your events calendar. <br> 
E-surge is all about helping you help your patients.</p>
                    </div>

                </div>

                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="bottom-menu">
                        <ul>

                            <li><a href="<?php echo base_url('index.php/journals');?>">Journals</a></li>
                            <li><a href="<?php echo base_url('index.php/videos');?>">Videos</a></li>

                            <li><a href="<?php echo base_url('index.php/events');?>">Events</a></li>
                            <li><a href="#">Gallery</a></li>

                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="bottom-menu">
                        <ul>
                            <li><a href="<?php echo base_url('index.php/news');?>">News</a></li>

                            <li><a href="<?php echo base_url('index.php/webcast');?>">Webcast</a></li>

                            <li><a href="<?php echo base_url('index.php/uro');?>">Uro&nbsp;Slide&nbsp;share</a></li>
                            <li><a href="<?php echo base_url('index.php/contact_us');?>">Contact Us</a></li>
                        </ul>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- Start Footer Middle -->
    <div class="footer-middle">
        <div class="container">
            <div class="row">
                <div class="footerLeft floatL">
                    <div class="footer-copyright">
                        <p>&copy; Copyright 2015
                            <a href="#">
                                <img src="<?php echo base_url('assets/front/images/sunlogo.png');?>" alt="sunlogo"/>
                            </a>
                        </p>
                    </div>
                </div>
<div class="footerLeft floatR">
                    <ul>
                        <li>
                            <a href="<?php echo base_url('index.php/disclaimer');?>">Disclaimer </a>|
                        </li>
                        <li>
                            <a href="<?php echo base_url('index.php/policy');?>"> Privacy Policy </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Custom Theme files -->
<script type="text/javascript" src="<?php echo base_url('assets/front/js/signinform.js');?>"></script>
<script type="text/javascript"  src="<?php echo base_url('assets/front/js/responsiveslides.min.js');?>"></script>
<script type="text/javascript"  src="<?php echo base_url('assets/front/js/menuzord.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/front/js/custom.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/front/js/videos.js');?>"></script>
<div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" >
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"></h4>
            </div>

            <div class="modal-body">
                <video width="100%" id="video" controls>
                    <source>
                </video>
            </div>
        </div>
    </div>
</div>
</body>
</html>