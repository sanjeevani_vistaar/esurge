<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/4/2016
 * Time: 4:31 PM
 */
?>
<div class="col-md-9 total-blog">
    <div class="content">
        <div class="main-title-head">
            <h3>Uro Slide Share</h3>
            <div class="clearfix"></div>
        </div>
        <div class="grids clearfix">
            <div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Benign6.pptx">Benign Prostatic Hyperplasia Management</a></h1>
							<h3>6th August 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>              
            </div>
            <div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						 <div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Benign29.pptx">Benign Prostatic Hyperplasia Management</a></h1>
							<h3>29th August 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div> 
            </div>
            <div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/BPH.pptx">BPH: Patient Awareness</a></h1>
							<h3>October 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>                
            </div>
            <div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						 <div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Cancer.pptx">Cancer Prostate</a></h1>
							<h3>November, 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>                  
            </div>
            <div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Detrusor.pptx">Detrusor Sphincter Dyssynergia</a></h1>
							<h3>September 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>                 
            </div>

            <div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Medical.pptx">Medical management of urolithiasis</a></h1>
							<h3>14 July 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>                  
            </div>
            <div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Complications of Laser Prostatectomy.pptx">Complications of Laser Prostatectomy</a></h1>
							<h3>&nbsp;&nbsp;</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>              
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						 <div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Current Options in CRPC.pptx">Current Options in CRPC</a></h1>
							<h3>&nbsp;&nbsp;</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>                
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Endourological procedures in Stone Disease December 2015.pptx">Endourological procedures in Stone Disease</a></h1>
							<h3> December 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>                
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						 <div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Genitourinary TB presentation November 2015.pptx">Genitourinary TB presentation </a></h1>
							<h3>November 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>               
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						 <div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Hemorrhagic Cystitis 21 October 2015.pptx">Hemorrhagic Cystitis </a></h1>
							<h3>21 October 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>              
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Male infertility 6 August 2015.pptx">Male infertility</a></h1>
							<h3>6 August 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div> 
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Molecular therapy for advanced renal cell carcinoma.pptx">Molecular therapy for advanced renal cell carcinoma</a></h1>
							<h3>&nbsp;&nbsp;</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div> 
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Nephrogenic Systemic Fibrosis.pptx">Nephrogenic Systemic Fibrosis</a></h1>
							<h3>&nbsp;&nbsp;</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>                 
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Neurogenic Bladder 22 October 2015.pptx">Neurogenic Bladder</a></h1>
							<h3>22 October 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>                 
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Pelvic urethral junction obstruction evidence presentation.pptx">Pelvic urethral junction obstruction evidence presentation</a></h1>
							<h3>&nbsp;&nbsp;&nbsp;</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>               
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						 <div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/PUJO basics presentation 12 Dec 2015.pptx">PUJO basics presentation </a></h1>
							<h3>12 Dec 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>                
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Renal Stones Patient Awareness October 2015.pptx">Renal Stones Patient Awareness </a></h1>
							<h3>October 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div> 
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						 <div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Single-session Supine Bilateral PCNL December 2015.pptx">Single-session Supine Bilateral PCNL </a></h1>
							<h3>December 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div> 
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Stone disease in pregnancy and its treatment.pptx">Stone disease in pregnancy and its treatment</a></h1>
							<h3>&nbsp;</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div> 
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Medical.pptx">Medical management of urolithiasis</a></h1>
							<h3>14 July 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div> 
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Testicular tumors Nov 2015.pptx">Testicular tumors</a></h1>
							<h3> Nov 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div> 
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Testosterone Replacement Therapy in Andropause October 2015.pptx">Testosterone Replacement Therapy in Andropause</a></h1>
							<h3> October 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div> 
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Testosterone Replacement Therapy Sep 2015 Dr Deepak Gupte.pptx">Testosterone Replacement Therapy Dr Deepak Gupte</a></h1>
							<h3>Sep 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div> 
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Tips and Tricks Hypospadias surgery 2015 Sep.pptx">Tips and Tricks Hypospadias surgery</a></h1>
							<h3> Sep 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
            </div>
			<div class="grid box journals-title">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa  fa-file-powerpoint-o "></span>
						</div>
						<div class="grid-img-content">
							<h1><a target="_blank" href="http://docs.google.com/gview?url=http://digitaswatermelon.com/esurge/uploads/uros/Urinary incontinence 18 Oct 2015.pptx">Urinary incontinence</a></h1>
							<h3>18 Oct 2015</h3>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
