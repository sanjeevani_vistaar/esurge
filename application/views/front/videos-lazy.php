<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/30/2015
 * Time: 10:38 AM
 */
?>
<script>
    $(document).ready(function(){

        $("#category").change(function(){
            $("#startindex").val(0);
            $('#marker-end').show();
            var category_id=$("#category").val();

                $.ajax({
                    url: "<?php echo base_url('videos/get_videos');?>",
                    type: "POST",
                    data: {id: category_id},
                    success: function (data) {
                        $("#cat_video").html(data);
                    }
                });
        })

        $('#marker-end').on('lazyshow', function () {
            var prev_startindex=$("#startindex").val();
            var startindex=prev_startindex*1 + <?php echo RECORD_PER_PAGE?>*1;
            var total_no_videos=$("#total_no_videos").val();
            if(prev_startindex >= total_no_videos*1-<?php echo RECORD_PER_PAGE?>*1){
                $('#marker-end').hide();
            }

            $("#startindex").val(startindex);
            var cat=$("#category").val();

            var url = "<?php echo base_url('videos/videos_list');?>";

                $.ajax({
                    url: url,
                    type: "POST",
                    data: {startindex: startindex,id:cat},
                })
                    .done(function (responseText) {
                        // add new elements
                        $('#cat_video').append(
                            $('<div>')
                                .append($.parseHTML(responseText))
                                .find('#cat_video')
                                .children()
                        );
                        // process added elements
                        $('#marker-end').lazyLoadXT({visibleOnly: false, checkDuplicates: false});
                    });
            }).lazyLoadXT({visibleOnly: false});

    });
</script>
<div class="col-md-9 total-blog">
    <div class="main-title-head">
        <h3>Videos</h3>
<div class="video-filter floatR">
            <ul>
                <li>Category : </li>
                <li>

                    <select class="form-select" id="category">
                        <option value="all">All Categories</option>
                        <?php foreach($categories as $row)
                        {
                            ?>
                            <option value="<?php echo $row->cat_id;?>"><?php echo $row->cat_name;?></option>
                        <?php } ?>
                    </select>

                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
    <?php $login_required = is_login_required($this->router->fetch_class());?>
    <input type="hidden" id="startindex" value="0">
<div class="video-grid" id="cat_video">
    <input type="hidden" id="total_no_videos" value="<?php echo $total_videos;?>">
        <?php if(!empty($all_videos)){
        foreach($all_videos as $row)
        {
        ?>
        <div class="video-main-container floatL" id="video-main-container3">
           <?php if($login_required) {
            if (!$this->session->userdata('user_id')) {
                ?>
                <a href="javascript:void(0);" onclick='$.ChkLogin()' >
            <?php } else {
                ?>
                <a href="javascript:void(0);" onclick='$.videoLink="<?php echo base_url($row->vid_path); ?>";$.videoTitle="<?php echo $row->vid_title; ?>";$.VideoPlay()'>
            <?php }
        }else{ ?>
              <a href="javascript:void(0);" onclick='$.videoLink="<?php echo base_url($row->vid_path); ?>";$.videoTitle="<?php echo $row->vid_title; ?>";$.VideoPlay()'>
         <?php }?>
                <img src="<?php echo base_url($row->img_path);?>" alt="">
                <h4>
					<?php 
					$title = substr($row->vid_title,0,45);
					echo $title."...";?>
				</h4>
            </a>
        </div>
        <?php }?>
<!--            <div class="clearfix"></div>-->
<!--            <div class="clearfix btvinpagination">-->
<!---->
<!--                --><?php //echo $ui_pagging; ?>
<!---->
<!--            </div>-->
			<div class="clearfix"></div>
           <?php }else{
            echo "No Records Found";
        }
        ?>
    </div>
    <div id="marker-end"></div>
</div>