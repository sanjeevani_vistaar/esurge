<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/5/2016
 * Time: 5:42 PM
 */
?>
<select name="city" id="" tabindex="8" required class="form-select ">
    <option value="" >Select Your City *</option>
    <?php foreach($city_list as $row)
    {
        ?>
        <option value="<?php echo $row->city_id;?>" <?php if($row->city_id==$this->session->userdata('city_id')){echo "selected";}?>><?php echo $row->city_name;?></option>
    <?php } ?>
</select>
