<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/16/2015
 * Time: 1:25 PM
 */
?>
<div class="main-title-head">
        <h3>Verification Mobile Number</h3>
        <div class="clearfix"></div>
</div>
<div class="regi-content">
    <div class="row">

        <form name="registration_frm" action="<?php echo base_url('registration/verify_sms');?>" method="post" class="regisform">
            <input type="hidden" name="user_id" value="<?php echo $user_id;?>">
            * Verification code has been sent to given mobile number.
            <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-lock"></span>
                                    </div>
                                   <input type="password" name="verification_code" Placeholder="Enter Verification Code" tabindex="2" required>
                                </div>
                            </div>
                            <div class="submit-btn clearfix">
                                <input type="submit" name="submit"class="floatL" Value="Submit">
                            </div>
            </div>
        </form>
    </div>
</div>
