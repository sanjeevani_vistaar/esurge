<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/30/2015
 * Time: 10:38 AM
 */
?>
<div class="col-md-9 total-blog">
    <div class="main-title-head">
        <h3>Images</h3>
        <div class="clearfix"></div>
    </div>
    <?php $login_required = is_login_required($this->router->fetch_class());?>

    <div class="video-grid" id="cat_video">

        <?php if(!empty($all_images)){
            foreach($all_images as $row)
            {
                ?>
                <div class="video-main-container floatL" id="video-main-container3">
                 <img src="<?php echo base_url($row->img_path);?>" alt="<?php echo clean($row->img_title);?>">
                        <h4>
                            <?php
                            $title = substr($row->img_title,0,45);
                            echo $title."...";?>
                        </h4>
                    </a>
                </div>
            <?php }?>
            <div class="clearfix"></div>
            <div class="clearfix btvinpagination">

                <?php echo $ui_pagging; ?>

            </div>
            <div class="clearfix"></div>
        <?php }else{
            echo "No Records Found";
        }
        ?>
    </div>

</div>