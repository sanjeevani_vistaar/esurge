<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/30/2015
 * Time: 10:38 AM
 */
?>
<div class="col-md-9 total-blog">
	<div class="main-title-head">
		<h3>Other Videos</h3>
		<div class="clearfix"></div>
	</div>
	<div class="video-grid" id="cat_video">

		<?php if(!empty($all_videos)){
			foreach($all_videos as $row)
			{
				?>
				<div class="video-main-container floatL webcast-margin" id="video-main-container3">
					<?php
				if (!$this->session->userdata('user_id')) {
					?>
					<a href="javascript:void(0);" onclick='$.ChkLogin()' alt="<?php echo $row->vid_title; ?>">
				<?php } else {
				if ($row->vid_path){
				?>
						<a href="javascript:void(0);" alt="<?php echo $row->vid_title; ?>"
						   onclick='$.videoLink="<?php echo base_url($row->vid_path); ?>";$.videoTitle="<?php echo $row->vid_title; ?>";$.trackingLink="<?php echo base_url('tracking'); ?>";$.videoSource="Other video";$.VideoPlay()'>
							<?php }
							if ($row->vid_link){
							?>
							<a href="javascript:void(0);" alt="<?php echo $row->vid_title; ?>"
							   onclick='$.videoLink="<?php echo $row->vid_link; ?>";$.videoTitle="<?php echo $row->vid_title; ?>";$.trackingLink="<?php echo base_url('tracking'); ?>";$.videoSource="Other video";$.UrlVideoPlay()'>
								<?php }
								}
					?>
							<img src="<?php echo base_url($row->img_path);?>" alt="">
							<h4>
								<?php
								$title = substr($row->vid_title,0,45);
								echo $title."...";?>
							</h4>
						</a>
				</div>
			<?php }?>
			<div class="clearfix"></div>
			<div class="clearfix btvinpagination">

				<?php echo $ui_pagging; ?>

			</div>
			<div class="clearfix"></div>
           <?php }else{
            echo "No Records Found";
        }
        ?>
	</div>
</div>