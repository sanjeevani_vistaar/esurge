<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/16/2015
 * Time: 1:25 PM
 */
?>
<script>
$(document).ready(function(){
var state_id=$("#state").val();
        if(state_id) {
            $.ajax({
                url: "<?php echo base_url('/registration/get_cities');?>",
                type: "POST",
                data:{id:state_id},
                success: function(data) {
                    $("#city").html(data);
                }
            });
        }
        $("#state").change(function(){
            var state_id=$("#state").val();
        if(state_id) {
            $.ajax({
                url: "<?php echo base_url('registration/get_cities');?>",
                type: "POST",
                data:{id:state_id},
                success: function(data) {
                    $("#city").html(data);
                }
            });
        }else{
                $("#city").html('<select name="city" id="" tabindex="8" required class="form-select "><option value="">Select City *</option></select>')
            }
        })
$("#reset").click(function(){
$.ajax({
                url: "<?php echo base_url('registration/clear_session');?>",
                type: "POST",
                data:{},
                success: function(data) {
                    window.location.href = "<?php echo base_url('registration');?>";
                }
            });

})
	$("#term_use").click(function(){
		$.ajax({
			url: "<?php echo base_url('site_contents/term_of_use');?>",
			type: "GET",
			success: function (data) {
				$('#regModal').modal('show');
				$('.regmodal-title').text('Term of Use');
				$('.regmodal-body').html(data);
			}
		})
	})
	$("#policy").click(function(){
		$.ajax({
			url: "<?php echo base_url('site_contents/privacy_policy');?>",
			type: "GET",
			success: function (data) {
				$('#regModal').modal('show');
				$('.regmodal-title').text('Privacy Policy');
				$('.regmodal-body').html(data);
			}
		})
	})
});
</script>
<div class="registrationform">
<div class="col-md-9 total-news">
        <div class="regi-content">
    <div class="main-title-head">
        <h3>Registration</h3>
        <div class="clearfix"></div>
    </div>
    <div class="row">

      
        <form name="registration_frm" action="<?php echo base_url('registration');?>" method="post" class="regisform">
            <div class="col-md-12">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</div>
						<input type="text" name="first_name" Placeholder="First Name *" tabindex="1" required value="<?php echo $this->session->userdata('first_name');?>">
					</div>
				</div>
<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</div>
						<input type="text" name="last_name" Placeholder="Last Name *" tabindex="1" required value="<?php echo $this->session->userdata('last_name');?>">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-envelope"></span>
						</div>
						<input type="email" name="email" Placeholder="Email ID *" tabindex="2" required value="<?php echo $this->session->userdata('email');?>">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-envelope"></span>
						</div>
						<input type="text" name="registration_code" Placeholder="Registration Code *" tabindex="3" required>
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-lock"></span>
						</div>
						<input type="password" name="password" Placeholder="Password *" tabindex="4" required>
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-lock"></span>
						</div>
						<input type="password" name="conf-password" Placeholder="Confirm Password *" tabindex="5" required>
					</div>
				</div>	
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-earphone"></span>
						</div>
						<input type="tel" name="mobile" Placeholder="Mobile Number *" maxlength="10" tabindex="6" required value="<?php echo $this->session->userdata('mobile');?>">
					</div>
				</div>		
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</div>
						<input type="text" name="dob" Placeholder="Birth Date" tabindex="12" id="datepicker" value="<?php echo $this->session->userdata('dob');?>">
					</div>
				</div>		
					
					
               
            </div>
            <div class="col-md-12">				
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-map-marker"></span>
						</div>
						<div class="form_arrow">
							<select name="state" id="state" tabindex="7" required class="form-select ">
								<option value="" >Select Your State *</option>
								<?php foreach($states as $row)
								{
									?>
									<option value="<?php echo $row->state_id;?>" <?php if($row->state_id==$this->session->userdata('state')){echo "selected";}?>><?php echo ucwords(strtolower($row->state_name));?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>					
               <div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-map-marker"></span>
						</div>
						<div class="form_arrow" id="city">
							<select name="city" id="" tabindex="8" required class="form-select ">
								<option value="">Select Your City *</option>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-check"></span>
						</div>
						<div class="form_arrow">
							<select name="user_type" id="" tabindex="9" required class="form-select ">
								<option value="">Select Your Status *</option>
								<option value="Consultant" <?php if($this->session->userdata('user_type')=='Consultant'){echo "selected";}?>>Consultant</option>
								<option value="Post graduates" <?php if($this->session->userdata('user_type')=='Post graduates'){echo "selected";}?>>Post graduates</option>
							</select>
						</div>
					</div>
				</div>
                <div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="fa fa-edit"></span>
						</div>
						<input type="text" name="registration_no" Placeholder="Registration Number *" tabindex="10" required value="<?php echo $this->session->userdata('registration_no');?>">
					</div>
				</div>
                <div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-th"></span>
						</div>
						<input type="text" name="usi_no" Placeholder="USI Number" tabindex="11" value="<?php echo $this->session->userdata('usi_no');?>">
					</div>
				</div>
				<input type="checkbox" class="opt" id="agree" name="agree_chk" required>
				<span class="txt-check">I agree to the <a href="javascript:void(0);" id="term_use" class="dark-blue">Terms of Use</a> and <a href="javascript:void(0);" id="policy" class="dark-blue">Privacy Policy</a></span><Br />
                
                <div class="submit-btn clearfix">
                    <input type="reset" class="floatL" id="reset">
                <input type="submit" name="submit" class="floatL" Value="Submit">
                </div>

            </div>

        </form>
    </div>
</div>
        </div>
</div>
<div class="modal fade" id="regModal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" >
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="regmodal-title"></h4>
			</div>
			<div class="regmodal-body">

			</div>
		</div>
	</div>
</div>