<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 11:09 AM
 */
?>
<div class="col-md-9 total-blog">
    <div class="main-title-head">
        <h3>Disclaimer</h3>
        <div class="clearfix"></div>
    </div>
    <div class="disclaimer-wrapper">
    <?php echo $disclaimer[0]->content_text;?>
    </div>
    <div class="main-title-head">
        <h3>Terms of use</h3>
        <div class="clearfix"></div>
    </div>
    <div class="disclaimer-wrapper">
        <?php echo $term_of_use[0]->content_text;?>
    </div>
</div>
