<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 10:41 AM
 */
?>
<script>
    $(document).ready(function(){

        $(".feedback_right").hide();
        $(".feedback_wrong").hide();
        $(".correct_answer").hide();
        $(".opt").change(function(){
            var selectedVal = "";
            var selected = $("input[type='radio'][name='puzz_answer']:checked");
            if (selected.length > 0) {
                selectedVal = selected.val();
            }
            var id=$("#question_id").val();
            $.ajax({
                url: "<?php echo base_url('puzzle/get_correct_answer');?>",
                type: "POST",
                data: {id: id},
                success: function (data) {
                    if(data != selectedVal)
                    {
                        $(".feedback_right").hide();
                        $(".correct_answer").show();
                        $(".feedback_wrong").show();
                        $(".feedback_wrong").html('Sorry ! Wrong Answer.Correct Answer is given below');
                        $(".correct_answer").html(data);


                    }else{
                        $(".feedback_wrong").hide();
                        $(".feedback_right").show();
                        $(".correct_answer").hide();
                        $(".feedback_right").html('Congratulation ! Correct Answer');

                    }
                }
            });

        })
    });
</script>
 <div class="grids" id="question_list">
            <?php if(!empty($puzzle)){
            foreach($puzzle as $row)
            {
            ?>
                <div class="grid box question_grid">
                    <div class="grid-header">
                        <input type="hidden" value="<?php echo $row->id;?>" id="question_id">
                        <?php echo $row->puzz_question;?>
                        <div class="clearfix"></div>
                        <input type="radio" name="puzz_answer" class="opt" value="<?php echo $row->option_1;?>">
                        <?php echo $row->option_1;?>
                        <div class="clearfix"></div>
                        <input type="radio" name="puzz_answer" class="opt" value="<?php echo $row->option_2;?>">
                        <?php echo $row->option_2;?>
                        <div class="clearfix"></div>
                        <input type="radio" name="puzz_answer" class="opt" value="<?php echo $row->option_3;?>">
                        <?php echo $row->option_3;?>
                        <div class="clearfix"></div>
                        <input type="radio" name="puzz_answer" class="opt" value="<?php echo $row->option_4;?>">
                        <?php echo $row->option_4;?>
                        <div class="clearfix"></div>
                        <div class="feedback_right"></div>
                        <div class="feedback_wrong"></div>
                        <div class="correct_answer"></div>
                    </div>
                </div>
            <?php }?>
                <div class="clearfix"></div>
                <?php
                $prev=$no-1;
                if($prev<1){
                    $prev='';
                }
                $next=$no+1;?>
                <?php if($no >=1){?>
                <div class="pull-left"><a href="javascript:void(0);" onclick='$.quizLink="<?php echo base_url('puzzle/question/'.$cat_details[0]->cat_id.'/'.$prev); ?>";$.quizTitle="<?php echo $cat_details[0]->cat_name; ?>";$.QuizPlay()'>Previous</a></div>
                    <?php }
                if($no < $total_no-1){?>
                <div class="pull-right"><a href="javascript:void(0);" onclick='$.quizLink="<?php echo base_url('puzzle/question/'.$cat_details[0]->cat_id.'/'.$next); ?>";$.quizTitle="<?php echo $cat_details[0]->cat_name; ?>";$.QuizPlay()'>Next</a></div>
                    <?php }?>
			<div class="clearfix"></div>
           <?php }
            else{
                echo "No Records Found";
            }?>
        </div>


