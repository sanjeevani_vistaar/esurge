<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/23/2016
 * Time: 3:28 PM
 */
$login_required = is_login_required($this->router->fetch_class());
if($all_events){    
    $available_data = 0;
    foreach($all_events as $row) {
        $date_split = explode(' ', $row->start);
        $date = $date_split[0];
        $time = strtotime($date);
        $year = date("Y", $time);
        $month = date("F", $time);
        $day = date("j", $time);
        $end_date_split = explode(' ', $row->end);
        $end_date = $end_date_split[0];
        $end_time = strtotime($end_date);
        $end_year = date("Y", $end_time);
        $end_month = date("F", $end_time);
        $end_day = date("j", $end_time);
        if ($selected_month == '') {
            if ($selected_year == $year) {
                 $available_data = 1;
                ?>
                <div class="events-wrap floatL">
                    <div class="inner clearfix">
                        <div class="first-wrap floatL">
                            <time datetime="2016-01-28">
                                <div class="date"><?php echo $day . " - " . $end_day; ?></div>
                                <div class="month"><?php echo $month; ?></div>
                                <div class="year"><?php echo $year; ?></div>
                            </time>
                        </div>
                        <div class="events-details floatL">
                            <?php $url = parse_url($row->link);
                            if(in_array("http", $url)) {
                                $link=$row->link;
                            }elseif(in_array("https", $url)) {
                                $link=$row->link;
                            }else{
                                $link="http://".$row->link;
                            }?>
                            <header>
                                <h3>
                                    <?php if($login_required)
                                    {
                                        if (!$this->session->userdata('user_id'))
                                        {	?>
                                            <a href="javascript:void(0);" onclick='$.ChkLogin()' ><?php echo $row->title;?></a>
                                            <?php
                                        }
                                        else {?>
                                            <a target="_blank" href="<?php echo $link;?>"><?php echo $row->title;?></a>
                                        <?php }
                                    }
                                    else{ ?>
                                        <a target="_blank" href="<?php echo $link;?>"><?php echo $row->title;?></a>
                                    <?php }?>
                                </h3>
                            </header>
                            <div class="place">
                                <p><?php echo $row->location; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }

        } else {
            if ($selected_year == $year && $selected_month == $month) {
                $available_data = 1;
                ?>
                <div class="events-wrap floatL">
                    <div class="inner clearfix">
                        <div class="first-wrap floatL">
                            <time datetime="2016-01-28">
                                <div class="date"><?php echo $day . " - " . $end_day; ?></div>
                                <div class="month"><?php echo $month; ?></div>
                                <div class="year"><?php echo $year; ?></div>
                            </time>
                        </div>
                        <div class="events-details floatL">
                            <?php $url = parse_url($row->link);
                            if(in_array("http", $url)) {
                                $link=$row->link;
                            }elseif(in_array("https", $url)) {
                                $link=$row->link;
                            }else{
                                $link="http://".$row->link;
                            }?>
                            <header>
                                <h3>
                                    <?php if($login_required)
                                    {
                                        if (!$this->session->userdata('user_id'))
                                        {	?>
                                            <a href="javascript:void(0);" onclick='$.ChkLogin()' ><?php echo $row->title;?></a>
                                            <?php
                                        }
                                        else {?>
                                            <a target="_blank" href="<?php echo $link;?>"><?php echo $row->title;?></a>
                                        <?php }
                                    }
                                    else{ ?>
                                        <a target="_blank" href="<?php echo $link;?>"><?php echo $row->title;?></a>
                                    <?php }?>
                                </h3>
                            </header>
                            <div class="place">
                                <p><?php echo $row->location; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }
        }
    }
        if( $available_data!=1){
            echo "No Records found";
        }
    }
else{
        echo "No Records found";
    } ?>