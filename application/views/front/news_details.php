<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 10:01 AM
 */
?>
<div class="col-md-9 total-news innercontent-left">
    <div class="main-title-head">
       <h3>News Details</h3>
       <div class="clearfix"></div>
   </div>
    <h1 class="innerpage-title">
        <?php echo $news_details[0]->news_title;?>
    </h1>
   <!-- <p>
						<span>
							<a class="author" href="#"></a>
							January 02, 2016
						</span>
    </p>-->
    <!--<h5 class="red-tags tag-img">
        <?php $tags=explode(',',$news_details[0]->tags);

        for($i=0;$i<count($tags);$i++)
        {?>
            <a href="#"><?php echo $tags[$i];?></a>
        <?php }
        ?>
    </h5>-->
    <div class="clearfix"></div>
    <div class="inner-desc">
        <img src="<?php echo base_url($news_details[0]->news_img_path);?>" alt="<?php echo clean($news_details[0]->news_title);?>"/>
        <div id="print_here">
            <p class="fulstorytext"><?php echo $news_details[0]->news_desc;?></p>
        </div>
    </div>
    <div class="latest-articles">
        <div class="main-title-head">
            <h3>Other News</h3>
            <div class="clearfix"></div>
        </div>
        <div class="world-news-grids">
            <?php if(!empty($home_news)){
                foreach($home_news as $row){
if($current_news!=$row->news_id){
                    ?>
                    <div class="world-news-grid">
                        <a href="<?php echo base_url('news/news_details/'.$row->news_id.'/'.clean($row->news_title));?>"><img src="<?php echo base_url($row->news_img_path);?>" alt="<?php echo clean($row->news_title);?>" /></a>
                        <a href="<?php echo base_url('news/news_details/'.$row->news_id.'/'.clean($row->news_title));?>" class="title"><?php echo $row->news_title;?></a>
<!--                        <p>--><?php //echo $row->news_desc;?><!--</p><!--<a href="">Read More</a>-->
                    </div>
                <?php }}
            }else{
                echo "No Records Found";
            }?>

            <div class="clearfix"></div>
        </div>
    </div>

</div>
