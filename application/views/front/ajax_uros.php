<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/27/2016
 * Time: 3:27 PM
 */
$login_required = is_login_required($this->router->fetch_class());
 if(!empty($all_uros)){?>

     <?php
    foreach($all_uros as $row)
    {
        ?>
        <div class="grid box journals-title">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span class="fa  fa-file-powerpoint-o "></span>
                    </div>
                    <div class="grid-img-content">
                        <h1>

                            <?php if($login_required)
                            {
                                if (!$this->session->userdata('user_id'))
                                {	?>
                                    <a href="javascript:void(0);" onclick='$.ChkLogin()' ><?php echo $row->uro_title;?></a>
                                    <?php
                                }
                                else {?>
                                    <a target="_blank" href="http://docs.google.com/gview?url=<?php echo base_url($row->file_path);?>"><?php echo $row->uro_title;?></a>
                                <?php }
                            }
                            else{ ?>
                                <a target="_blank" href="http://docs.google.com/gview?url=<?php echo base_url($row->file_path);?>"><?php echo $row->uro_title;?></a>
                            <?php }?>

                        </h1>
                        <h3><?php echo $row->date;?></h3>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>
        </div>
    <?php }
}else{
    echo "No Records Found";
}