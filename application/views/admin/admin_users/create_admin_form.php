<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:08 AM
 */
?>

<div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Create Admin</h4>
            </div><!-- /.box-header -->
            <!-- form start -->
            <?php if(!empty($this->session->flashdata('err_msg'))){?>
                <div class="alert alert-danger text-center">
                    <?php echo $this->session->flashdata('err_msg'); ?>
                </div>
            <?php }?>
            <form role="form" name="myForm" action="<?php echo base_url('/admin/admin_users/create_admin');?>" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">First Name</label>
                        <input type="text" placeholder="Enter First Name" name="firstname" class="form-control" required value="<?php echo $this->session->userdata('fname');?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Last Name</label>
                        <input type="text" placeholder="Enter Last Name" name="lastname" class="form-control" required value="<?php echo $this->session->userdata('lname');?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email Id</label>
                        <input type="email" placeholder="Enter email" name="email" class="form-control" required value="<?php echo $this->session->userdata('email');?>">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" placeholder="Password" name="password" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Confirm Password</label>
                        <input type="password" placeholder="Password" name="conf-password" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mobile Number</label>
                        <input type="text" placeholder="Enter Mobile Number" name="mobile" class="form-control" maxlength="10" required value="<?php echo $this->session->userdata('mobile');?>">
                    </div>
                    <div class="form-group">
                        <label>Role</label>
                        <select class="form-control" name="role"  required>
                            <option value="">Select Role</option>
                            <?php foreach($admin_roles as $row)
                            {
                            ?>
                            <option value="<?php echo $row->role_id;?>" <?php if($this->session->userdata('role')==$row->role_id)echo "selected";?>><?php echo $row->role;?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <div class="radio">
                            <label class="">
                                <div class="iradio_minimal checked" style="position: relative;" aria-checked="false" aria-disabled="false"><input type="radio" checked="" value="1" id="optionsRadios1" name="status" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                Active
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <div class="iradio_minimal" style="position: relative;" aria-checked="false" aria-disabled="false"><input type="radio" value="0" id="optionsRadios2" name="status" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                Deactive
                            </label>
                        </div>

                    </div>

                </div><!-- /.box-body -->

                <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Create</button>
            </form>
        </div>
</div>