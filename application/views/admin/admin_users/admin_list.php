<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:24 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="col-md-12">
    <h4>Admin List</h4>
    <div class="pull-right"><a href="<?php echo base_url('admin/admin_users/create_admin');?>">Create New Admin</a> </div>
    <?php if($this->session->flashdata('succ_msg')){?>
        <div class="alert alert-success text-center">
            <?php echo $this->session->flashdata('succ_msg'); ?>
        </div>
    <?php }?>
    <table id="records_pagination" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>

        <tr class="active">
            <th> Fullname</th>
            <th> Email id </th>
            <th> Mobile No </th>
            <th> Role </th>
            <th> Status </th>
            <th> Action </th>
        </tr>
        </thead>
        <tbody>
        <?php if(!empty($arrData)){
            foreach($arrData as $row)
            {
                ?>
                <tr>
                    <td align="center"><?php echo ucwords($row->first_name." ".$row->last_name);?></td>
                    <td align="center"><?php echo $row->email_id;?></td>
                    <td align="center"><?php echo $row->mobile_no;?></td>
                    <td align="center"><?php echo $row->role;?></td>
                    <td align="center"><?php if($row->status==1){echo "Active";}else{echo "Deactive";}?></td>
                    <td align="center"><a href="<?php echo base_url('admin/admin_users/edit_admin/'.$row->admin_id);?>"><i class="fa fa-pencil"></i></a> | <a href='javascript:void(0)' onclick='$.deleteLink="<?php echo base_url('admin/admin_users/delete_admin/'.$row->admin_id);?>";$.OnDeleteDialog()'><i class="fa fa-trash-o"></i></a></td>
                </tr>
                <?php
            }
        }
        else{?>
            <tr><th colspan="4"> No Records Found</th></tr>
        <?php }?>
        </tbody>
    </table>
</div>