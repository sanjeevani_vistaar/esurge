<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:24 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="col-md-12">
    <h4>Users Tracking Report / <?php echo ucwords($user_details->first_name." ".$user_details->last_name);?></h4>

    <div class="pull-right"><a href="<?php echo base_url('admin/front_users/download_all_tracking/'.$user_details->user_id);?>">Download</a></div>
  <table id="records_pagination" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>

        <tr class="active">
            <th> Source </th>
            <th> Title </th>
            <th> Number of Hits </th>
            <th> Date </th>
        </tr>
        </thead>
        <tbody>
        <?php if(!empty($tracking_details)){
            foreach($tracking_details as $row)
            {
                ?>
                <tr>
                    <td><?php echo ucwords($row->source);?></td>
                    <td><?php echo $row->title;?></td>
                    <td><?php echo $row->hit;?></td>
                    <td><?php echo date('d-m-Y',strtotime($row->view_date));?></td>
                </tr>
                <?php
            }
        }
        else{?>
            <tr><th colspan="4"> No Records Found</th></tr>
        <?php }?>
        </tbody>
    </table>
</div>