<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:24 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script>
    $(document).ready(function(){
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd'
        });

        $("#filter").click(function(){
            var todate=$("#to_date").val();
            var fromdate=$("#from_date").val();
            window.location.href="<?php echo base_url('admin/front_users/all_list_by_date');?>/"+todate+"/"+fromdate;
        })

        $("#state").change(function(){
            var state_id=$("#state").val();
            window.location.href="<?php echo base_url('admin/front_users/all_list');?>/"+state_id;
        })
    });
</script>
<div class="col-md-12">
    <h4>Users List</h4>
    <?php if(!empty($this->session->flashdata('succ_msg'))){?>
        <div class="alert alert-success text-center">
            <?php echo $this->session->flashdata('succ_msg'); ?>
        </div>
    <?php }?>
    <div class="pull-right" style="margin-top: -33px;">
        To : <input type="text" value="<?php echo $to_date;?>" Placeholder="To Date" tabindex="12" id="to_date" class="datepicker">
        From : <input type="text" value="<?php echo $from_date;?>" Placeholder="From Date" tabindex="12" id="from_date" class="datepicker">
        <input type="button" id="filter" value="Filter">

    </div>
    <div class="pull-right">
        <?php if($to_date && $from_date){?>
            <a href="<?php echo base_url('admin/front_users/download_all_by_date/'.$to_date.'/'.$from_date);?>">Download</a>
       <?php }else{?>
            <a href="<?php echo base_url('admin/front_users/download_all');?>">Download</a>
        <?php }?>
    </div>
    <!--
    <div class="pull-right">
        <label>State</label>
        <select class="form-control" name="state_id" id="state">
            <option value="">Select State</option>
            <?php foreach($states as $row)
            {
                ?>
                <option value="<?php echo $row->state_id;?>" <?php if($selected_state==$row->state_id)echo "selected";?>><?php echo $row->state_name;?></option>
            <?php } ?>
        </select>
    </div>
    -->
    <table id="records_pagination" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>

        <tr class="active">
            <th> Fullname</th>
            <th> Email id </th>
            <th> Mobile No </th>
            <th> State </th>
            <th> Type </th>
            <th> Status </th>
            <th> Creation Date </th>
            <th> Action </th>
        </tr>
        </thead>
        <tbody>
        <?php if(!empty($all_records)){
            foreach($all_records as $row)
            {
                ?>
                <tr>
                    <td align="center"><?php echo ucwords($row->first_name." ".$row->last_name);?></td>
                    <td align="center"><?php echo $row->email_id;?></td>
                    <td align="center"><?php echo $row->mobile_no;?></td>
                    <td align="center"><?php echo $row->state_name;?></td>
                    <td align="center"><?php echo $row->user_type;?></td>
                    <td align="center"><?php if($row->status==1){echo "Active";}else{echo "Deactive";}?></td>
                    <td align="center"><?php echo date('d-m-Y',strtotime($row->created_on));?></td>
                    <td align="center"><a href='javascript:void(0)' onclick='$.analyticsLink="<?php echo base_url('admin/front_users/tracking/'.$row->user_id);?>";$.OnAnalyticsDialog()'>Page View Details</a> | <a href='javascript:void(0)' onclick='$.statusChangeLink="<?php echo base_url('admin/front_users/change_status/'.$row->user_id);?>";$.OnStatuschangeDialog()'>Change Status</a> | <a href='javascript:void(0)' onclick='$.deleteLink="<?php echo base_url('admin/front_users/delete_user/'.$row->user_id);?>";$.OnDeleteDialog()'><i class="fa fa-trash-o"></i></a></td>
                </tr>
                <?php
            }
        }
        else{?>
            <tr><th colspan="4"> No Records Found</th></tr>
        <?php }?>
        </tbody>
    </table>
</div>