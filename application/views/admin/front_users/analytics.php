<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:24 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php if($login_details){
    echo "<b>Last Login Date and Time : ".date('d-m-Y',strtotime($login_details[0]->login_date))." ".$login_details[0]->login_time."</b>";
    echo "  <a href=".base_url('admin/front_users/all_tracking/'.$user_id).">View All</a>";
}?>
   <table class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>

        <tr class="active">
            <th> Source </th>
            <th> Title </th>
            <th> Number of Hits </th>
        </tr>
        </thead>
        <tbody>
        <?php if(!empty($tracking_details)){
            foreach($tracking_details as $row)
            {
                ?>
                <tr>
                    <td><?php echo ucwords($row->source);?></td>
                    <td><?php echo $row->title;?></td>
                    <td><?php echo $row->hit;?></td>
                </tr>
                <?php
            }
        }
        else{?>
            <tr><th colspan="4"> No Records Found</th></tr>
        <?php }?>
        </tbody>
    </table>