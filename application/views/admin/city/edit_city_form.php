<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:08 AM
 */
?>
<div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Update City</h4>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo base_url('index.php/admin/city/edit_city/'.$city_details[0]->city_id);?>" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">City Name</label>
                        <input type="text" placeholder="Enter City Name" name="name" class="form-control" required value="<?php echo $city_details[0]->city_name;?>">
                    </div>
                    <div class="form-group">
                        <label>State</label>
                        <select class="form-control" name="state_id"  required>
                            <option value="">Select State</option>
                            <?php foreach($states as $row)
                            {
                                ?>
                                <option value="<?php echo $row->state_id;?>" <?php if($city_details[0]->state_id==$row->state_id)echo "selected";?>><?php echo $row->state_name;?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Update</button>
            </form>
        </div>
</div>