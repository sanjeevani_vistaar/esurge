<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:24 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="col-md-12">
    <h4>Menu List</h4>
    <div class="pull-right"><a href="<?php echo base_url('admin/menu/add_menu');?>">Add New Menu</a> </div>
    <?php if($this->session->flashdata('succ_msg')){?>
        <div class="alert alert-success text-center">
            <?php echo $this->session->flashdata('succ_msg'); ?>
        </div>
    <?php }?>
    <?php if($this->session->flashdata('err_msg')){?>
        <div class="alert alert-danger text-center">
            <?php echo $this->session->flashdata('err_msg'); ?>
        </div>
    <?php }?>
    <table id="records_pagination" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>

        <tr class="active">
            <th> Menu Title</th>
            <th> Status</th>
            <th> Is Login Required</th>
            <th> Access By</th>
            <th> Action </th>
        </tr>
        </thead>
        <tbody>
        <?php if(!empty($arrData)){
            foreach($arrData as $row)
            {
                if($row->menu_title=='pg_icon'){
                    $title='Meet The Icon';
                }elseif($row->menu_title=='pg_contri'){
                    $title="Academician's Contribution";
                }elseif($row->menu_title=='uro'){
                    $title="Uro Slides Share";
                }else{
                    $title=$row->menu_title;
                }

                ?>
                <tr>
                    <td align="center"><?php echo ucwords($title);?></td>
                    <td align="center"><?php if($row->status==1){echo "Active";}else{echo "Deactive";}?></td>
                    <td align="center"><?php if($row->is_login_required==1){echo "Yes";}else{echo "No";}?></td>
                    <td align="center"><?php echo $row->access_by?></td>
                    <td align="center"><a href="<?php echo base_url('admin/menu/edit_menu/'.$row->id);?>"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php
            }
        }
        else{?>
            <tr><th colspan="4"> No Records Found</th></tr>
        <?php }?>
        </tbody>
    </table>
</div>