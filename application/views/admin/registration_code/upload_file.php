<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:08 AM
 */
?>

<div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Upload File</h4>
            </div><!-- /.box-header -->
            <!-- form start -->
            <?php if(!empty($this->session->flashdata('err_msg'))){?>
                <div class="alert alert-danger text-center">
                    <?php echo $this->session->flashdata('err_msg'); ?>
                </div>
            <?php }?>
            <form ng-app="" role="form" name="myForm" action="<?php echo base_url('admin/registration_code/upload_codes');?>" method="post" enctype="multipart/form-data">
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputFile">Select File</label>
                        <input type="file" name="file_path" id="exampleInputFile" required>

                    </div>

                </div><!-- /.box-body -->

                <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Upload</button>
            </form>
        </div>
</div>