<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/14/2015
 * Time: 4:30 PM
 */
?>
<div class="col-md-6">
    <?php if($this->session->flashdata('err_msg')){?>
        <div class="alert alert-danger text-center">
            <?php echo $this->session->flashdata('err_msg'); ?>
        </div>
    <?php }?>
    <form action="<?php echo base_url('admin/registration_code/edit_details/'.$details[0]->registration_code_id);?>" method="post">
        <h4> Update Registration Code</h4>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="text" placeholder="Full Name" class="form-control" name="fullname" required value="<?php echo $details[0]->full_name;?>">
        </div>
        <br>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            <input type="email" placeholder="Email" class="form-control" name="email" value="<?php echo $details[0]->email_id;?>">
        </div>
        <br>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
            <input type="mobile" placeholder="Mobile" class="form-control" name="mobile" value="<?php echo $details[0]->mobile_no;?>">
        </div>
        <br>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
            <input type="text" placeholder="Registration Code" class="form-control" name="reg_code" required value="<?php echo $details[0]->registration_code;?>">
        </div>
        <br>
        <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Update</button>
    </form>
</div>