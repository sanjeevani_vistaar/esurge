<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:24 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <?php echo "Total Numbers - ".count($tracking_details);?>
    <br>
    <table id="records_pagination" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>

        <tr class="active">
            <th> First Name </th>
            <th> Last Name </th>
            <th> Email Id </th>
            <th> Contact Number </th>
            <th> Date </th>
        </tr>
        </thead>
        <tbody>
        <?php if(!empty($tracking_details)){
            foreach($tracking_details as $row)
            {
                if($row->first_name){
                    $username=$row->first_name;
                }else{
                    $username='Guest User';
                }
                ?>
                <tr>
                    <td><?php echo $username;?></td>
                    <td><?php echo $row->last_name;?></td>
                    <td><?php echo $row->email_id;?></td>
                    <td><?php echo $row->mobile_no;?></td>
                    <td><?php echo date('d-m-Y',strtotime($row->played_date));?></td>
                </tr>
                <?php
            }
        }
        else{?>
            <tr><th colspan="4"> No Records Found</th></tr>
        <?php }?>
        </tbody>
    </table>
