<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:24 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="col-md-12">
    <h4>Quiz List</h4>
    <div class="pull-right"><a href="<?php echo base_url('admin/puzzle/create_puzzle');?>">Create New Quiz</a> </div>
    <?php if($this->session->flashdata('succ_msg')){?>
        <div class="alert alert-success text-center">
            <?php echo $this->session->flashdata('succ_msg'); ?>
        </div>
    <?php }?>
    <table id="records_pagination" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>

        <tr class="active">
            <th> Question</th>
            <th> Option 1 </th>
            <th> Option 2 </th>
            <th> Option 3 </th>
            <th> Option 4 </th>
            <th> Correct Answer </th>
            <th> Status </th>
            <th> Action </th>
        </tr>
        </thead>
        <tbody>
        <?php if(!empty($arrData)){
            foreach($arrData as $row)
            {
                ?>
                <tr>
                    <td align="center"><?php echo $row->puzz_question;?></td>
                    <td align="center"><?php echo $row->option_1;?></td>
                    <td align="center"><?php echo $row->option_2;?></td>
                    <td align="center"><?php echo $row->option_3;?></td>
                    <td align="center"><?php echo $row->option_4;?></td>
                    <td align="center"><?php echo $row->correct_answer;?></td>
                    <td align="center"><?php if($row->status==1){echo "Active";}else{echo "Deactive";}?></td>
                    <td align="center"><a href='javascript:void(0)' onclick='$.PuzzTrackingLink="<?php echo base_url('admin/puzzle/tracking/'.$row->id);?>";$.OnPuzzleTracking()'>Count Details</a> |<a href="<?php echo base_url('admin/puzzle/edit_puzzle/'.$row->id);?>"><i class="fa fa-pencil"></i></a> | <a href='javascript:void(0)' onclick='$.deleteLink="<?php echo base_url('admin/puzzle/delete_puzzle/'.$row->id);?>";$.OnDeleteDialog()'><i class="fa fa-trash-o"></i></a></td>
                </tr>
                <?php
            }
        }
        else{?>
            <tr><th colspan="4"> No Records Found</th></tr>
        <?php }?>
        </tbody>
    </table>
</div>