<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:08 AM
 */
?>

<div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Create Quiz</h4>
            </div><!-- /.box-header -->
            <!-- form start -->
            <?php if(!empty($this->session->flashdata('err_msg'))){?>
                <div class="alert alert-danger text-center">
                    <?php echo $this->session->flashdata('err_msg'); ?>
                </div>
            <?php }?>
            <form role="form" name="myForm" action="<?php echo base_url('/admin/puzzle/create_puzzle');?>" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label>Category</label>
                         <select class="form-control" name="category" required>
                            <option value="">Select Category</option>
                            <?php foreach($categories as $row)
                            {
                                ?>
                                <option value="<?php echo $row->cat_id;?>"><?php echo $row->cat_name;?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Question</label>
                        <input type="text" placeholder="Enter Question" name="puzz_question" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Option 1</label>
                        <input type="text" placeholder="Enter Option 1" name="opt_1" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Option 2</label>
                        <input type="text" placeholder="Enter Option 2" name="opt_2" class="form-control" required>

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Option 3</label>
                        <input type="text" placeholder="Enter Option 3" name="opt_3" class="form-control" required>

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Option 4</label>
                        <input type="text" placeholder="Enter Option 4" name="opt_4" class="form-control" required>

                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Correct Answer</label>
                        <input type="text" placeholder="Enter Correct Answer" name="corr_answer" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <div class="radio">
                            <label class="">
                                <div class="iradio_minimal checked" style="position: relative;" aria-checked="false" aria-disabled="false"><input type="radio" checked="" value="1" id="optionsRadios1" name="status" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                Active
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <div class="iradio_minimal" style="position: relative;" aria-checked="false" aria-disabled="false"><input type="radio" value="0" id="optionsRadios2" name="status" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                Deactive
                            </label>
                        </div>

                    </div>

                </div><!-- /.box-body -->

                <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Create</button>
            </form>
        </div>
</div>