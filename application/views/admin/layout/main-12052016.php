<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/14/2015
 * Time: 1:30 PM
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>eSurge | Admin</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="<?php echo base_url('assets/common/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="<?php echo base_url('assets/admin/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="<?php echo base_url('assets/admin/css/ionicons.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="<?php echo base_url('assets/admin/css/morris/morris.css');?>" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="<?php echo base_url('assets/admin/css/jvectormap/jquery-jvectormap-1.2.2.css');?>" rel="stylesheet" type="text/css" />
    <!-- fullCalendar -->
    <link href="<?php echo base_url('assets/admin/css/fullcalendar/fullcalendar.css');?>" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="<?php echo base_url('assets/admin/css/daterangepicker/daterangepicker-bs3.css');?>" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="<?php echo base_url('assets/admin/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url('assets/admin/css/AdminLTE.css');?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/admin/css/ng-css.css');?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/common/css/bootstrap-dialog.min.css');?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/admin/css/dataTables.min.css');?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/front/css/bootstrap-datepicker.css');?>" rel="stylesheet" type="text/css" media="all" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url('assets/common/js/html5shiv.min.js');?>"></script>
    <script src="<?php echo base_url('assets/common/js/respond.min.js');?>"></script>
    <![endif]-->
    <!-- jQuery 2.0.2 -->
    <script src="<?php echo base_url('assets/common/js/jquery.min.js');?>"></script>
    <!-- jQuery UI 1.10.3 -->
    <script src="<?php echo base_url('assets/common/js/jquery-ui-1.10.3.min.js');?>" type="text/javascript"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url('assets/common/js/bootstrap.min.js');?>" type="text/javascript"></script>

    <!-- fullCalendar -->
    <script src="<?php echo base_url('assets/admin/js/plugins/fullcalendar/fullcalendar.min.js');?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/common/js/gcal.js');?>"></script>
    <script src="<?php echo base_url('assets/common/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>
<!--    <script src="--><?php //echo base_url('assets/common/js/angular.min.js');?><!--"></script>-->
<!--    <script src="--><?php //echo base_url('assets/admin/js/ng-app.js');?><!--"></script>-->
<!--    <script src="--><?php //echo base_url('assets/admin/js/ng-controller.js');?><!--"></script>-->
    <style>
        .bootstrap-dialog .bootstrap-dialog-message br{display: none;}
        .box.box-primary {margin-top: 7px;}
        .no-print{display: none;}
    </style>
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<header class="header">
    <a href="<?php echo base_url('admin/dashboard');?>" class="logo" style="background: #fff none repeat scroll 0 0;">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        <img width="65%" src="<?php echo base_url('assets/front/images/logo.png');?>">
        <!--        --><?php //echo $this->session->userdata('adminrole');?>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->



                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                        <span><?php echo ucwords($this->session->userdata('adminname'));?><i class="caret"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                       <!-- Menu Footer-->
                        <li class="user-footer">

                            <div class="pull-left">
                                <a href="<?php echo base_url('admin/admin_users/update_profile');?>" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo base_url('admin/login/logout');?>" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->

            <ul class="sidebar-menu">

                <li class="active">
                    <a href="<?php echo base_url('admin/dashboard');?>">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>


                <li>
                    <a href="<?php echo base_url('admin/journals/all_list');?>">
                        <i class="fa fa-dashboard"></i> <span>Journals</span>
                    </a>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i> <span>Videos</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('admin/gallery/categories_list');?>"><i class="fa fa-angle-double-right"></i>Categories</a></li>
                        <!--                        <li><a href="--><?php //echo base_url('admin/gallery/images_list');?><!--"><i class="fa fa-angle-double-right"></i>Images</a></li>-->
                        <li><a href="<?php echo base_url('admin/gallery/videos_list');?>"><i class="fa fa-angle-double-right"></i>Videos</a></li>
                    </ul>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/calendar');?>">
                        <i class="fa fa-dashboard"></i> <span>Events Calendar</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/news/all_list');?>">
                        <i class="fa fa-dashboard"></i> <span>News / Updates</span>
                    </a>
                </li>
<!--                <li class="treeview">-->
<!--                    <a href="#">-->
<!--                        <i class="fa fa-folder"></i> <span>Webcast</span>-->
<!--                        <i class="fa fa-angle-left pull-right"></i>-->
<!--                    </a>-->
<!--                    <ul class="treeview-menu">-->
<!--                        <li><a href="--><?php //echo base_url('admin/gallery/categories_list');?><!--"><i class="fa fa-angle-double-right"></i>Categories</a></li>-->
<!--                        <li><a href="--><?php //echo base_url('admin/webcast/videos_list');?><!--"><i class="fa fa-angle-double-right"></i>Videos</a></li>-->
<!--                    </ul>-->
<!--                </li>-->
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i> <span>Meet The Icon</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('admin/gallery/categories_list');?>"><i class="fa fa-angle-double-right"></i>Categories</a></li>
                        <li><a href="<?php echo base_url('admin/pg_icon/videos_list');?>"><i class="fa fa-angle-double-right"></i>Videos</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i> <span>Uro Slides</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('admin/uro/categories_list');?>"><i class="fa fa-angle-double-right"></i>Categories</a></li>
                        <li><a href="<?php echo base_url('admin/uro/all_list');?>"><i class="fa fa-angle-double-right"></i>List</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i> <span>Users</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                       <li><a href="<?php echo base_url('admin/admin_users/all_list');?>"><i class="fa fa-angle-double-right"></i>Admin Users</a></li>
                        <li><a href="<?php echo base_url('admin/front_users/all_list');?>"><i class="fa fa-angle-double-right"></i>Website Users</a></li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/banner');?>">
                        <i class="fa fa-dashboard"></i> <span>Scroller Management</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/menu/all_list');?>">
                        <i class="fa fa-dashboard"></i> <span>Menu Management</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i> <span>Webcast</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
<!--                        <li><a href="--><?php //echo base_url('admin/gallery/categories_list');?><!--"><i class="fa fa-angle-double-right"></i>Categories</a></li>-->
                        <!--                        <li><a href="--><?php //echo base_url('admin/gallery/images_list');?><!--"><i class="fa fa-angle-double-right"></i>Images</a></li>-->
                        <li><a href="<?php echo base_url('admin/webcast/videos_list');?>"><i class="fa fa-angle-double-right"></i>Videos</a></li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/gallery/images_list');?>">
                        <i class="fa fa-dashboard"></i> <span>Gallery</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/othervideos/videos_list');?>">
                        <i class="fa fa-dashboard"></i> <span>Other Videos</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i> <span>Quiz</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('admin/puzzle/categories_list');?>"><i class="fa fa-angle-double-right"></i>Categories</a></li>
                        <!--                        <li><a href="--><?php //echo base_url('admin/gallery/images_list');?><!--"><i class="fa fa-angle-double-right"></i>Images</a></li>-->
                        <li><a href="<?php echo base_url('admin/puzzle/all_list');?>"><i class="fa fa-angle-double-right"></i>Quiz</a></li>
                    </ul>
                </li>
<!--                <li class="treeview">-->
<!--                    <a href="#">-->
<!--                        <i class="fa fa-folder"></i> <span>Poll</span>-->
<!--                        <i class="fa fa-angle-left pull-right"></i>-->
<!--                    </a>-->
<!--                    <ul class="treeview-menu">-->
<!--                        <li><a href="--><?php //echo base_url('admin/poll/categories_list');?><!--"><i class="fa fa-angle-double-right"></i>Categories</a></li>-->
<!--                        <!--                        <li><a href="--><?php ////echo base_url('admin/gallery/images_list');?><!--<!--"><i class="fa fa-angle-double-right"></i>Images</a></li>-->
<!--                        <li><a href="--><?php //echo base_url('admin/poll/all_list');?><!--"><i class="fa fa-angle-double-right"></i>Poll</a></li>-->
<!--                    </ul>-->
<!--                </li>-->
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i> <span>Others</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('admin/registration_code/codes_list');?>"><i class="fa fa-angle-double-right"></i>Registration Code</a></li>
                        <li><a href="<?php echo base_url('admin/state/all_list');?>"><i class="fa fa-angle-double-right"></i>State</a></li>
                        <li><a href="<?php echo base_url('admin/city/all_list');?>"><i class="fa fa-angle-double-right"></i>City</a></li>
                        <li><a href="<?php echo base_url('admin/email_id/all_list');?>"><i class="fa fa-angle-double-right"></i>Contact Email Id</a></li>
                        <li><a href="<?php echo base_url('admin/site_contents/term_of_use');?>"><i class="fa fa-angle-double-right"></i>Terms of Use</a></li>
                        <li><a href="<?php echo base_url('admin/site_contents/privacy_policy');?>"><i class="fa fa-angle-double-right"></i>Privacy Policy</a></li>
                        <li><a href="<?php echo base_url('admin/site_contents/disclaimer');?>"><i class="fa fa-angle-double-right"></i>Disclaimer</a></li>
                    </ul>
                </li>

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
    <?php $this->load->view($middle);?>
    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<!-- add new calendar event modal -->



<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/admin/js/plugins/jqueryKnob/jquery.knob.js');?>" type="text/javascript"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('assets/admin/js/plugins/daterangepicker/daterangepicker.js');?>" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>" type="text/javascript"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/admin/js/plugins/iCheck/icheck.min.js');?>" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/admin/js/AdminLTE/app.js');?>" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('assets/admin/js/AdminLTE/dashboard.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/common/js/bootstrap-dialog.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/admin/js/jquery.dataTables.min.js');?>" type="text/javascript"></script>
<script>
$('.alert-danger').delay(3000).fadeOut();
$('.alert-success').delay(3000).fadeOut();
    $.OnDeleteDialog = function() {
        BootstrapDialog.confirm({
            title: 'CONFIRM',
            message: 'Are you sure to Delete?',
            callback: function(result) {
                if(result) {
                    window.location = $.deleteLink;
                }else {
                }
            }
        });
    }
    $.OnStatuschangeDialog = function() {
        BootstrapDialog.confirm({
            title: 'CONFIRM',
            message: 'Are you sure to change Status?',
            callback: function(result) {
                if(result) {
                    window.location = $.statusChangeLink;
                }else {
                }
            }
        });
    }
    $.OnEventUpdateDialog = function() {
        $.ajax({
            url: $.updateLink,
            type: "POST",
            success: function(data) {
                BootstrapDialog.show({
                    title:"Update Event",
                    message: data,
                });

            }
        });
    }
    $.OnBannerDialog = function() {
        $.ajax({
            url: $.ViewLink,
            type: "POST",
            success: function(data) {
                BootstrapDialog.show({
                    title:"Add To Banner",
                    message: data,
                });

            }
        });
    }
    $.OnBannerViewDialog = function() {
        $.ajax({
            url: $.bannerViewLink,
            type: "POST",
            success: function(data) {
                BootstrapDialog.show({
                    title:"Banner Details",
                    message: data,
                });

            }
        });
    }
    $.OnAnalyticsDialog = function() {
        $.ajax({
            url: $.analyticsLink,
            type: "POST",
            success: function(data) {
                BootstrapDialog.show({
                    title:"Page View Details",
                    message: data,
                });

            }
        });
    }
    $.OnPuzzleTracking = function() {
        $.ajax({
            url: $.PuzzTrackingLink,
            type: "POST",
            success: function(data) {
                BootstrapDialog.show({
                    title:"Count Details",
                    message: data,
                });

            }
        });
    }
    $.OnPollTracking = function() {
        $.ajax({
            url: $.PollTrackingLink,
            type: "POST",
            success: function(data) {
                BootstrapDialog.show({
                    title:"Count Details",
                    message: data,
                });

            }
        });
    }

    $('#records_pagination').DataTable({
        "ordering": false
    } ); // pagination


</script>

</body>
</html>