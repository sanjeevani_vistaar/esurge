<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:24 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<div class="col-md-12">
    <h4>News List</h4>
    <div class="pull-right"><a href="<?php echo base_url('index.php/admin/news/create_news');?>">Add News/Update</a> </div>
    <?php if($this->session->flashdata('succ_msg')){?>
        <div class="alert alert-success text-center">
            <?php echo $this->session->flashdata('succ_msg'); ?>
        </div>
    <?php }?>
    <table id="records_pagination" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>

        <tr class="active">
            <th> Title </th>
            <th> Description </th>
            <th> Image </th>
            <th> Status </th>
            <th> Action </th>

        </tr>
        </thead>
        <tbody>
        <?php if(!empty($arrData)){
            foreach($arrData as $row)
            {
                ?>
                <tr>
                    <td align="center"><?php echo ucwords($row->news_title);?></td>
                    <td align="center"><?php echo $row->news_desc;?></td>
                    <td align="center"><img src="<?php echo base_url($row->news_img_path);?>"></td>
                    <td align="center"><?php if($row->status==1){echo "Active";}else{echo "Deactive";}?></td>
                    <td align="center"><a href="<?php echo base_url('index.php/admin/news/edit_news/'.$row->news_id);?>"><i class="fa fa-pencil"></i></a> | <a href='javascript:void(0)' onclick='$.deleteLink="<?php echo base_url('index.php/admin/news/delete_news/'.$row->news_id);?>";$.OnDeleteDialog()'><i class="fa fa-trash-o"></i></a></td>

                </tr>
                <?php
            }
        }
        else{?>
            <tr><th colspan="4"> No Records Found</th></tr>
        <?php }?>
        </tbody>
    </table>
</div>