<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:08 AM
 */
?>
<link href="<?php echo base_url('/assets/tiny_mce/themes/advanced/skins/default/ui.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/tiny_mce/plugins/inlinepopups/skins/clearlooks2/window.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/tiny_mce/themes/advanced/skins/o2k7/ui.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/tiny_mce/themes/advanced/skins/o2k7/ui_silver.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/tiny_mce/themes/advanced/skins/o2k7/ui_black.css') ?>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url('/assets/tiny_mce/tiny_mce.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/admin/js/tinymce.js') ?>"></script>
<div class="col-md-9">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Update Disclaimer</h4>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo base_url('admin/site_contents/edit_disclaimer/'.$details[0]->id);?>" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Content</label>
                        <textarea placeholder="Enter Content" rows="3" class="form-control tiny_mce" name="cont_text"><?php echo $details[0]->content_text;?></textarea>
                    </div>
                </div>

                <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Update</button>
            </form>
        </div>
</div>