<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:24 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="col-md-12">
    <h4>Privacy Policy</h4>
<!--    <div class="pull-right"><a href="--><?php //echo base_url('admin/email_id/add_emailid');?><!--">Add New Email Id</a> </div>-->
    <?php if($this->session->flashdata('succ_msg')){?>
        <div class="alert alert-success text-center">
            <?php echo $this->session->flashdata('succ_msg'); ?>
        </div>
    <?php }?>
    <?php if($this->session->flashdata('err_msg')){?>
        <div class="alert alert-danger text-center">
            <?php echo $this->session->flashdata('err_msg'); ?>
        </div>
    <?php }?>
    <table id="records_pagination" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>

        <tr class="active">
            <th> Content</th>
            <th> Action </th>
        </tr>
        </thead>
        <tbody>
        <?php if(!empty($privacy_policy)){
            foreach($privacy_policy as $row)
            {
                ?>
                <tr>
                    <td align="center"><?php echo $row->content_text;?></td>
                    <td align="center"><a href="<?php echo base_url('admin/site_contents/edit_privacypolicy/'.$row->id);?>"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php
            }
        }
        else{?>
            <tr><th colspan="4"> No Records Found</th></tr>
        <?php }?>
        </tbody>
    </table>
</div>