<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:08 AM
 */
?>
<div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Update State</h4>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo base_url('admin/state/edit_state/'.$state_details[0]->state_id);?>" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">State Name</label>
                        <input type="text" placeholder="Enter State Name" name="name" class="form-control" required value="<?php echo $state_details[0]->state_name;?>">
                    </div>

                </div>

                <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Update</button>
            </form>
        </div>
</div>