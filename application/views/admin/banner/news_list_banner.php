<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:24 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form name="frm" action="<?php echo base_url('admin/banner/add_banner')?>" method="post">
    <?php if(!empty($arrData)){?>
    <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Add To Banner</button>
    <input type="hidden" name="banner_from" value="news">
    <table id="records_pagination" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>

        <tr class="active">
            <th> Title </th>
             <th> Image </th>
            <th> Status </th>
            <th> Banner </th>

        </tr>
        </thead>
        <tbody>
        <?php if(!empty($arrData)){
            foreach($arrData as $row)
            {
                ?>
                <tr>
                    <td align="center"><?php echo ucwords($row->news_title);?></td>
                    <td align="center"><img width="100%" src="<?php echo base_url($row->news_img_path);?>"></td>
                    <td align="center"><?php if($row->status==1){echo "Active";}else{echo "Deactive";}?></td>
                    <td align="center"><?php if($row->status==1){?><input name="id[]" type="checkbox" <?php if($row->is_banner==1) echo "checked";?> value="<?php echo $row->news_id; ?>"><?php }?></td>

                </tr>
                <?php
            }
        }
        else{?>
            <tr><th colspan="4"> No Records Found</th></tr>
        <?php }?>
        </tbody>
    </table>

        <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Add To Banner</button>
   <?php }else{
   ?>
        No Records Found.
   <?php }?>
</form>