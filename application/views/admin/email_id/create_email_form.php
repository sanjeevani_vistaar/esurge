<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:08 AM
 */
?>
<div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Add Email Id</h4>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo base_url('admin/email_id/add_emailid');?>" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email Id</label>
                        <input type="email" placeholder="Enter Email Id" name="emailid" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Type</label>
                        <select class="form-control" name="type"  required>
                            <option value="">Select Type</option>
                            <option value="Reciever">Reciever</option>
                            <option value="Sender">Sender</option>
                        </select>
                    </div>
                </div>

                <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Add</button>
            </form>
        </div>
</div>