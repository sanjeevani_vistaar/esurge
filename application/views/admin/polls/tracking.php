<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:24 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <table id="records_pagination" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>

        <tr class="active">
            <th> Option / Answer </th>
            <th> Count </th>
        </tr>
        </thead>
        <tbody>
        <?php
            foreach($option_details as $opt) {

                    ?>
                    <tr>
                        <td><?php echo $opt->option_1; ?></td>
                        <td><?php echo total_count($opt->id,$opt->option_1); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $opt->option_2; ?></td>
                        <td><?php echo total_count($opt->id,$opt->option_2); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $opt->option_3; ?></td>
                        <td><?php echo total_count($opt->id,$opt->option_3); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $opt->option_4; ?></td>
                        <td><?php echo total_count($opt->id,$opt->option_4); ?></td>
                    </tr>
                    <?php
            }
        ?>
        </tbody>
    </table>
