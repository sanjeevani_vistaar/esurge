<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:08 AM
 */
?>
<div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Update Category</h4>
            </div><!-- /.box-header -->
            <!-- form start -->
            <?php if(!empty($this->session->flashdata('err_msg'))){?>
                <div class="alert alert-danger text-center">
                    <?php echo $this->session->flashdata('err_msg'); ?>
                </div>
            <?php }?>
            <form role="form" action="<?php echo base_url('admin/poll/edit_category/'.$cat_details[0]->cat_id);?>" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Category Name</label>
                        <input type="text" placeholder="Enter category Name" name="name" class="form-control" required value="<?php echo $cat_details[0]->cat_name;?>">
                    </div>

                </div>

                <button type="submit" name="submit" class="btn btn-primary btn-sm pull-right">Update</button>
            </form>
        </div>
</div>