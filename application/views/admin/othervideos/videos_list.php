<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/21/2015
 * Time: 11:24 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<div class="col-md-12">
    <h4>Videos List</h4>

    <div class="pull-right"> <a href="<?php echo base_url('admin/othervideos/upload_video');?>">Upload New Video</a> </div>
    <?php if($this->session->flashdata('succ_msg')){?>
        <div class="alert alert-success text-center">
            <?php echo $this->session->flashdata('succ_msg'); ?>
        </div>
    <?php }?>
    <table id="records_pagination" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>

        <tr class="active">
            <th> Title </th>
            <th> Video </th>
            <th> Status </th>
            <th> Action </th>

        </tr>
        </thead>
        <tbody>
        <?php if(!empty($arrData)){
            foreach($arrData as $row)
            {
                ?>
                <tr>
                    <td align="center"><?php echo ucwords($row->vid_title);?></td>
                    <td align="center"><img width="50%" src="<?php echo base_url($row->img_path);?>"></td>
                    <td align="center"><?php if($row->status==1){echo "Active";}else{echo "Deactive";}?></td>
                    <td align="center"><a href="<?php echo base_url('admin/othervideos/edit_video/'.$row->vid_id);?>"><i class="fa fa-pencil"></i></a> | <a href='javascript:void(0)' onclick='$.deleteLink="<?php echo base_url('admin/othervideos/delete_video/'.$row->vid_id);?>";$.OnDeleteDialog()'><i class="fa fa-trash-o"></i></a></td>

                </tr>
                <?php
            }
        }
        else{?>
            <tr><th colspan="4"> No Records Found</th></tr>
        <?php }?>
        </tbody>
    </table>
</div>