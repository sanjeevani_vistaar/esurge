<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 11:24 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="col-md-12">
    <h4>Hit Report / <?php echo ucwords($page_source);?></h4>
    <?php if($page_source=='Meet The Icon'){
        $page_source='meettheicon';
    }?>
    <div class="pull-right"><a href="<?php echo base_url('admin/dashboard/download_report/'.$page_source);?>">Download</a></div>
    <table id="records_pagination" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>

        <tr class="active">
            <th> Title </th>
            <th> Number of Hits </th>

        </tr>
        </thead>
        <tbody>
        <?php if(!empty($tracking_details)){
            foreach($tracking_details as $row)
            {
                ?>
                <tr>
                    <td><?php echo $row->title;?></td>
                    <td><?php echo $row->hit;?></td>

                </tr>
                <?php
            }
        }
        else{?>
            <tr><th colspan="4"> No Records Found</th></tr>
        <?php }?>
        </tbody>
    </table>
</div>