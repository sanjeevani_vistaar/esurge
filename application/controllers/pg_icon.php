<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/16/2015
 * Time: 3:56 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Pg_icon extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('pg_model');
        $this->load->model('category_model');
        //$this->output->enable_profiler(TRUE);
        if ($this->session->userdata('user_id')) {
            $access_by = access_by('pg_icon');
            $user_access=$this->session->userdata('user_type');
            $userArr=explode(',',$access_by);
            if (!in_array($user_access,$userArr)) {
                $this->session->set_flashdata('err_msg', 'Sorry!! You do not have access to Meet The Icon Section.Please contact administrator.');
                redirect('home');
            }
        }
    }
    public function index()
    {
        redirect('pg_icon/videos_list');
    }
    /*
     * display index
     */
    public function videos_list()
    {
        $arrData=array();
        $record_per_page = RECORD_PER_PAGE;
        $total_records = $this->pg_model->get_all_videos();
        $total_records=count($total_records);
        $arrData['ui_pagging'] = zebra_paggination($total_records,$record_per_page);
        $current_page = $this->zebra_pagination->get_page();
        $current_page = $current_page -1;
        $start_index = $current_page * $record_per_page;
        $arrData['all_videos']=$this->pg_model->get_all_videos_page($start_index,$record_per_page);
        $arrData['categories'] = $this->category_model->get_all_records();
        $arrData['selected_cat'] = 'all';
        load_front_view('pg_icon',$arrData);
    }
    /*
     * display videos list by category
     */
    public function get_videos($cat_id)
    {
        $arrData=array();
        $record_per_page = RECORD_PER_PAGE;
        $total_records = $this->pg_model->get_cat_videos($cat_id);
        $total_records=count($total_records);
        $arrData['ui_pagging'] = zebra_paggination($total_records,$record_per_page);
        $current_page = $this->zebra_pagination->get_page();
        $current_page = $current_page -1;
        $start_index = $current_page * $record_per_page;

        $arrData['all_videos']=$this->pg_model->get_cat_videos_page($cat_id,$start_index,$record_per_page);
        $arrData['categories'] = $this->category_model->get_all_records();
        $arrData['selected_cat'] = $cat_id;
        load_front_view('pg_icon',$arrData);
    }
}