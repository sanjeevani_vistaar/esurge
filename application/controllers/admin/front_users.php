<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Front_users extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
       $this->load->model('user_model');
        $this->load->model('state_model');
        //$this->output->enable_profiler(TRUE);
    }


    /*
     * Delete user
     */
    public function delete_user($id)
    {
        $data = array(
            'is_deleted' =>1,
            'status' =>0
        );
        $sid = $this->user_model->update_user($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/front_users/all_list');
    }

    /*
     * Display all users
     */
    public function all_list($state_id=NULL)
    {
        $arrData['states']=$this->state_model->get_all_records();
        $arrData['all_records'] = $this->user_model->get_all_users_admin($state_id);
        $arrData['selected_state']=$state_id;
        $arrData['to_date']='';
        $arrData['from_date']='';
        load_admin_view('front_users/users_list',$arrData);
    }
    /*
     * Display all users
     */
    public function all_list_by_date($to_date=NULL,$from_date=NULL)
    {
        $arrData['states']=$this->state_model->get_all_records();
        $arrData['all_records'] = $this->user_model->get_all_users_by_date($to_date,$from_date);
        $arrData['to_date']=$to_date;
        $arrData['from_date']=$from_date;
        load_admin_view('front_users/users_list',$arrData);
    }

    /*
     * Change status
     */
    public function change_status($id)
    {
        $status=$this->user_model->get_user_status($id);
        $old_status=$status->status;
        if($old_status){
            $new_status=0;
        }else{
            $new_status=1;
        }
        $data = array(
            'status' =>$new_status
        );

        $sid = $this->user_model->update_user($id,$data);
        $this->session->set_flashdata('succ_msg', 'Status Changed Successfully!');
        redirect('admin/front_users/all_list');
    }

    // view last login  report
    public function tracking($user_id)
    {
        $arrData=array();
        $arrData['login_details']=$this->tracking_model->get_login_details($user_id);
        if($arrData['login_details']){
            $last_login = $arrData['login_details'][0]->login_date;
            $arrData['tracking_details']=$this->tracking_model->get_tracking_details($user_id,$last_login);
        }
        $arrData['user_id']=$user_id;
        $this->load->view('admin/front_users/analytics', $arrData);
    }
    // view all login  report
    public function all_tracking($user_id)
    {
        $arrData=array();
        $arrData['user_details']=$this->tracking_model->get_user_details($user_id);
        $arrData['tracking_details']=$this->tracking_model->get_all_tracking_details($user_id);
        load_admin_view('front_users/all_analytics',$arrData);
    }
    // download all login  report
    public function download_all_tracking($user_id)
    {
        $arrData=array();
        $arrData['user_details']=$this->tracking_model->get_user_details($user_id);

        $all_records=$this->tracking_model->get_all_tracking_details($user_id);
        if (!empty($all_records)) {
            $data = "<table border=1>
           <tr>
                <th> Source </th>
                <th> Title </th>
                <th> Number of Hits </th>
                <th> Date </th>
            </tr>";
            foreach ($all_records as $row) {
                $data .= "<tr><td>" . ucwords($row->source) . "</td><td>" . $row->title . "</td><td>" . $row->hit . "</td><td>" . date('d-m-Y',strtotime($row->view_date)) . "</td></tr>";
            }
            $data .= "</table>";
            // filename for download
            $filename = "Report_".$arrData['user_details']->first_name." ".$arrData['user_details']->last_name."_" . date('d-m-Y') . ".xls";

            header("Content-Disposition: attachment; filename=\"$filename\"");
            header("Content-Type: application/vnd.ms-excel");
            echo $data;
        }else{
            $this->session->set_flashdata('err_msg', 'No Records Found to download');
            redirect('admin/front_users/all_tracking/'.$user_id);
        }
    }
    /*
     * Download all users
     */
    public function download_all()
    {
        $all_records = $this->user_model->get_all_users_admin($state_id=NULL);
        if (!empty($all_records)) {
            $data = "<table border=1>
           <tr>
                <th> Fullname</th>
                <th> Email id </th>
                <th> Mobile No </th>
                <th> State </th>
                <th> Type </th>
                <th> Creation Date </th>
            </tr>";
            foreach ($all_records as $row) {
                $data .= "<tr><td>" . ucwords($row->first_name." ".$row->last_name) . "</td><td>" . $row->email_id . "</td><td>" . $row->mobile_no . "</td><td>" . $row->state_name . "</td><td>" . $row->user_type . "</td><td>" . date('d-m-Y',strtotime($row->created_on)) . "</td></tr>";
            }
            $data .= "</table>";
            // filename for download
            $filename = "Registered_All_Users" . date('d-m-Y') . ".xls";

            header("Content-Disposition: attachment; filename=\"$filename\"");
            header("Content-Type: application/vnd.ms-excel");
            echo $data;
        }else{
            $this->session->set_flashdata('err_msg', 'No Records Found to download');
            redirect('admin/front_users/all_list');
        }
    }
    public function download_all_by_date($to_date=NULL,$from_date=NULL)
    {
        $all_records = $this->user_model->get_all_users_by_date($to_date,$from_date);
        if (!empty($all_records)) {
            $data = "<table border=1>
           <tr>
                <th> Fullname</th>
                <th> Email id </th>
                <th> Mobile No </th>
                <th> State </th>
                <th> Type </th>
                <th> Creation Date </th>
            </tr>";
            foreach ($all_records as $row) {
                $data .= "<tr><td>" . ucwords($row->first_name." ".$row->last_name) . "</td><td>" . $row->email_id . "</td><td>" . $row->mobile_no . "</td><td>" . $row->state_name . "</td><td>" . $row->user_type . "</td><td>" . date('d-m-Y',strtotime($row->created_on)) . "</td></tr>";
            }
            $data .= "</table>";
            // filename for download
            $filename = "Registered_Users_".$to_date."-".$from_date . ".xls";

            header("Content-Disposition: attachment; filename=\"$filename\"");
            header("Content-Type: application/vnd.ms-excel");
            echo $data;
        }else{
            $this->session->set_flashdata('err_msg', 'No Records Found to download');
            redirect('admin/front_users/all_list');
        }
    }
}