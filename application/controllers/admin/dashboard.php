<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/14/2015
 * Time: 4:05 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('user_model');
        $this->load->model('admin_model');
        $this->load->model('videos_model');
        $this->load->model('pg_model');
        $this->load->model('uro_model');
        $this->load->model('journals_model');
       //$this->output->enable_profiler(TRUE);
    }

    /*
     * display index
     */
    public function index()
    {
        $arrData['all_users']=$this->user_model->get_all_users();
        $arrData['all_videos']=$this->videos_model->get_all_videos_admin();
        $arrData['all_pg_icon']=$this->pg_model->get_all_videos_admin();
        $arrData['all_uro']=$this->uro_model->get_all_records();
        $arrData['all_journals']=$this->journals_model->get_all_journals_admin();
        $arrData['all_news_hit']=$this->tracking_model->get_total_hit('news');
        $arrData['all_videos_hit']=$this->tracking_model->get_total_hit('videos');
        $arrData['all_journals_hit']=$this->tracking_model->get_total_hit('journals');
        $arrData['all_meettheicon_hit']=$this->tracking_model->get_total_hit('Meet The Icon');
        load_admin_view('dashboard1',$arrData);
    }

    public function tracking_report($page_source)
    {
        if($page_source=='meettheicon'){
            $page_source='Meet The Icon';
        }
        $arrData['tracking_details']=$this->tracking_model->get_page_tracking_details($page_source);
        $arrData['page_source']=$page_source;
        load_admin_view('all_tracking_reports',$arrData);
    }
    public function download_report($page_source)
    {
        if($page_source=='meettheicon'){
            $page_source='Meet The Icon';
        }
        $all_records=$this->tracking_model->get_page_tracking_details($page_source);
        if (!empty($all_records)) {
            $data = "<table border=1>
           <tr>
                <th> Title </th>
                <th> Number of Hits </th>
            </tr>";
            foreach ($all_records as $row) {
                $data .= "<tr><td>" . $row->title . "</td><td>" . $row->hit . "</td></tr>";
            }
            $data .= "</table>";
            // filename for download
            $filename = "Report_".$page_source."_" . date('d-m-Y') . ".xls";

            header("Content-Disposition: attachment; filename=\"$filename\"");
            header("Content-Type: application/vnd.ms-excel");
            echo $data;
        }else{
            $this->session->set_flashdata('err_msg', 'No Records Found to download');
            redirect('admin/dashboard/tracking_report/'.$page_source);
        }
    }

}