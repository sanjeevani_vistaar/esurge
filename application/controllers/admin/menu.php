<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('menu_model');
        //$this->output->enable_profiler(TRUE);
    }
    /*
     * display create menu form and save
     */
    public function add_menu()
    {
        $arrData['all_menu']=$this->menu_model->get_all_records_admin_priority();
        if (isset($_POST["submit"])) {

            if(empty($this->input->post("access_by"))){
                $this->session->set_flashdata('err_msg', 'Plesae allow to access atleast one user.');
                redirect('admin/menu/add_menu');
            }
            //set validations
            $this->form_validation->set_rules("title", "Menu Title", "trim|required|callback_title_check");
            $this->form_validation->set_rules("page_title", "SEO Title", "trim|required");
            $this->form_validation->set_rules("page_keywords", "SEO Keywords", "trim|required");
            $this->form_validation->set_rules("page_desc", "SEO Description", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/menu/add_menu');
            } else {
                $pid = $this->menu_model->add_priority($this->input->post("priority"));
                $data = array(
                    'menu_title' =>strtolower(trim($this->input->post("title"))),
                    'priority' =>$this->input->post("priority")+1,
                    'status' =>1,
                    'is_login_required' =>$this->input->post("is_login_required"),
                    'access_by' =>implode(',',$this->input->post("access_by")),
                    'seo_title' =>trim($this->input->post("page_title")),
                    'seo_keywords' =>trim($this->input->post("page_keywords")),
                    'seo_desc' =>trim($this->input->post("page_desc")),
                    'created_on' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->menu_model->insert_menu($data);
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('admin/menu/all_list');
            }
        }else{
            load_admin_view('menus/add_menu_form',$arrData);
        }
    }

    /*
      * display edit menu form and save
      */
    public function edit_menu($id)
    {
        $arrData=array();
        $arrData['menu_details']=$this->menu_model->get_menu_records($id);
        $arrData['all_menu']=$this->menu_model->get_all_records_admin_priority();
        $arrData['selected_menu']=$id;

        if (isset($_POST["submit"])) {

            if(empty($this->input->post("access_by"))){
                $this->session->set_flashdata('err_msg', 'Plesae allow to access atleast one user.');
                redirect('admin/menu/edit_menu/'.$id,$arrData);
            }
            //set validations
            if($arrData['menu_details'][0]->menu_title != strtolower(trim($this->input->post("title")))){
                $this->form_validation->set_rules("title", "Menu Title", "trim|required|callback_title_check");
            }else {
                $this->form_validation->set_rules("title", "Menu Title", "trim|required");
            }
            $this->form_validation->set_rules("page_title", "SEO Title", "trim|required");
            $this->form_validation->set_rules("page_keywords", "SEO Keywords", "trim|required");
            $this->form_validation->set_rules("page_desc", "SEO Description", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/menu/edit_menu/'.$id,$arrData);
            } else {
                if($arrData['menu_details'][0]->priority > $this->input->post("priority"))
                {
                $priority=$this->input->post("priority")+1;}
            else{
                $priority=$this->input->post("priority");
            }
                $pid = $this->menu_model->update_priority($arrData['menu_details'][0]->priority,$this->input->post("priority"));
                $data = array(
                    'status' =>$this->input->post("status"),
                    'priority' =>$priority,
                    'is_login_required' =>$this->input->post("is_login_required"),
                    'access_by' =>implode(',',$this->input->post("access_by")),
                    'seo_title' =>trim($this->input->post("page_title")),
                    'seo_keywords' =>trim($this->input->post("page_keywords")),
                    'seo_desc' =>trim($this->input->post("page_desc")),
                    'modified_on' => date('Y-m-d'),
                    'modified_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->menu_model->update_menu($id,$data);

                $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                redirect('admin/menu/all_list');
            }
        }else{
            load_admin_view('menus/edit_menu_form',$arrData);
        }
    }
//check title already exsists or not
    public function title_check($title)
    {
        $already_exsist=$this->menu_model->check_exsisting_menu(strtolower(trim($title)));
        if (!empty($already_exsist))
        {
            $this->form_validation->set_message('title_check', 'Menu Already Added.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    /*
     * Delete menu
     */
    public function delete_menu($id)
    {
        $data = array(
            'is_deleted' =>1,
            'status' =>0
        );
        $sid = $this->menu_model->update_menu($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/menu/all_list');
    }

    /*
     * Display all menus
     */
    public function all_list()
    {
        $all_records = $this->menu_model->get_all_records_admin();
        load_admin_view('menus/menu_list',array('arrData' => $all_records));
    }

}