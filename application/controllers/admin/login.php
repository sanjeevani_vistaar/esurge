<?php
/**
 * Login
 * User: Mukesh
 * Date: 10/21/2015
 *
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        //load the login model
        $this->load->model('login_model');
        $this->load->model('admin_model');

    }

    public function index()
    {
        // check if user loggedin then redirect to home page
        if($this->session->userdata('admin_id')){
            redirect('admin/dashboard');
        }
        if (isset($_POST["submit"]) && !empty($_POST["submit"])) {

            //get the posted values
            $username = $this->input->post("username");
            $password = $this->input->post("password");

            //set validations
            $this->form_validation->set_rules("username", "Username", "trim|required");
            $this->form_validation->set_rules("password", "Password", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                //validation fails
                load_login_view('admin/login');
            } else {
                //validation succeeds
                if ($this->input->post('submit') == "Login") {
                    //check if username and password is correct
                    $usr_result = $this->login_model->get_admin_user($username, $password);
                    if ($usr_result){
                    $admin_id = $usr_result->admin_id;
                    $admin_firstname = $usr_result->first_name;
                    $admin_lastname = $usr_result->last_name;
                    $admin_type=$usr_result->role_id;
                    $admin_role=$usr_result->role;
                    }

                    if (!empty($usr_result)) //active user record is present
                    {

                        //set the session variables
                        $sessiondata = array(
                            'admin_id' => $admin_id,
                            'adminname' => $admin_firstname . " " . $admin_lastname,
                            'admintype' => $admin_type,
                            'adminrole' => $admin_role,
                            'loginuser' => TRUE
                        );
                        $this->session->set_userdata($sessiondata);

                        if ($admin_type==1) {
                            //for super admin
                            redirect('admin/dashboard');
                        }elseif($admin_type==2) {
                            // for admin
                            echo "hi admin";
                        }else{
                           // for moderator
                            echo "hi moderator";
                        }
                    } else {
                        $this->session->set_flashdata('err_msg', 'Invalid username and password!');
                        redirect('admin/login/index');
                    }
                } else {
                    redirect('admin/login/index');
                }
            }
        }else{
            load_login_view('admin/login');
        }
    }
    /*
     * forgot password
     */
    public function forgot_password()
    {
        if (isset($_POST["submit"]) && !empty($_POST["submit"])) {
            $user_email=$this->input->post("email");
            $admin_detials = $this->admin_model->get_details_by_email($user_email);

            if (!empty($admin_detials)) {
                $adminname = ucwords($admin_detials->first_name . " " . $admin_detials->last_name);
                $Characteres = 'ABCD0123456789EFGHIJKLMOPQRSTUVXWYZ';
                $Characteres_length = strlen($Characteres);
                $Characteres_length--;
                $pwd = NULL;
                for ($x = 1; $x <= 4; $x++) {
                    $Posicao = rand(0, $Characteres_length);
                    $pwd .= substr($Characteres, $Posicao, 1);
                }

                $data = array(
                    'password' => md5(sha1($pwd))
                );

                $sid = $this->admin_model->update_password($user_email, $data);

                if ($sid) {
                    $to = $user_email;
                    $subject = "Reset Password";
                    $message = reset_password_msg($adminname,$pwd);

                    send_email($to, $subject, $message);
                    $this->session->set_flashdata('succ_msg', 'New password has been sent to given email id');
                } else {
                    $this->session->set_flashdata('err_msg', 'Sorry !!! Please Contact Administrator.');
                }
            }else{
                $this->session->set_flashdata('err_msg', 'Sorry !!! Record not available.');
            }
             redirect('admin/login/forgot_password');
        }
        else{
            load_login_view('admin/forgot_password');
        }
    }
/*
 * Logout function
 */
    function logout()
    {
        $this->session->sess_destroy();
        redirect('admin/login');
    }
}