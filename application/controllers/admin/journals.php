<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Journals extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('journals_model');
        //$this->output->enable_profiler(TRUE);
    }
     /*
      * display create journal form and save
      */
    public function create_journal()
    {

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("desc", "Description", "trim|required");
            $this->form_validation->set_rules("link", "Link", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            $sessiondata = array(
                'title' => $this->input->post("title"),
                'desc' => $this->input->post("desc"),
                'link' => $this->input->post("link"),
                'status' => $this->input->post("status")
            );
            $this->session->set_userdata($sessiondata);

            if ($this->form_validation->run() == FALSE) {
                 $this->session->set_flashdata('err_msg', validation_errors());
                load_admin_view('journals/create_journal_form');
            } else {
                if ($_FILES['img_path']['name']) {
                    $ext = pathinfo($_FILES['img_path']['name'], PATHINFO_EXTENSION);
                    $allowed_ext=array('jpg','jpeg','png');
                    if(!in_array($ext,$allowed_ext))
                    {
                        $this->session->set_flashdata('err_msg', 'Selected File extension not allowed !');
                        redirect('admin/journals/create_journal');
                    }
                    $image_info = getimagesize($_FILES["img_path"]["tmp_name"]);
                    $image_width = $image_info[0];
                    $image_height = $image_info[1];
                    if($image_width != 284 && $image_height !=148){
                        $this->session->set_flashdata('err_msg', 'Uploaded file does not fit into the allowed dimensions !');
                        redirect('admin/journals/create_journal');
                    }
                    $new_file_name = "journal-" . time();

                    $config['upload_path'] = './uploads/journals/'; /* NB! create this dir! */
                    $config['allowed_types'] = '*';/* Passing the extension to be upload */
                    $config['file_name'] = $new_file_name;
                    $config['max_width']  = '284';
                    $config['max_height']  = '148';
                    //Loading library for uploading a file with configuration setting
                    $this->load->library('upload', $config);


                    //Checking whether file is uploaded
                    if (!$this->upload->do_upload('img_path')) {
                        $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                        load_admin_view('journals/create_journal_form');
                    } else {
                        $img_data = $this->upload->data();
                        $img_path = "/uploads/journals/" . $img_data['file_name'];
                    }
                }
                    $data = array(
                        'journal_title' => $this->input->post("title"),
                        'journal_desc' => $this->input->post("desc"),
                        'journal_link' => $this->input->post("link"),
                        'journal_img_path' => $img_path,
                        'status' => $this->input->post("status"),
                        'created_on' => date('Y-m-d'),
                        'created_by' => $this->session->userdata('admin_id')
                    );
                    $sid = $this->journals_model->insert_journal($data);
                    $this->clear_session();
                    $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                    redirect('admin/journals/all_list');

            }
        }else{
            load_admin_view('journals/create_journal_form');
        }
    }

    /*
      * display edit journal form and save
      */
    public function edit_journal($id)
    {
        $arrData=array();
        $arrData['journal_details']=$this->journals_model->get_journal_records($id);

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("desc", "Description", "trim|required");
            $this->form_validation->set_rules("link", "Link", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                 $this->session->set_flashdata('err_msg', validation_errors());
                load_admin_view('journals/edit_journal_form');
            } else {
                if ($_FILES['img_path']['name']) {
                    $ext = pathinfo($_FILES['img_path']['name'], PATHINFO_EXTENSION);
                    $allowed_ext=array('jpg','jpeg','png');
                    if(!in_array($ext,$allowed_ext))
                    {
                        $this->session->set_flashdata('err_msg', 'Selected File extension not allowed !');
                        redirect('admin/journals/edit_journal/'.$id);
                    }
                    $image_info = getimagesize($_FILES["img_path"]["tmp_name"]);
                    $image_width = $image_info[0];
                    $image_height = $image_info[1];
                    if($image_width != 284 && $image_height !=148){
                        $this->session->set_flashdata('err_msg', 'Uploaded file does not fit into the allowed dimensions !');
                        redirect('admin/journals/edit_journal/'.$id);
                    }
                    $new_file_name = "journal-" . time();

                    $config['upload_path'] = './uploads/journals/'; /* NB! create this dir! */
                    $config['allowed_types'] = '*';/* Passing the extension to be upload */
                    $config['file_name'] = $new_file_name;
                    $config['max_width']  = '284';
                    $config['max_height']  = '148';
                    //Loading library for uploading a file with configuration setting
                    $this->load->library('upload', $config);


                    //Checking whether file is uploaded
                    if (!$this->upload->do_upload('img_path')) {
                        $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                        load_admin_view('journals/edit_journal_form',$arrData);
                    } else {
                        $img_data = $this->upload->data();
                        $img_path = "/uploads/journals/" . $img_data['file_name'];
                        $data['journal_img_path']=$img_path;
                    }
                }

                $data['journal_title']=$this->input->post("title");
                $data['journal_desc']=$this->input->post("desc");
                $data['journal_link']=$this->input->post("link");
                $data['status']=$this->input->post("status");
                $data['modified_on']=date('Y-m-d');
                $data['modified_by']=$this->session->userdata('admin_id');

                $sid = $this->journals_model->update_journal($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record updated Successfully!');
                redirect('admin/journals/all_list');
            }
        }else{
            load_admin_view('journals/edit_journal_form',$arrData);
        }
    }

    /*
     * Delete journal
     */
    public function delete_journal($id)
    {
        $data = array(
            'is_deleted' =>1,
            'status' =>0
        );
        $sid = $this->journals_model->update_journal($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/journals/all_list');
    }

    /*
     * Display all journals
     */
    public function all_list()
    {
        $this->clear_session();
        $all_records = $this->journals_model->get_all_journals_admin();
        load_admin_view('journals/journals_list',array('arrData' => $all_records));
    }
    public function clear_session()
    {
        $this->session->unset_userdata('title');
        $this->session->unset_userdata('desc');
        $this->session->unset_userdata('link');
        $this->session->unset_userdata('status');
    }
}