<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class City extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('city_model');
        $this->load->model('state_model');
        //$this->output->enable_profiler(TRUE);
    }
     /*
      * display create city form and save
      */
    public function create_city()
    {
        $arrData=array();
        $arrData['states']=$this->state_model->get_all_records();
        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("name", "City name", "trim|required");


            if ($this->form_validation->run() == FALSE) {
                //validation fails
                load_admin_view('city/create_city_form');
            } else {
                $data = array(
                    'city_name' =>$this->input->post("name"),
                    'state_id' =>$this->input->post("state_id"),
                    'created_on' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->city_model->insert_city($data);
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('admin/city/all_list');
            }
        }else{
            load_admin_view('city/create_city_form',$arrData);
        }
    }

    /*
      * display edit city form and save
      */
    public function edit_city($id)
    {
        $arrData=array();
        $arrData['city_details']=$this->city_model->get_city_records($id);
        $arrData['states']=$this->state_model->get_all_records();
        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("name", "city name", "trim|required");


            if ($this->form_validation->run() == FALSE) {
                //validation fails
                load_admin_view('city/edit_city_form',$arrData);
            } else {
                $data = array(
                    'city_name' =>$this->input->post("name"),
                    'state_id' =>$this->input->post("state_id"),
                    'modified_on' => date('Y-m-d'),
                    'modified_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->city_model->update_city($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                redirect('admin/city/all_list');
            }
        }else{
            load_admin_view('city/edit_city_form',$arrData);
        }
    }

    /*
     * Delete city
     */
    public function delete_city($id)
    {
        $data = array(
            'is_deleted' =>1
        );
        $sid = $this->city_model->update_city($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/city/all_list');
    }

    /*
     * Display all citys
     */
    public function all_list()
    {
        $all_records = $this->city_model->get_all_records();
        load_admin_view('city/city_list',array('arrData' => $all_records));
    }

}