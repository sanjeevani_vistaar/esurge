<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Uro extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('uro_model');
        $this->load->model('category_model');
        //$this->output->enable_profiler(TRUE);
    }
     /*
      * display upload file form and save
      */
    public function upload_file()
    {
        $arrData['categories'] = $this->category_model->get_uros_records();
        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("desc", "Description", "trim|required");
            $this->form_validation->set_rules("file_path", "Select File", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            $sessiondata = array(
                'uro_cat' => implode(',',$this->input->post("category")),
                'uro_title' => $this->input->post("title"),
                'uro_desc' => $this->input->post("desc"),
                'status' => $this->input->post("status"),
                'tags' => $this->input->post("tags")
            );
            $this->session->set_userdata($sessiondata);

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/uro/upload_file');
            } else {
                $ext = pathinfo($this->input->post("file_path"), PATHINFO_EXTENSION);
                $allowed_ext=array('pptx','ppt');
                if(!in_array($ext,$allowed_ext))
                {
                    $this->session->set_flashdata('err_msg', 'Selected File extension not allowed !');
                    redirect('admin/uro/upload_file');
                }
                    $file_name=time().'.'.$ext;
                    $data = array(
                        'cat_id' => implode(',',$this->input->post("category")),
                        'uro_title' => $this->input->post("title"),
                        'uro_desc' => $this->input->post("desc"),
                        'tags' => $this->input->post("tags"),
                        'file_path' => '/uploads/uros/'.$file_name,
                        'status' => $this->input->post("status"),
                        'created_on' => date('Y-m-d'),
                        'created_by' => $this->session->userdata('admin_id')
                    );
                    $sid = $this->uro_model->insert_record($data);
                    rename(".".$this->input->post("file_path"), "./uploads/uros/".$file_name);
                    $this->clear_session();
                    $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                    redirect('admin/uro/all_list');

            }
        }else{
            load_admin_view('uro/upload_file_form',$arrData);
        }
    }

    /*
      * display edit file form and save
      */
    public function edit_file($id)
    {
        $arrData=array();
        $arrData['file_details']=$this->uro_model->get_records($id);
        $arrData['categories'] = $this->category_model->get_uros_records();
        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("desc", "Description", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                load_admin_view('uros/edit_file_form',$arrData);
            } else {
                if ($this->input->post("file_path")) {

                    $ext = pathinfo($this->input->post("file_path"), PATHINFO_EXTENSION);
                    $allowed_ext=array('pptx','ppt');
                    if(!in_array($ext,$allowed_ext))
                    {
                        $this->session->set_flashdata('err_msg', 'Selected File extension not allowed !');
                        redirect('admin/uro/edit_file/'.$id);
                    }
                    $file_name=time().'.'.$ext;
                    rename(".".$this->input->post("file_path"), "./uploads/uros/".$file_name);
                    $data['file_path']='/uploads/uros/'.$file_name;
                }
                $data['cat_id'] = implode(',',$this->input->post("category"));
                $data['uro_title']=$this->input->post("title");
                $data['uro_desc']=$this->input->post("desc");
                $data['tags']=$this->input->post("tags");
                $data['status']=$this->input->post("status");
                $data['modified_on']=date('Y-m-d');
                $data['modified_by']=$this->session->userdata('admin_id');

                $sid = $this->uro_model->update_record($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record updated Successfully!');
                redirect('admin/uro/all_list');
            }
        }else{
            load_admin_view('uro/edit_file_form',$arrData);
        }
    }

    /*
     * Delete file
     */
    public function delete_file($id)
    {
        $data = array(
            'is_deleted' =>1,
            'status' =>0
        );
        $sid = $this->uro_model->update_record($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/uro/all_list');
    }

    /*
     * Display all files
     */
    public function all_list()
    {
        $this->clear_session();
        $all_records = $this->uro_model->get_all_records();
        load_admin_view('uro/files_list',array('arrData' => $all_records));
    }
    /*
        * Display all categories
        */
    public function categories_list()
    {
        $all_records = $this->category_model->get_uros_records();
        load_admin_view('uro/categories_list',array('arrData' => $all_records));
    }
    /*
          * display create category form and save
          */
    public function create_category()
    {
        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("name", "Category name", "trim|required|callback_category_check");


            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/uro/create_category');
            } else {
                $data = array(
                    'cat_name' =>strtoupper($this->input->post("name")),
                    'created_on' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->category_model->insert_uro_category($data);
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('admin/uro/categories_list');
            }
        }else{
            load_admin_view('uro/create_category_form');
        }
    }

    /*
      * display edit category form and save
      */
    public function edit_category($id)
    {
        $arrData=array();
        $arrData['cat_details']=$this->category_model->get_cat_uro_records($id);

        if (isset($_POST["submit"])) {

            //set validations
            if($arrData['cat_details'][0]->cat_name == strtoupper($this->input->post("name")))
            {
                $this->form_validation->set_rules("name", "Category name", "trim|required");
            }else{
                $this->form_validation->set_rules("name", "Category name", "trim|required|callback_category_check");
            }

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/uro/edit_category/'.$id,$arrData);
            } else {
                $data = array(
                    'cat_name' =>strtoupper($this->input->post("name")),
                    'modified_on' => date('Y-m-d'),
                    'modified_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->category_model->update_uro_category($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                redirect('admin/uro/categories_list');
            }
        }else{
            load_admin_view('uro/edit_category_form',$arrData);
        }
    }

    /*
     * Delete category
     */
    public function delete_category($id)
    {
        $data = array(
            'is_deleted' =>1
        );
        $sid = $this->category_model->update_uro_category($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/uro/categories_list');
    }

    //check category already exsists or not
    public function category_check($cat)
    {
        $already_exsist=$this->category_model->check_exsisting_uro_category(strtoupper($cat));
        if (!empty($already_exsist))
        {
            $this->form_validation->set_message('category_check', 'Category Already Added.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    public function clear_session()
    {
        $this->session->unset_userdata('uro_title');
        $this->session->unset_userdata('uro_cat');
        $this->session->unset_userdata('uro_desc');
        $this->session->unset_userdata('tags');
        $this->session->unset_userdata('status');
    }
}