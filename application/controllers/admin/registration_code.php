<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/14/2015
 * Time: 4:05 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration_code extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('registrationcode_model');
        //$this->output->enable_profiler(TRUE);
    }

   /*
     * display registration code generate form
     */
    public function gererate_registration_code()
    {
        if (isset($_POST["submit"])) {
            //set validations
            $this->form_validation->set_rules("fullname", "Fullname", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required");
            $this->form_validation->set_rules("mobile", "Mobile", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/registration_code/gererate_registration_code');
            } else {
                $fullname = $this->input->post("fullname");
                $email = $this->input->post("email");
                $mobile = $this->input->post("mobile");
                $data = array(
                    'full_name' =>$fullname,
                    'email_id' =>$email,
                    'mobile_no' =>$mobile,
                    'registration_code'=>REGISTRATION_CODE_PREFIX,
                    'status' =>1,
                    'created_on' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->registrationcode_model->insert_registration_code($data);
                if($sid){
                    $data = array(
                        'registration_code'=>REGISTRATION_CODE_PREFIX.$sid
                    );
                    $this->registrationcode_model->update_registration_code($sid,$data);
                }
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('admin/registration_code/codes_list');
            }
        }else{
            load_admin_view('registration_code/generate_registration_code_form');
        }
    }

    /*
      * display create admin form and save
      */
    public function edit_details($id)
    {
        $arrData=array();
        $arrData['details']=$this->registrationcode_model->get_records($id);

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("fullname", "Fullname", "trim|required");
            //$this->form_validation->set_rules("email", "Email", "trim|required");
            //$this->form_validation->set_rules("mobile", "Mobile", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/registration_code/edit_details/'.$id);
                //load_admin_view('admin_users/edit_admin_form',$arrData);
            } else {
                $fullname = $this->input->post("fullname");
                $email = $this->input->post("email");
                $mobile = $this->input->post("mobile");
                $reg_code = $this->input->post("reg_code");
                $data = array(
                    'full_name' =>$fullname,
                    'email_id' =>$email,
                    'mobile_no' =>$mobile,
                    'registration_code' =>$reg_code,
                );
                $this->registrationcode_model->update_registration_code($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                redirect('admin/registration_code/codes_list');
            }
        }else{
            load_admin_view('registration_code/edit_registration_code_form',$arrData);
        }
    }

    /*
     * Display all registration codes
     */
    public function codes_list()
    {
        $all_records = $this->registrationcode_model->get_all_regisration_codes();
        load_admin_view('registration_code/registration_codes_list',array('arrData' => $all_records));
    }

    public function upload_codes()
    {
        ini_set('max_execution_time', 0);
        if (isset($_POST["submit"])) {
            if ($_FILES['file_path']['name']) {

                $config['upload_path'] = './uploads/tmp/'; /* NB! create this dir! */
                $config['allowed_types'] = '*';/* Passing the extension to be upload */
                //Loading library for uploading a file with configuration setting
                $this->load->library('upload', $config);


                //Checking whether file is uploaded
                if (!$this->upload->do_upload('file_path')) {
                    $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                    redirect('admin/registration_code/upload_codes/');

                } else {
                    $arrData['data'] = $this->upload->data();
                    /** Include PHPExcel */
                    require_once __DIR__ . '/../../libraries/excelreader/PHPExcel/IOFactory.php';
                    require_once __DIR__ . '/../../libraries/excelreader/PHPExcel.php';

                    $inputFileName = $arrData['data']['full_path'];

                    //  Read your Excel workbook

                    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFileName);

                    $objWorksheet = $objPHPExcel->getActiveSheet();
                    $highestRow = $objWorksheet->getHighestRow();
                    $highestColumn = $objWorksheet->getHighestColumn();
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    $rows = array();
                    $counter = 0;
                    for ($row = 2; $row <= $highestRow; ++$row) {
                        $full_name = $objWorksheet->getCellByColumnAndRow(0, $row)->getValue();
                        $code = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();

                        $fullname=preg_replace('/\s+/', ' ', $full_name);
                        /* add registration code while registration :: start Remove in next version launch*/
                        $data = array(
                            'full_name' => strtolower(trim($fullname)),
                            'email_id' => '',
                            'mobile_no' => '',
                            'registration_code' => trim($code),
                            'status' => 1,
                            'created_on' => date('Y-m-d'),
                            'created_by' => 1
                        );
                        $ssid = $this->registrationcode_model->insert_registration_code($data);

                    }
                    $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                    redirect('admin/registration_code/codes_list');

                }
            }
        }else{
            load_admin_view('registration_code/upload_file');
        }
    }


}