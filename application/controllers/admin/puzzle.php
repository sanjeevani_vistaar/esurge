<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Puzzle extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
       $this->load->model('puzzle_model');
        //$this->output->enable_profiler(TRUE);
    }
     /*
      * display create admin form and save
      */
    public function create_puzzle()
    {
        $arrData=array();
        $arrData['categories'] = $this->puzzle_model->get_all_records();
       if (isset($_POST["submit"])) {

            //set validations
           $this->form_validation->set_rules("category", "Category", "trim|required");
            $this->form_validation->set_rules("puzz_question", "Question", "trim|required");
            $this->form_validation->set_rules("opt_1", "Option 1", "trim|required");
            $this->form_validation->set_rules("opt_2", "Option 2", "trim|required");
            $this->form_validation->set_rules("opt_3", "Option 3", "trim|required");
            $this->form_validation->set_rules("opt_4", "Option 4", "trim|required");
            $this->form_validation->set_rules("corr_answer", "Correct Answer", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/puzzle/create_puzzle');
                //load_admin_view('admin_users/create_admin_form',$arrData);
            } else {
                $data = array(
                    'cat_id' => $this->input->post("category"),
                    'puzz_question' =>$this->input->post("puzz_question"),
                    'option_1' =>$this->input->post("opt_1"),
                    'option_2' =>$this->input->post("opt_2"),
                    'option_3' =>$this->input->post("opt_3"),
                    'option_4' =>$this->input->post("opt_4"),
                    'correct_answer' =>$this->input->post("corr_answer"),
                    'status' =>$this->input->post("status"),
                );
                $sid = $this->puzzle_model->insert_puzzle($data);
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('admin/puzzle/all_list');
            }
        }else{
            load_admin_view('puzzle/create_puzzle_form',$arrData);
        }
    }

    /*
      * display create admin form and save
      */
    public function edit_puzzle($id)
    {
        $arrData=array();
        $arrData['puzz_details']=$this->puzzle_model->get_puzzle($id);
        $arrData['categories'] = $this->puzzle_model->get_all_records();
        if (isset($_POST["submit"])) {
            //set validations
            $this->form_validation->set_rules("category", "Category", "trim|required");
            $this->form_validation->set_rules("puzz_question", "Question", "trim|required");
            $this->form_validation->set_rules("opt_1", "Option 1", "trim|required");
            $this->form_validation->set_rules("opt_2", "Option 2", "trim|required");
            $this->form_validation->set_rules("opt_3", "Option 3", "trim|required");
            $this->form_validation->set_rules("opt_4", "Option 4", "trim|required");
            $this->form_validation->set_rules("corr_answer", "Correct Answer", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/puzzle/create_puzzle');
                //load_admin_view('admin_users/create_admin_form',$arrData);
            } else {
                $data = array(
                    'cat_id' => $this->input->post("category"),
                    'puzz_question' => $this->input->post("puzz_question"),
                    'option_1' => $this->input->post("opt_1"),
                    'option_2' => $this->input->post("opt_2"),
                    'option_3' => $this->input->post("opt_3"),
                    'option_4' => $this->input->post("opt_4"),
                    'correct_answer' => $this->input->post("corr_answer"),
                    'status' => $this->input->post("status"),
                );

                $sid = $this->puzzle_model->update_puzzle($id, $data);
                $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                redirect('admin/puzzle/all_list');
            }
        }
        else{
            load_admin_view('puzzle/edit_puzzle_form',$arrData);
        }
    }


    /*
     * Delete admin
     */
    public function delete_puzzle($id)
    {
        $data = array(
            'is_deleted' =>1,
            'status' =>0
        );
        $sid = $this->puzzle_model->update_puzzle($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/puzzle/all_list');
    }

    /*
     * Display all Admin users
     */
    public function all_list()
    {
        $all_records = $this->puzzle_model->get_all_puzzles_admin();
        load_admin_view('puzzle/puzzle_list',array('arrData' => $all_records));
    }
    // tracking report
    public function tracking($id)
    {
        $arrData=array();
        $arrData['tracking_details']=$this->puzzle_model->get_puzzle_tracking($id);
        $this->load->view('admin/puzzle/tracking', $arrData);
    }
    /*
       * Display all categories
       */
    public function categories_list()
    {
        $all_records = $this->puzzle_model->get_all_records();
        load_admin_view('puzzle/categories_list',array('arrData' => $all_records));
    }
    /*
      * display create category form and save
      */
    public function create_category()
    {

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("name", "Category name", "trim|required|callback_category_check");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/puzzle/create_category');
            } else {
                $data = array(
                    'cat_name' =>strtoupper($this->input->post("name")),
                    'created_on' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->puzzle_model->insert_category($data);
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('admin/puzzle/categories_list');
            }
        }else{
            load_admin_view('puzzle/create_category_form');
        }
    }

    /*
      * display edit category form and save
      */
    public function edit_category($id)
    {
        $arrData=array();
        $arrData['cat_details']=$this->puzzle_model->get_cat_records($id);

        if (isset($_POST["submit"])) {

            //set validations
            if($arrData['cat_details'][0]->cat_name == strtoupper($this->input->post("name")))
            {
                $this->form_validation->set_rules("name", "Category name", "trim|required");
            }else{
                $this->form_validation->set_rules("name", "Category name", "trim|required|callback_category_check");
            }
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/puzzle/edit_category/'.$id,$arrData);
            } else {
                $data = array(
                    'cat_name' =>strtoupper($this->input->post("name")),
                    'modified_on' => date('Y-m-d'),
                    'modified_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->puzzle_model->update_category($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                redirect('admin/puzzle/categories_list');
            }
        }else{
            load_admin_view('puzzle/edit_category_form',$arrData);
        }
    }

    /*
     * Delete category
     */
    public function delete_category($id)
    {
        $data = array(
            'is_deleted' =>1
        );
        $sid = $this->puzzle_model->update_category($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/puzzle/categories_list');
    }

    //check category already exsists or not
    public function category_check($cat)
    {
        $already_exsist=$this->puzzle_model->check_exsisting_puzzle_category(strtoupper($cat));
        if (!empty($already_exsist))
        {
            $this->form_validation->set_message('category_check', 'Category Already Added.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
}