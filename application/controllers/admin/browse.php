<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 2/9/2016
 * Time: 1:06 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Browse extends CI_Controller
{
    private $videos_directory = "uploads/tmp/videos";
    private $pg_directory = "uploads/tmp/pg";
    private $webcast_directory = "uploads/tmp/webcast";
    private $othervideos_directory = "uploads/tmp/othervideos";
    private $uros_directory = "uploads/tmp/uros";
    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        } elseif ($this->session->userdata('admintype') == 2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
    }

    /*
     * browse video from server folder
     *
     */
    public function browse_videos(){
        $arrData = array();
        $files = scandir($this->videos_directory,1);
        $files = array_diff($files,array('.','..'));
        foreach($files as $file){
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $video_arr=array('mp4');
            if(in_array($ext,$video_arr))
            {
                $arrData['videos_list'][$file] = filemtime($this->videos_directory.'/'.$file);
            }
        }
        $arrData['dir_path'] = $this->videos_directory;
        $this->load->view('admin/browse/browse_videos',$arrData);
    }
    /*
     * browse video from server folder
     *
     */
    public function browse_pg(){
        $arrData = array();
        $files = scandir($this->pg_directory,1);
        $files = array_diff($files,array('.','..'));
        foreach($files as $file){
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $video_arr=array('mp4');
            if(in_array($ext,$video_arr))
            {
                $arrData['videos_list'][$file] = filemtime($this->pg_directory.'/'.$file);
            }
        }
        $arrData['dir_path'] = $this->webcast_directory;
        $this->load->view('admin/browse/browse_pg',$arrData);
    }
    /*
     * browse video from server folder
     *
     */
    public function browse_uros(){
        $arrData = array();
        $files = scandir($this->uros_directory,1);
        $files = array_diff($files,array('.','..'));
        foreach($files as $file){
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $file_arr=array('pptx','ppt');
            if(in_array($ext,$file_arr))
            {
                $arrData['file_list'][$file] = filemtime($this->uros_directory.'/'.$file);
            }
        }
        $arrData['dir_path'] = $this->uros_directory;
        $this->load->view('admin/browse/browse_uros',$arrData);
    }

    public function browse_webcast(){
        $arrData = array();
        $files = scandir($this->webcast_directory,1);
        $files = array_diff($files,array('.','..'));
        foreach($files as $file){
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $video_arr=array('mp4');
            if(in_array($ext,$video_arr))
            {
                $arrData['videos_list'][$file] = filemtime($this->webcast_directory.'/'.$file);
            }
        }
        $arrData['dir_path'] = $this->webcast_directory;
        $this->load->view('admin/browse/browse_webcast',$arrData);
    }

    public function browse_othervideos(){
        $arrData = array();
        $files = scandir($this->othervideos_directory,1);
        $files = array_diff($files,array('.','..'));
        foreach($files as $file){
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $video_arr=array('mp4');
            if(in_array($ext,$video_arr))
            {
                $arrData['videos_list'][$file] = filemtime($this->othervideos_directory.'/'.$file);
            }
        }
        $arrData['dir_path'] = $this->othervideos_directory;
        $this->load->view('admin/browse/browse_othervideos',$arrData);
    }

}