<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/21/2015
 * Time: 10:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Pg_icon extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        } elseif ($this->session->userdata('admintype') == 2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('pg_model');
        $this->load->model('category_model');
        //$this->output->enable_profiler(TRUE);
    }
    /*
      * display upload video form and save
      */
    public function upload_video()
    {
        $arrData = array();
        $arrData['categories'] = $this->category_model->get_all_records();
        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("desc", "Description", "trim|required");
            $this->form_validation->set_rules("video_path", "Select Video", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            $sessiondata = array(
                'vid_cat' => implode(',',$this->input->post("category")),
                'vid_title' => $this->input->post("title"),
                'vid_desc' => $this->input->post("desc"),
                'status' => $this->input->post("status"),
                'tags' => $this->input->post("tags")
            );
            $this->session->set_userdata($sessiondata);

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/pg_icon/upload_video');
            } else {

                if ($_FILES['img_path']['name']) {
                    $ext = pathinfo($_FILES['img_path']['name'], PATHINFO_EXTENSION);
                    $allowed_ext=array('jpg','jpeg','png');
                    if(!in_array($ext,$allowed_ext))
                    {
                        $this->session->set_flashdata('err_msg', 'Selected File extension not allowed !');
                        redirect('admin/pg_icon/upload_video');
                    }
                    $image_info = getimagesize($_FILES["img_path"]["tmp_name"]);
                    $image_width = $image_info[0];
                    $image_height = $image_info[1];
                    if($image_width != 262 && $image_height !=152){
                        $this->session->set_flashdata('err_msg', 'Uploaded file does not fit into the allowed dimensions !');
                        redirect('admin/pg_icon/upload_video');
                    }
                    $new_file_name = "pg-image-" . time();

                    $config1['upload_path'] = './uploads/pg/images'; /* NB! create this dir! */
                    $config1['allowed_types'] = '*';/* Passing the extension to be upload */
                    $config1['file_name'] = $new_file_name;
                    $config1['max_width']  = '262';
                    $config1['max_height']  = '152';
                    //Loading library for uploading a file with configuration setting
                   // echo "<pre>";print_r($config1);die;
                    $this->load->library('upload', $config1);
                    $this->upload->initialize($config1);

                    //Checking whether file is uploaded
                    if (!$this->upload->do_upload('img_path')) {
                        $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                        redirect('admin/pg_icon/upload_video');
                    } else {
                        $img_data = $this->upload->data();
                        $img_path = "/uploads/pg/images/" . $img_data['file_name'];
                    }
                }else{
                    $this->session->set_flashdata('err_msg', 'Please select image');
                    redirect('admin/pg_icon/upload_video');
                }
                $video_name=time().'.mp4';
                $data = array(
                    'cat_id' => implode(',',$this->input->post("category")),
                    'vid_title' => $this->input->post("title"),
                    'vid_desc' => $this->input->post("desc"),
                    'vid_path' => '/uploads/pg/'.$video_name,
                    'img_path' => $img_path,
                    'tags' => $this->input->post("tags"),
                    'status' => $this->input->post("status"),
                    'created_on' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->pg_model->insert_video($data);
                rename(".".$this->input->post("video_path"), "./uploads/pg/".$video_name);
                $this->clear_session();
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('admin/pg_icon/videos_list');

            }
        } else {
            load_admin_view('pg/upload_video_form', $arrData);
        }
    }


    /*
      * display edit video form and save
      */
    public function edit_video($id)
    {
        $arrData=array();
        $arrData['video_details']=$this->pg_model->get_video_records($id);
        $arrData['categories']=$this->category_model->get_all_records();
        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("desc", "Description", "trim|required");
           // $this->form_validation->set_rules("category", "Category", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                load_admin_view('pg/edit_video_form',$arrData);
            } else {
                if ($this->input->post("video_path")) {
                    $video_name=time().'.mp4';
                    rename(".".$this->input->post("video_path"), "./uploads/pg/".$video_name);
                    $data['vid_path']='/uploads/pg/'.$video_name;
                }
                if ($_FILES['img_path']['name']) {
                    $ext = pathinfo($_FILES['img_path']['name'], PATHINFO_EXTENSION);
                    $allowed_ext=array('jpg','jpeg','png');
                    if(!in_array($ext,$allowed_ext))
                    {
                        $this->session->set_flashdata('err_msg', 'Selected File extension not allowed !');
                        redirect('admin/pg_icon/edit_video/'.$id);
                    }
                    $image_info = getimagesize($_FILES["img_path"]["tmp_name"]);
                    $image_width = $image_info[0];
                    $image_height = $image_info[1];
                    if($image_width != 262 && $image_height !=152){
                        $this->session->set_flashdata('err_msg', 'Uploaded file does not fit into the allowed dimensions !');
                        redirect('admin/pg_icon/edit_video/'.$id);
                    }
                    $new_file_name = "pg-image-" . time();

                    $config1['upload_path'] = './uploads/pg/images'; /* NB! create this dir! */
                    $config1['allowed_types'] = '*';/* Passing the extension to be upload */
                    $config1['file_name'] = $new_file_name;
                    $config1['max_width']  = '262';
                    $config1['max_height']  = '152';
                    //Loading library for uploading a file with configuration setting
                    $this->load->library('upload', $config1);
                    $this->upload->initialize($config1);

                    //Checking whether file is uploaded
                    if (!$this->upload->do_upload('img_path')) {
                        $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                        redirect('admin/pg_icon/edit_video/'.$id);
                    } else {
                        $img_data = $this->upload->data();
                        $img_path = "/uploads/pg/images/" . $img_data['file_name'];
                        $data['img_path'] = $img_path;
                    }
                }

                $data['cat_id']=implode(',',$this->input->post("category"));
                $data['vid_title']=$this->input->post("title");
                $data['vid_desc']=$this->input->post("desc");
                $data['tags']=$this->input->post("tags");
                $data['status']=$this->input->post("status");
                $data['modified_on']=date('Y-m-d');
                $data['modified_by']=$this->session->userdata('admin_id');

                $sid = $this->pg_model->update_video($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record updated Successfully!');
                redirect('admin/pg_icon/videos_list');
            }
        }else{
            load_admin_view('pg/edit_video_form',$arrData);
        }
    }

    /*
     * Delete video
     */
    public function delete_video($id)
    {
        $data = array(
            'is_deleted' =>1,
            'status' =>0
        );
        $sid = $this->pg_model->update_video($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/pg_icon/videos_list');
    }

    /*
    * Display all videos
    */
    public function videos_list()
    {
        $this->clear_session();
        $all_records = $this->pg_model->get_all_videos_admin();
        load_admin_view('pg/videos_list',array('arrData' => $all_records));
    }
    public function clear_session()
    {
        $this->session->unset_userdata('vid_title');
        $this->session->unset_userdata('vid_cat');
        $this->session->unset_userdata('vid_desc');
        $this->session->unset_userdata('tags');
        $this->session->unset_userdata('status');
    }

}