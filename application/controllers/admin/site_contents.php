<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/30/2015
 * Time: 10:39 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_contents extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
// check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('sitecontents_model');
    }
    public function term_of_use()
    {
        $arraData=array();
        $arraData['term_of_use'] = $this->sitecontents_model->get_term_of_use();
        load_admin_view('site_contents/term_list',$arraData);
    }
    /*
      * display edit term use form and save
      */
    public function edit_termofuse($id)
    {
        $arrData=array();
        $arrData['details']=$this->sitecontents_model->get_term_of_use();

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("cont_text", "Contents", "trim|required");


            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/site_contents/edit_termofuse/'.$id,$arrData);
            } else {
                $data = array(
                    'content_text' =>$this->input->post("cont_text"),
                    'modified_on' => date('Y-m-d'),
                    'modified_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->sitecontents_model->update_term_of_use($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                redirect('admin/site_contents/term_of_use');
            }
        }else{
            load_admin_view('site_contents/edit_term_of_use',$arrData);
        }
    }

    public function privacy_policy()
    {
        $arraData=array();
        $arraData['privacy_policy'] = $this->sitecontents_model->get_privacy_policy();
        load_admin_view('site_contents/privacy_policy_list',$arraData);
    }
    /*
      * display edit term use form and save
      */
    public function edit_privacypolicy($id)
    {
        $arrData=array();
        $arrData['details']=$this->sitecontents_model->get_privacy_policy();

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("cont_text", "Contents", "trim|required");


            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/site_contents/edit_privacypolicy/'.$id,$arrData);
            } else {
                $data = array(
                    'content_text' =>$this->input->post("cont_text"),
                    'modified_on' => date('Y-m-d'),
                    'modified_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->sitecontents_model->update_privacy_policy($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                redirect('admin/site_contents/privacy_policy');
            }
        }else{
            load_admin_view('site_contents/edit_privacy_policy',$arrData);
        }
    }
    public function disclaimer()
    {
        $arraData=array();
        $arraData['disclaimer'] = $this->sitecontents_model->get_disclaimer();
        load_admin_view('site_contents/disclaimer_list',$arraData);
    }
    /*
      * display edit term use form and save
      */
    public function edit_disclaimer($id)
    {
        $arrData=array();
        $arrData['details']=$this->sitecontents_model->get_disclaimer();

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("cont_text", "Contents", "trim|required");


            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/site_contents/edit_disclaimer/'.$id,$arrData);
            } else {
                $data = array(
                    'content_text' =>$this->input->post("cont_text"),
                    'modified_on' => date('Y-m-d'),
                    'modified_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->sitecontents_model->update_disclaimer($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                redirect('admin/site_contents/disclaimer');
            }
        }else{
            load_admin_view('site_contents/edit_disclaimer',$arrData);
        }
    }

}