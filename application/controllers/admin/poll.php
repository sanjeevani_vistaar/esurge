<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Poll extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
       $this->load->model('poll_model');
        //$this->output->enable_profiler(TRUE);
    }
     /*
      * display create admin form and save
      */
    public function create_poll()
    {
        $arrData=array();
        $arrData['categories'] = $this->poll_model->get_all_records();
       if (isset($_POST["submit"])) {

            //set validations
           $this->form_validation->set_rules("category", "Category", "trim|required");
            $this->form_validation->set_rules("poll_question", "Question", "trim|required");
            $this->form_validation->set_rules("opt_1", "Option 1", "trim|required");
            $this->form_validation->set_rules("opt_2", "Option 2", "trim|required");
            $this->form_validation->set_rules("opt_3", "Option 3", "trim|required");
            $this->form_validation->set_rules("opt_4", "Option 4", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/poll/create_poll');
                //load_admin_view('admin_users/create_admin_form',$arrData);
            } else {
                $data = array(
                    'cat_id' => $this->input->post("category"),
                    'poll_question' =>$this->input->post("poll_question"),
                    'option_1' =>$this->input->post("opt_1"),
                    'option_2' =>$this->input->post("opt_2"),
                    'option_3' =>$this->input->post("opt_3"),
                    'option_4' =>$this->input->post("opt_4"),
                    'status' =>$this->input->post("status"),
                );
                $sid = $this->poll_model->insert_poll($data);
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('admin/poll/all_list');
            }
        }else{
            load_admin_view('polls/create_poll_form',$arrData);
        }
    }

    /*
      * display create admin form and save
      */
    public function edit_poll($id)
    {
        $arrData=array();
        $arrData['poll_details']=$this->poll_model->get_poll($id);
        $arrData['categories'] = $this->poll_model->get_all_records();
        if (isset($_POST["submit"])) {
            //set validations
            $this->form_validation->set_rules("category", "Category", "trim|required");
            $this->form_validation->set_rules("poll_question", "Question", "trim|required");
            $this->form_validation->set_rules("opt_1", "Option 1", "trim|required");
            $this->form_validation->set_rules("opt_2", "Option 2", "trim|required");
            $this->form_validation->set_rules("opt_3", "Option 3", "trim|required");
            $this->form_validation->set_rules("opt_4", "Option 4", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/poll/create_poll');
                //load_admin_view('admin_users/create_admin_form',$arrData);
            } else {
                $data = array(
                    'cat_id' => $this->input->post("category"),
                    'poll_question' => $this->input->post("poll_question"),
                    'option_1' => $this->input->post("opt_1"),
                    'option_2' => $this->input->post("opt_2"),
                    'option_3' => $this->input->post("opt_3"),
                    'option_4' => $this->input->post("opt_4"),
                    'status' => $this->input->post("status"),
                );

                $sid = $this->poll_model->update_poll($id, $data);
                $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                redirect('admin/poll/all_list');
            }
        }
        else{
            load_admin_view('polls/edit_poll_form',$arrData);
        }
    }


    /*
     * Delete admin
     */
    public function delete_poll($id)
    {
        $data = array(
            'is_deleted' =>1,
            'status' =>0
        );
        $sid = $this->poll_model->update_poll($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/poll/all_list');
    }

    /*
     * Display all Admin users
     */
    public function all_list()
    {
        $all_records = $this->poll_model->get_all_poll_admin();
        load_admin_view('polls/poll_list',array('arrData' => $all_records));
    }
// tracking report
    public function tracking($id)
    {
        $arrData=array();
        //$arrData['tracking_details']=$this->poll_model->get_poll_tracking($id);
        $arrData['option_details']=$this->poll_model->get_poll($id);
        $this->load->view('admin/polls/tracking', $arrData);
    }
    /*
       * Display all categories
       */
    public function categories_list()
    {
        $all_records = $this->poll_model->get_all_records();
        load_admin_view('polls/categories_list',array('arrData' => $all_records));
    }
    /*
      * display create category form and save
      */
    public function create_category()
    {

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("name", "Category name", "trim|required|callback_category_check");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/poll/create_category');
            } else {
                $data = array(
                    'cat_name' =>strtoupper($this->input->post("name")),
                    'created_on' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->poll_model->insert_category($data);
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('admin/poll/categories_list');
            }
        }else{
            load_admin_view('polls/create_category_form');
        }
    }

    /*
      * display edit category form and save
      */
    public function edit_category($id)
    {
        $arrData=array();
        $arrData['cat_details']=$this->poll_model->get_cat_records($id);

        if (isset($_POST["submit"])) {

            //set validations
            if($arrData['cat_details'][0]->cat_name == strtoupper($this->input->post("name")))
            {
                $this->form_validation->set_rules("name", "Category name", "trim|required");
            }else{
                $this->form_validation->set_rules("name", "Category name", "trim|required|callback_category_check");
            }
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/poll/edit_category/'.$id,$arrData);
            } else {
                $data = array(
                    'cat_name' =>strtoupper($this->input->post("name")),
                    'modified_on' => date('Y-m-d'),
                    'modified_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->poll_model->update_category($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                redirect('admin/poll/categories_list');
            }
        }else{
            load_admin_view('polls/edit_category_form',$arrData);
        }
    }

    /*
     * Delete category
     */
    public function delete_category($id)
    {
        $data = array(
            'is_deleted' =>1
        );
        $sid = $this->poll_model->update_category($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/poll/categories_list');
    }

    //check category already exsists or not
    public function category_check($cat)
    {
        $already_exsist=$this->poll_model->check_exsisting_polls_category(strtoupper($cat));
        if (!empty($already_exsist))
        {
            $this->form_validation->set_message('category_check', 'Category Already Added.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    // cat tracking report
    public function cat_tracking($catid)
    {
        $arrData=array();
        $arrData['tracking_count']=$this->poll_model->get_cat_tracking($catid);
        $this->load->view('admin/polls/cat_tracking', $arrData);
    }
}