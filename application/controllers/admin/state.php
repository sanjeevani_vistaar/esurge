<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class State extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('state_model');
        //$this->output->enable_profiler(TRUE);
    }
     /*
      * display create state form and save
      */
    public function create_state()
    {

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("name", "State name", "trim|required");


            if ($this->form_validation->run() == FALSE) {
                //validation fails
                load_admin_view('state/create_state_form');
            } else {
                $data = array(
                    'state_name' =>$this->input->post("name"),
                    'created_on' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->state_model->insert_state($data);
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('admin/state/all_list');
            }
        }else{
            load_admin_view('state/create_state_form');
        }
    }

    /*
      * display edit state form and save
      */
    public function edit_state($id)
    {
        $arrData=array();
        $arrData['state_details']=$this->state_model->get_state_records($id);

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("name", "State name", "trim|required");


            if ($this->form_validation->run() == FALSE) {
                //validation fails
                load_admin_view('state/edit_state_form',$arrData);
            } else {
                $data = array(
                    'state_name' =>$this->input->post("name"),
                    'modified_on' => date('Y-m-d'),
                    'modified_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->state_model->update_state($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                redirect('admin/state/all_list');
            }
        }else{
            load_admin_view('state/edit_state_form',$arrData);
        }
    }

    /*
     * Delete state
     */
    public function delete_state($id)
    {
        $data = array(
            'is_deleted' =>1
        );
        $sid = $this->state_model->update_state($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/state/all_list');
    }

    /*
     * Display all states
     */
    public function all_list()
    {
        $all_records = $this->state_model->get_all_records();
        load_admin_view('state/state_list',array('arrData' => $all_records));
    }

}