<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/28/2015
 * Time: 9:49 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        } elseif ($this->session->userdata('admintype') == 2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('banner_model');
        $this->load->model('journals_model');
        $this->load->model('news_model');
        $this->load->model('images_model');
        $this->load->model('videos_model');
        //$this->output->enable_profiler(TRUE);
    }

    /*
     * display all records
     */
    public function index()
    {
        $all_records = $this->banner_model->get_all_banners_admin();
        load_admin_view('banner/banner_list',array('arrData' => $all_records));
    }
    /*
     * Display all journals
     */
    public function source_links()
    {
        $all_records = $this->banner_model->get_all_banners_admin();
        if(count($all_records) < CONTENTS_SCROLLER_LIMIT) {
            load_admin_view('banner/create_banner_form');
        }else{
            $this->session->set_flashdata('err_msg', 'Remove one banner from banner list to add new');
            redirect('admin/banner');
        }
    }
    /*
     * Display all journals
     */
    public function journals_list_banner()
    {
        $all_records = $this->journals_model->get_all_journals_banner();
        $this->load->view('admin/banner/journals_list_banner',array('arrData' => $all_records));
    }
    /*
     * Display all news
     */
    public function news_list_banner()
    {
        $all_records = $this->news_model->get_all_news_banner();
        $this->load->view('admin/banner/news_list_banner',array('arrData' => $all_records));
    }
    /*
    * Display all images
    */
    public function images_list_banner()
    {
        $all_records = $this->images_model->get_all_images_banner();
        $this->load->view('admin/banner/images_list_banner',array('arrData' => $all_records));
    }
    /*
    * Display all videos
    */
    public function videos_list_banner()
    {
        $all_records = $this->videos_model->get_all_videos_banner();
        $this->load->view('admin/banner/videos_list_banner',array('arrData' => $all_records));
    }

    /*
     * Add banner
     */
    public function add_banner()
    {
        $all_records = $this->banner_model->get_all_banners_admin();
        if(count($all_records) >= CONTENTS_SCROLLER_LIMIT) {
            $this->session->set_flashdata('err_msg', 'Remove one banner from banner list to add new');
            redirect('admin/banner');
        }else{
            $cnt=count($all_records);
        }
        $id=$_POST['id'];
        $seleted_item = count($id);
        $remaining_addition=CONTENTS_SCROLLER_LIMIT - $cnt;
        if($seleted_item > $remaining_addition)
        {
            $this->session->set_flashdata('err_msg', 'Please select only '.$remaining_addition. ' Records to Add');
            redirect('admin/banner');
        }
        $banner_from=$_POST['banner_from'];
        for($i=0;$i<count($id);$i++)
        {
            $arrData['main_id']= $id[$i];
            $arrData['banner_from']= $banner_from;
            $arrData['created_on']= date('Y-m-d');
            $arrData['created_by']= $this->session->userdata('admin_id');
            $sid = $this->banner_model->insert_record($arrData);
            if($sid) {
                if($banner_from=='journals') {
                    $arrData1['is_banner'] = 1;
                    $uid = $this->journals_model->update_journal($id[$i], $arrData1);
                }elseif($banner_from=='news') {
                    $arrData1['is_banner'] = 1;
                    $uid = $this->news_model->update_news($id[$i], $arrData1);
                }elseif($banner_from=='images') {
                    $arrData1['is_banner'] = 1;
                    $uid = $this->images_model->update_image($id[$i], $arrData1);
                }elseif($banner_from=='videos') {
                    $arrData1['is_banner'] = 1;
                    $uid = $this->videos_model->update_video($id[$i], $arrData1);
                }
            }
        }
        $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
        redirect('admin/banner');
    }
    /*
     * Delete banner
     */
    public function delete_banner($id)
    {
        $data = array(
            'is_deleted' =>1,
            'status' =>0,
            'modified_on' =>date('Y-m-d'),
            'modified_by' => $this->session->userdata('admin_id')
        );
        $sid = $this->banner_model->update_record($id,$data);
        $status=$this->banner_model->get_banner_details($id);
        $data2['is_banner']=0;
        if($status->banner_from=='journals') {
            $uid = $this->journals_model->update_journal($status->main_id, $data2);
        }
        if($status->banner_from=='news') {
        $uid = $this->news_model->update_news($status->main_id, $data2);
        }
        if($status->banner_from=='images') {
            $uid = $this->images_model->update_image($status->main_id, $data2);
        }
        if($status->banner_from=='videos') {
            $uid = $this->videos_model->update_video($status->main_id, $data2);
        }

        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/banner');
    }
    /*
     * Change status
     */
    public function change_status($id)
    {
        $status=$this->banner_model->get_banner_details($id);
        $old_status=$status->status;
        if($old_status){
            $new_status=0;
        }else{
            $new_status=1;
        }
        $data = array(
            'status' =>$new_status,
            'modified_on' =>date('Y-m-d'),
            'modified_by' => $this->session->userdata('admin_id')
        );

        $sid = $this->banner_model->update_record($id,$data);

        $this->session->set_flashdata('succ_msg', 'Status Changed Successfully!');
        redirect('admin/banner');
    }
    /*
     * display journal details
     */
    public function journal_details($id)
    {
        $arrData['journal_details']=$this->journals_model->get_journal_records($id);
        $this->load->view('admin/banner/details',$arrData);
    }
    /*
     * display news details
     */
    public function news_details($id)
    {
        $arrData['news_details']=$this->news_model->get_news_records($id);
        $this->load->view('admin/banner/details',$arrData);
    }
    /*
     * display images details
     */
    public function image_details($id)
    {
        $arrData['image_details']=$this->images_model->get_image_records($id);
        $this->load->view('admin/banner/details',$arrData);
    }
    /*
     * display video details
     */
    public function video_details($id)
    {
        $arrData['video_details']=$this->videos_model->get_video_records($id);
        $this->load->view('admin/banner/details',$arrData);
    }
}