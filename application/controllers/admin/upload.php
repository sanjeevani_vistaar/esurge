<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 11:07 AM
 */
class Upload extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('user_model');
        $this->load->model('registrationcode_model');
        $this->load->model('city_model');
        //$this->output->enable_profiler(TRUE);
    }

    public function index()
    {
        load_admin_view('upload_file');
    }

    public function upload_file()
    {
        ini_set('max_execution_time', 0);
        if ($_FILES['file_path']['name']) {

            $config['upload_path'] = './uploads/tmp/'; /* NB! create this dir! */
            $config['allowed_types'] = '*';/* Passing the extension to be upload */
            //Loading library for uploading a file with configuration setting
            $this->load->library('upload', $config);


            //Checking whether file is uploaded
            if (!$this->upload->do_upload('file_path')) {
                echo "<pre>";
                print_r($this->upload->display_errors());

            } else {
                $arrData['data'] = $this->upload->data();
                /** Include PHPExcel */
                require_once __DIR__ . '/../../libraries/excelreader/PHPExcel/IOFactory.php';
                require_once __DIR__ . '/../../libraries/excelreader/PHPExcel.php';

                $inputFileName = $arrData['data']['full_path'];

                //  Read your Excel workbook

                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);

                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow();
                $highestColumn = $objWorksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $rows = array();
                $counter = 0;
                for ($row = 2; $row <= $highestRow; ++$row) {
                    $first_name = $objWorksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $last_name = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $email = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $mobile = $objWorksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $registration_no = $objWorksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $usi_no = $objWorksheet->getCellByColumnAndRow(7, $row)->getValue();
                    $city_name = $objWorksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $usertype = $objWorksheet->getCellByColumnAndRow(5, $row)->getValue();

                    $city_details=$this->city_model->get_city_records_byname(ucwords($city_name));


                    if(empty($city_details)){
                        $city_id=99999;
                        $state_id=9999;
                    }else{
                        $city_id=$city_details[0]->city_id;
                        $state_id=$city_details[0]->state_id;
                    }
                    if($usertype=='Consultant Urologist'){
                        $user_type='Consultant';
                    }elseif($usertype=='Post Graduate Student'){
                        $user_type='Post graduates';
                    }

                    /* add registration code while registration :: start Remove in next version launch*/
                    $data11 = array(
                        'full_name' =>$first_name." ".$last_name,
                        'email_id' =>$email,
                        'mobile_no' =>$mobile,
                        'registration_code'=>REGISTRATION_CODE_PREFIX,
                        'status' =>1,
                        'created_on' => date('Y-m-d'),
                        'created_by' => 1
                    );
                    $ssid = $this->registrationcode_model->insert_registration_code($data11);
                    if($ssid){
                        $login_code=REGISTRATION_CODE_PREFIX.$ssid;
                        $data12 = array(
                            'registration_code'=>REGISTRATION_CODE_PREFIX.$ssid
                        );
                        $this->registrationcode_model->update_registration_code($ssid,$data12);
                    }
                    /* add registration code while registration :: End */
                    /* add to user table::Start*/
                    $data2 = array(
                        'first_name' =>$first_name,
                        'last_name' =>$last_name,
                        'email_id' =>$email,
                        'mobile_no' =>$mobile,
                        'city_id' =>$city_id,
                        'state_id' =>$state_id,
                        'registration_no' =>$registration_no,
                        'usi_no' =>$usi_no,
                        //'registration_code' =>$this->input->post("registration_code"),
                        'registration_code' =>$login_code,
                        'dob' =>'',
                        'password' =>md5(sha1('esurgesun@123')),
                        'token' =>0,
                        'user_type' =>$user_type,
                        'status' =>1,
                        'created_on' => date('Y-m-d')
                    );
                    $sid = $this->user_model->insert_user($data2);
                    /* add to user table::Start*/
                }


            }
        }
    }

}
