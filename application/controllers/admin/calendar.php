<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/21/2015
 * Time: 5:16 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        } elseif ($this->session->userdata('admintype') == 2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('events_model');
        //$this->output->enable_profiler(TRUE);
    }

    /*
     * display calendar view
     */
    public function index()
    {
        load_admin_view('events/calendar');
    }

    /*
     * display all events
     */
    public function all_events()
    {
       $arrData=array();
       $arrData['all_events']=$this->events_model->get_all_records();
        echo json_encode($arrData['all_events']);
    }

    /*
     * Add event
     */
    public function add_event()
    {
     $arrData['start']=$_POST['start'];
     $arrData['end']=$_POST['end'];
        if (isset($_POST["submit"])) {
                $data = array(
                    'title' => $this->input->post("title"),
                    'view' => $this->input->post("desc"),
                    'location' => $this->input->post("location"),
                    'link' => $this->input->post("link"),
                    'start' => $this->input->post("start"),
                    'end' => $this->input->post("end"),
                    'created_on' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->events_model->insert_record($data);
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('admin/calendar');

        }else {
            $this->load->view('admin/events/add_event', $arrData);
        }
    }

    /*
     * display event description
     */
    public function event_desc()
    {
        $event_id=$_POST['event_id'];
        $arrData['event_details']=$this->events_model->get_event_records($event_id);
        $this->load->view('admin/events/event_desc', $arrData);
    }

    /*
     * Update event
     */
    public function update_event($id)
    {
        if (isset($_POST["submit"])) {
            $data = array(
                'title' => $this->input->post("title"),
                'view' => $this->input->post("desc"),
                'location' => $this->input->post("location"),
                'link' => $this->input->post("link"),
                'modified_on' => date('Y-m-d'),
                'modified_by' => $this->session->userdata('admin_id')
            );
            $sid = $this->events_model->update_record($id,$data);
            $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
            redirect('admin/calendar');

        }else {
            $arrData['event_details']=$this->events_model->get_event_records($id);
            $this->load->view('admin/events/update_event', $arrData);
        }
    }

    /*
     * Delete event
     */
    public function delete_event($id)
    {
        $data = array(
            'is_deleted' =>1
        );
        $sid = $this->events_model->update_record($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/calendar');
    }
}