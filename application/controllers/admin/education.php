<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Education extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('education_model');
        //$this->output->enable_profiler(TRUE);
    }
     /*
      * display upload file form and save
      */
    public function upload_file()
    {

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("desc", "Description", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                //validation fails
                load_admin_view('education/upload_file_form');
            } else {
                if ($_FILES['file_path']['name']) {

                    $new_file_name = "edu-" . time();

                    $config['upload_path'] = './uploads/educations/'; /* NB! create this dir! */
                    $config['allowed_types'] = '*';/* Passing the extension to be upload */
                    $config['file_name'] = $new_file_name;
                    //Loading library for uploading a file with configuration setting
                    $this->load->library('upload', $config);


                    //Checking whether file is uploaded
                    if (!$this->upload->do_upload('file_path')) {
                        $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                        load_admin_view('education/upload_file_form');
                    } else {
                        $file_data = $this->upload->data();
                        $file_path = "/uploads/educations/" . $file_data['file_name'];
                    }
                }
                    $data = array(
                        'edu_title' => $this->input->post("title"),
                        'edu_desc' => $this->input->post("desc"),
                        'tags' => $this->input->post("tags"),
                        'file_path' => $file_path,
                        'status' => $this->input->post("status"),
                        'created_on' => date('Y-m-d'),
                        'created_by' => $this->session->userdata('admin_id')
                    );
                    $sid = $this->education_model->insert_record($data);
                    $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                    redirect('admin/education/all_list');

            }
        }else{
            load_admin_view('education/upload_file_form');
        }
    }

    /*
      * display edit file form and save
      */
    public function edit_file($id)
    {
        $arrData=array();
        $arrData['file_details']=$this->education_model->get_records($id);

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("desc", "Description", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                //validation fails
                load_admin_view('education/edit_file_form');
            } else {
                if ($_FILES['file_path']['name']) {

                    $new_file_name = "news-" . time();

                    $config['upload_path'] = './uploads/educations/'; /* NB! create this dir! */
                    $config['allowed_types'] = '*';/* Passing the extension to be upload */
                    $config['file_name'] = $new_file_name;
                    //Loading library for uploading a file with configuration setting
                    $this->load->library('upload', $config);


                    //Checking whether file is uploaded
                    if (!$this->upload->do_upload('file_path')) {
                        $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                        load_admin_view('education/edit_file_form');
                    } else {
                        $file_data = $this->upload->data();
                        $file_path = "/uploads/education/" . $file_data['file_name'];
                        $data['file_path']=$file_path;
                    }
                }

                $data['edu_title']=$this->input->post("title");
                $data['edu_desc']=$this->input->post("desc");
                $data['tags']=$this->input->post("tags");
                $data['status']=$this->input->post("status");
                $data['modified_on']=date('Y-m-d');
                $data['modified_by']=$this->session->userdata('admin_id');

                $sid = $this->education_model->update_record($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record updated Successfully!');
                redirect('admin/education/all_list');
            }
        }else{
            load_admin_view('education/edit_file_form',$arrData);
        }
    }

    /*
     * Delete file
     */
    public function delete_file($id)
    {
        $data = array(
            'is_deleted' =>1,
            'status' =>0
        );
        $sid = $this->education_model->update_record($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/education/all_list');
    }

    /*
     * Display all files
     */
    public function all_list()
    {
        $all_records = $this->education_model->get_all_records();
        load_admin_view('education/files_list',array('arrData' => $all_records));
    }


}