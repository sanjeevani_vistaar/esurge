<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 11:07 AM
 */
class Policy extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('sitecontents_model');
        //$this->output->enable_profiler(TRUE);
    }

    /*
     * display policy page
     */
    public function index()
    {
        $arraData['privacy_policy'] = $this->sitecontents_model->get_privacy_policy();
        load_front_view('policy',$arraData);
    }
}
