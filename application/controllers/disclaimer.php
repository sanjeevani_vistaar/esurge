<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 11:07 AM
 */
class Disclaimer extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('sitecontents_model');
        //$this->output->enable_profiler(TRUE);
    }

    /*
     * display disclaimer page
     */
    public function index()
    {
        $arraData['disclaimer'] = $this->sitecontents_model->get_disclaimer();
        $arraData['term_of_use'] = $this->sitecontents_model->get_term_of_use();
        load_front_view('disclaimer',$arraData);
    }
}
