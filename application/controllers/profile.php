<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/21/2015
 * Time: 3:48 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_id')){
            redirect('home');
        }
        $this->load->database();
        $this->load->model('user_model');
        $this->load->model('state_model');
        $this->load->model('city_model');
        $this->load->model('registrationcode_model');
        //$this->output->enable_profiler(TRUE);
    }


    public function update_profile()
    {
        $this->clear_session();
        $arrData=array();
        $arrData['states']=$this->state_model->get_all_records();
        $arrData['city']=$this->city_model->get_all_records();
        $arrData['user_details']=$this->user_model->get_user_records($this->session->userdata('user_id'));

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("first_name", "First name", "trim|required");
           $this->form_validation->set_rules("last_name", "Last name", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required|callback_email_check");
            $this->form_validation->set_rules("mobile", "Mobile", "trim|required|callback_number_check");
            $this->form_validation->set_rules("city", "City", "trim|required");
            $this->form_validation->set_rules("state", "State", "trim|required");
            $this->form_validation->set_rules("registration_no", "Registration Number", "trim|required");
            $this->form_validation->set_rules("user_type", "Type", "trim|required");
            $sessiondata = array(
                            'ufirst_name' => $this->input->post("first_name"),
                            'ulast_name' => $this->input->post("last_name"),
                            'uemail' => $this->input->post("email"),
                            'umobile' => $this->input->post("mobile"),
                            'udob' => $this->input->post("dob"),
                            'uusi_no' =>$this->input->post("usi_no"),
                            'city_id' => $this->input->post("city"),
                            'ustate' => $this->input->post("state"),
                            'uregistration_no' => $this->input->post("registration_no"),
                            'uuser_type' => $this->input->post("user_type")                            
                        );
                        $this->session->set_userdata($sessiondata);
            if ($this->form_validation->run() == FALSE) {
                //$this->session->set_flashdata('err_msg', validation_errors());
                load_front_view('update_profile', $arrData);
            } else {
                       if($this->input->post("dob")==''){
                           $dob="";
                           }else{
                               $dob=date('Y-m-d',strtotime($this->input->post("dob")));
                            }
                    $data = array(
                        'first_name' => $this->input->post("first_name"),
                        'last_name' => $this->input->post("last_name"),
                        'email_id' => $this->input->post("email"),
                        'mobile_no' => $this->input->post("mobile"),
                        'city_id' => $this->input->post("city"),
                        'state_id' => $this->input->post("state"),
                        'registration_no' => $this->input->post("registration_no"),
                        'usi_no' => $this->input->post("usi_no"),
                        'dob' => $dob,
                        'user_type' => $this->input->post("user_type"),

                    );
                    $sid = $this->user_model->update_user($this->session->userdata('user_id'),$data);
                    //Unset session Variable
                    $this->session->unset_userdata('ufirst_name');
                    $this->session->unset_userdata('ulast_name');
                    $this->session->unset_userdata('uemail');
                    $this->session->unset_userdata('umobile');
                    $this->session->unset_userdata('ustate');
                    $this->session->unset_userdata('city_id');
                    $this->session->unset_userdata('uregistration_no');
                    $this->session->unset_userdata('uusi_no');
                    $this->session->unset_userdata('udob');
                    $this->session->unset_userdata('uuser_type');
                    // end
                    $registration_code=$arrData['user_details'][0]->registration_code;
                    $code_data = array(
                        'email_id' => $this->input->post("email"),
                        'mobile_no' => $this->input->post("mobile"),
                    );
                    $cid = $this->registrationcode_model->update_user_details($registration_code,$code_data);

                    $this->session->set_flashdata('succ_msg', 'Records Changed Successfully');
                    redirect('profile/update_profile');
            }
        }else {
            load_front_view('update_profile', $arrData);
        }
    }
    //check emai id already exsists or not
    public function email_check($email)
    {
        $old_email = $this->user_model->get_user_records($this->session->userdata('user_id'));
        if ($old_email[0]->email_id != $email) {
        $already_exsist = $this->user_model->check_exsisting_email($email);
        }
        if (!empty($already_exsist))
        {
            $this->form_validation->set_message('email_check', 'Email Id Already Registred.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
//check mobile number
    public function number_check($no)
    {
        if(!preg_match('/^\d{10}$/', $no))
        {
            $this->form_validation->set_message('number_check', 'Please Enter Valid Mobile Number.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    public function update_password()
    {
         if (isset($_POST["submit"])) {

            //set validations
             $this->form_validation->set_rules("password", "Password", "trim|required|matches[conf-password]");
             $this->form_validation->set_rules("conf-password", "Confirm Password", "trim|required");


             if ($this->form_validation->run() == FALSE) {
               // $this->session->set_flashdata('err_msg', validation_errors());
                load_front_view('update_password');
            } else {

                $data = array(
                    'password' =>md5(sha1($this->input->post("password"))),
                    'token' =>'Done'

                );
                $sid = $this->user_model->update_user($this->session->userdata('user_id'),$data);
                $this->session->set_flashdata('succ_msg', 'Password Changed Successfully');
                redirect('profile/update_password');
            }
        }else {
            load_front_view('update_password');
        }
    }
    /*
    * Logout function
    */
    function logout()
    {
        $this->session->sess_destroy();
        redirect('home');
    }

public function clear_session()
{
                    $this->session->unset_userdata('ufirst_name');
                    $this->session->unset_userdata('ulast_name');
                    $this->session->unset_userdata('uemail');
                    $this->session->unset_userdata('umobile');
                    $this->session->unset_userdata('ustate');
                    $this->session->unset_userdata('city_id');
                    $this->session->unset_userdata('uregistration_no');
                    $this->session->unset_userdata('uusi_no');
                    $this->session->unset_userdata('udob');
                    $this->session->unset_userdata('uuser_type');
}

}