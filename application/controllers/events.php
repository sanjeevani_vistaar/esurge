<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/31/2015
 * Time: 4:04 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('events_model');
        //$this->output->enable_profiler(TRUE);
        if ($this->session->userdata('user_id')) {
            $access_by = access_by('events');
            $user_access=$this->session->userdata('user_type');
            $userArr=explode(',',$access_by);
            if (!in_array($user_access,$userArr)) {
                $this->session->set_flashdata('err_msg', 'Sorry!! You do not have access to Events Section.Please contact administrator.');
                redirect('home');
            }
        }
    }
    public function index()
    {
        $year = date("Y");
        $month = date("F");
        redirect('events/events_list/'.$year.'/'.$month);
    }
    /*
     * display calendar view
     */
    public function events_list($year=NULL,$month=NULL)
    {

        if(!$year){
            $year = date("Y");
        }
        if(!$month){
            $month1 = date("m");
            $month=date("F");
        }else{
            $month1 = date("m", strtotime($month));
        }

        $arrData=array();
        $record_per_page = RECORD_PER_PAGE;
        $total_records = $this->events_model->get_all_records_year_month($year,$month1);
        $total_records=count($total_records);
        $arrData['ui_pagging'] = zebra_paggination($total_records,$record_per_page);
        $current_page = $this->zebra_pagination->get_page();
        $current_page = $current_page -1;
        $start_index = $current_page * $record_per_page;

        $arrData['all_events']=$this->events_model->get_all_records_year_month_page($year,$month1,$start_index,$record_per_page);
        $arrData['selected_year']=$year;
        $arrData['selected_month']=$month;
        load_front_view('events',$arrData);
    }

    /*
    * display calendar view
    */
    public function events_list_filter($year=NULL,$month=NULL)
    {

        if(!$year){
            $year = date("Y");
        }
        if(!$month){
            $month1 = date("m");
            $month=date("F");
        }else{
            $month1 = date("m", strtotime($month));
        }

        $arrData=array();
        $record_per_page = RECORD_PER_PAGE;
        $total_records = $this->events_model->get_all_records_year_month_filter($year,$month1);
        $total_records=count($total_records);
        $arrData['ui_pagging'] = zebra_paggination($total_records,$record_per_page);
        $current_page = $this->zebra_pagination->get_page();
        $current_page = $current_page -1;
        $start_index = $current_page * $record_per_page;

        $arrData['all_events']=$this->events_model->get_all_records_year_month_page_filter($year,$month1,$start_index,$record_per_page);
        $arrData['selected_year']=$year;
        $arrData['selected_month']=$month;
        load_front_view('events',$arrData);
    }

    /*
     * display all events
     */
    public function all_events()
    {
        $arrData=array();
        $arrData['all_events']=$this->events_model->get_all_records();
        echo json_encode($arrData['all_events']);
    }
    /*
     * display event description
     */
    public function event_desc()
    {
        $event_id=$_POST['event_id'];
        $arrData['event_details']=$this->events_model->get_event_records($event_id);
        $this->load->view('front/event_desc', $arrData);
    }
/*
    * display all events
    */
    public function filter_events()
    {
        $arrData=array();
        $arrData['selected_year']=$_POST['year'];
        $arrData['selected_month']=$_POST['month'];
        $arrData['all_events']=$this->events_model->get_all_records();
        $this->load->view('front/ajax_events',$arrData);
    }

}