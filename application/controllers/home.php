<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/16/2015
 * Time: 3:56 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('news_model');
        $this->load->model('events_model');
        $this->load->model('banner_model');
        $this->load->model('videos_model');

        //$this->output->enable_profiler(TRUE);
    }
    /*
     * display home contents
     */
    public function index()
    {
        $arrData=array();
        $arrData['home_news']=$this->news_model->get_home_news();
        $arrData['home_videos']=$this->videos_model->get_home_videos();
        $arrData['content_scroller']=$this->banner_model->get_all_banners();
        load_front_view('home',$arrData);
    }
}