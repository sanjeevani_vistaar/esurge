<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 10:03 AM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Puzzle extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('puzzle_model');
       //$this->output->enable_profiler(TRUE);
        if ($this->session->userdata('user_id')) {
            $access_by = access_by('puzzle');
            $user_access=$this->session->userdata('user_type');
            $userArr=explode(',',$access_by);
            if (!in_array($user_access,$userArr)) {
                $this->session->set_flashdata('err_msg', 'Sorry!! You do not have access to News Section.Please contact administrator.');
                redirect('home');
            }
        }
    }
    public function index()
    {
        redirect('puzzle/category');
    }
    /*
     * display category list
     */
    public function category()
    {
        $arrData['puzzle_cat'] = $this->puzzle_model->get_all_cat();
        load_front_view('puzzle_cat',$arrData);
    }
    /*
     * display list
     */
    public function question($catid,$no=0)
    {
        $arrData['puzzle'] = $this->puzzle_model->get_all_puzzles($catid,$no);
        $arrData['cat_details'] = $this->puzzle_model->get_cat_records($catid);
        $all_puzzles = $this->puzzle_model->count_all_puzzles($catid);
        $arrData['no'] = $no;
        $arrData['total_no'] = $all_puzzles;
        //load_front_view('puzzle',$arrData);
        $this->load->view('front/puzzle',$arrData);
    }

    public function get_correct_answer()
    {
        $id=$this->input->post('id');
        if ($this->session->userdata('user_id')){
            $user=$this->session->userdata('user_id');
        }else{
            $user='guest';
        }
        $data = array(
            'puzzle_id' => $id,
            'user_id' => $user,
            'played_date' => date('Y-m-d')
        );

        $sid = $this->puzzle_model->insert_puzzle_report($data);
        $correct_answer=$this->puzzle_model->get_answer($id);
        echo $correct_answer;
    }
}