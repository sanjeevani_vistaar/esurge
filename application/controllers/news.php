<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 10:03 AM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('news_model');
        $this->load->model('events_model');
        $this->load->model('banner_model');
        //$this->output->enable_profiler(TRUE);
        if ($this->session->userdata('user_id')) {
            $access_by = access_by('news');
            $user_access=$this->session->userdata('user_type');
            $userArr=explode(',',$access_by);
            if (!in_array($user_access,$userArr)) {
                $this->session->set_flashdata('err_msg', 'Sorry!! You do not have access to News Section.Please contact administrator.');
                redirect('home');
            }
        }
    }

    public function index()
    {
        redirect('news/news_list');
    }
    /*
     * display list
     */
    public function news_list()
    {
        $arrData=array();
        $record_per_page = RECORD_PER_PAGE;
        $total_records = $this->news_model->get_all_news();
        $total_records=count($total_records);
        $arrData['ui_pagging'] = zebra_paggination($total_records,$record_per_page);
        $current_page = $this->zebra_pagination->get_page();
        $current_page = $current_page -1;
        $start_index = $current_page * $record_per_page;
        $arrData['all_news'] = $this->news_model->get_all_news_page($start_index, $record_per_page);
        load_front_view('news_list',$arrData);
    }

    /*
     * display details
     */
    public function news_details($id)
    {
        $login_required = is_login_required($this->router->fetch_class());
        if($login_required) {
            if (!$this->session->userdata('user_id')) {
                $this->session->set_flashdata('err_msg', 'Sorry !!! Please Login to access this page.');
                redirect('home');
            }
        }
        $arrData=array();
        $arrData['home_news']=$this->news_model->get_others_news();
        $arrData['news_details']=$this->news_model->get_news_records($id);
        $arrData['current_news']=$id;
        if ($this->session->userdata('user_id')) {
            user_tracking('news',$arrData['news_details'][0]->news_title);
        }
        load_front_view('news_details',$arrData);
    }
}