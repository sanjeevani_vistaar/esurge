<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/30/2015
 * Time: 10:39 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Journals extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('journals_model');
        //$this->output->enable_profiler(TRUE);
        if ($this->session->userdata('user_id')) {
            $access_by = access_by('journals');
            $user_access=$this->session->userdata('user_type');
            $userArr=explode(',',$access_by);
            if (!in_array($user_access,$userArr)) {
                $this->session->set_flashdata('err_msg', 'Sorry!! You do not have access to Journals Section.Please contact administrator.');
                redirect('home');
            }
        }
    }
    public function index()
    {
        redirect('journals/journals_list');
    }
    /*
     * display create admin form and save
     */
    public function journals_list()
    {
        $arrData=array();
        $record_per_page = RECORD_PER_PAGE;
        $total_records = $this->journals_model->get_all_journals();
        $total_records=count($total_records);
        $arrData['ui_pagging'] = zebra_paggination($total_records,$record_per_page);
        $current_page = $this->zebra_pagination->get_page();
        $current_page = $current_page -1;
        $start_index = $current_page * $record_per_page;
        $arrData['all_journals']=$this->journals_model->get_all_journals_page($start_index,$record_per_page);

        load_front_view('journals',$arrData);
    }

    function scrrefcustomerror($errornumber, $errormessage, $errorfile, $errorrow)
    {
        echo '<meta http-equiv="refresh" content="0;url=http://www.error.com?err=Error code '.$errornumber.' - '.$errormessage.'">';
    }

/*
     * redirect on landing page
     */
    public function wiley_page($id)
    {
        $login_required = is_login_required($this->router->fetch_class());
        if($login_required) {
            if (!$this->session->userdata('user_id')) {
                $this->session->set_flashdata('err_msg', 'Sorry !!! Please Login to access this page.');
                redirect('home');
            }
        }

        $details=$this->journals_model->get_journal_records($id);
        $url=$details[0]->journal_link;

        user_tracking('journals',$details[0]->journal_title);

        ?>
        <html>
                <head>
                    <meta http-equiv='Content-Language' content='en-us'>
                    <meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>
                    <?php
                    $ticketurl = 'http://onlinelibrary.wiley.com/login-proxy-tps?targetURL='.$url.'&domain=http://esurge.in/';

                    if (($fp = fopen($ticketurl, 'r')))
                    {
                        $content = fread($fp, 1000000);
                        echo '<meta http-equiv="refresh" content="0;url='.$content.'">';
                    }
                    fclose($fp);

                    ?>
                </head>
        </html>
     <?php
    }

}