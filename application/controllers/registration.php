<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 4:12 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('user_model');
        $this->load->model('state_model');
        $this->load->model('city_model');
        $this->load->model('registrationcode_model');
        //$this->output->enable_profiler(TRUE);
    }
    /*
     * display create admin form and save
     */
    public function index()
    {
        //$this->clear_session();
        if($this->session->userdata('user_id')){
            redirect('home');
        }
        $arrData=array();
        $arrData['states']=$this->state_model->get_all_records();
        $arrData['city']=$this->city_model->get_all_records();

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("first_name", "First Name", "trim|required");
            $this->form_validation->set_rules("last_name", "Last Name", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required|callback_email_check");
            $this->form_validation->set_rules("mobile", "Mobile", "trim|required|callback_number_check");
            $this->form_validation->set_rules("city", "City", "trim|required");
            $this->form_validation->set_rules("state", "State", "trim|required");
            $this->form_validation->set_rules("registration_no", "Registration Number", "trim|required");
            //$this->form_validation->set_rules("registration_code", "Login Code", "trim|required");
            $this->form_validation->set_rules("password", "Password", "trim|required|matches[conf-password]");
            $this->form_validation->set_rules("conf-password", "Confirm Password", "trim|required");
            $this->form_validation->set_rules("user_type", "Type", "trim|required");
            $this->form_validation->set_rules("agree_chk", "Agree to Term of use & Privacy Policy", "trim|required");
              $sessiondata = array(
                            'first_name' => $this->input->post("first_name"),
                            'last_name' => $this->input->post("last_name"),
                            'email' => $this->input->post("email"),
                            'mobile' => $this->input->post("mobile"),
                            'dob' => $this->input->post("dob"),
                            'usi_no' =>$this->input->post("usi_no"),
                            'city_id' => $this->input->post("city"),
                            'state' => $this->input->post("state"),
                            'registration_no' => $this->input->post("registration_no"),
                            'user_type' => $this->input->post("user_type")
                        );
                        $this->session->set_userdata($sessiondata);
            if ($this->form_validation->run() == FALSE) {
                //$this->session->set_flashdata('err_msg', validation_errors());
                load_front_view('registration_form',$arrData);
            } else {
               $fullname=strtolower(trim($this->input->post("first_name"))." ".trim($this->input->post("last_name")));
               $is_valid_code=$this->verify_code($fullname,$this->input->post("registration_code"));
                //$is_valid_code=1; // uncomment the above and comment this line in next version launch
                if($is_valid_code){
                          $Characteres = 'ABCD0123456789EFGHIJKLMOPQRSTUVXWYZ';
                          $Characteres_length = strlen($Characteres);
                          $Characteres_length--;
                          $token=NULL;
                          for($x=1;$x<=4;$x++){
                          $Posicao = rand(0,$Characteres_length);
                          $token .= substr($Characteres,$Posicao,1);
                          }
                          if($this->input->post("dob")==''){
                           $dob="";
                           }else{
                               $dob=date('Y-m-d',strtotime($this->input->post("dob")));
                            }
                    $data = array(
                        'first_name' =>$this->input->post("first_name"),
                        'last_name' =>$this->input->post("last_name"),
                        'email_id' =>$this->input->post("email"),
                        'mobile_no' =>$this->input->post("mobile"),
                        'city_id' =>$this->input->post("city"),
                        'state_id' =>$this->input->post("state"),
                        'registration_no' =>$this->input->post("registration_no"),
                        'usi_no' =>$this->input->post("usi_no"),
                        'registration_code' =>$this->input->post("registration_code"),
                         //'registration_code' =>$login_code,
                        'dob' =>$dob,
                        'password' =>md5(sha1($this->input->post("password"))),
                        'token' =>$token,
                        'user_type' =>$this->input->post("user_type"),
                        'status' =>0,
                        'created_on' => date('Y-m-d')

                    );
                    $sid = $this->user_model->insert_user($data);
                    if($sid){
                         $data12 = array(
                            'email_id' =>$this->input->post("email"),
                            'mobile_no' =>$this->input->post("mobile"),
                        );
                        $this->registrationcode_model->update_registration_code_by_name($fullname,$this->input->post("registration_code"),$data12);

                    }
                    //email verification and active account

                      $to=$this->input->post("email");
                      $subject="Verification email";

                    $url=base_url('index.php/registration/verify_email/'.$sid.'/'.$token);
                    $uname=$this->input->post("first_name")." ".$this->input->post("last_name");
                    $message=active_account_msg($uname,$url);

                    send_email($to,$subject,$message);

                   //Unset session Variable
                    $this->session->unset_userdata('first_name');
                    $this->session->unset_userdata('last_name');
                    $this->session->unset_userdata('email');
                    $this->session->unset_userdata('mobile');
                    $this->session->unset_userdata('state');
                    $this->session->unset_userdata('city_id');
                    $this->session->unset_userdata('registration_no');
                    $this->session->unset_userdata('usi_no');
                    $this->session->unset_userdata('dob');
                    $this->session->unset_userdata('user_type');
                    // end
//                    $this->session->set_flashdata('succ_msg', 'Verification code has been sent to given mobile number.');
//                    redirect('registration/verify_sms_form/'.$sid);
                    $this->session->set_flashdata('succ_msg', 'Activation link has been sent to given email id.');
                    redirect('registration');
                }else{
                    $this->session->set_flashdata('err_msg', 'Invalid Registration Code');
                    redirect('registration');
                }
            }
        }else{
            load_front_view('registration_form',$arrData);
        }
    }

    //check emai id already exsists or not
    public function email_check($email)
    {
        $already_exsist=$this->user_model->check_exsisting_email($email);
        if (!empty($already_exsist))
        {
            $this->form_validation->set_message('email_check', 'Email Id Already Registred.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
//check mobile number
    public function number_check($no)
    {
        if(!preg_match('/^\d{10}$/', $no))
        {
            $this->form_validation->set_message('number_check', 'Please Enter Valid Mobile Number.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    /*
     * verify the registration code
     */
    private function verify_code($fullname,$code)
    {
       $code_verify=$this->registrationcode_model->verify_code($fullname,$code);
       if(!empty($code_verify)){
           return 1;
       }else{
           return 0;
       }
    }

    /*
     * verify the user email
     */
    public function verify_email($user_id,$token)
    {
        $data = array(
            'status' =>1
        );
        $sid = $this->user_model->update_acc_status($user_id,$token,$data);
        if($sid){
            $this->session->set_flashdata('succ_msg', 'Thank you.Now your account is active');
        }else{
            $this->session->set_flashdata('err_msg', 'Sorry !!! Unauthorised Access.');
        }
        redirect('home');
    }
    /*
     * verify the user sms
     */
    public function verify_sms_form($userid)
    {
        $arrData['user_id']=$userid;
        load_front_view('sms_verification',$arrData);
    }
    /*
     * verify the user sms
     */
    public function verify_sms()
    {
        $user_id = $this->input->post("user_id");
        $this->form_validation->set_rules("verification_code", "Verification Code", "trim|required");
        if ($this->form_validation->run() == FALSE) {
           $this->session->set_flashdata('err_msg', validation_errors());
           redirect('registration/verify_sms_form/' . $user_id);
        }else {
            $data = array(
                'status' => 1
            );
            $token = $this->input->post("verification_code");

            $sid = $this->user_model->update_acc_status($user_id, $token, $data);
            if ($sid) {
                $this->session->set_flashdata('succ_msg', 'Thank you.Now your account is active');
                redirect('home');
            } else {
                $this->session->set_flashdata('err_msg', 'Sorry !!! Invalid Verification Code.');
                redirect('registration/verify_sms_form/' . $user_id);
            }
        }

    }

    public function profile()
    {
        $arrData=array();
        $arrData['user_details']=$this->user_model->get_user_records($this->session->userdata('user_id'));
        load_admin_view('user_profile',$arrData);
    }
public function get_cities()
    {
        $state_id=$_POST['id'];
        $arrData=array();
        $arrData['city_list']=$this->city_model->get_city($state_id);
        $this->load->view('front/ajax_city_list',$arrData);
    }
public function clear_session()
{
                    $this->session->unset_userdata('first_name');
                    $this->session->unset_userdata('last_name');
                    $this->session->unset_userdata('email');
                    $this->session->unset_userdata('mobile');
                    $this->session->unset_userdata('state');
                    $this->session->unset_userdata('city_id');
                    $this->session->unset_userdata('registration_no');
                    $this->session->unset_userdata('usi_no');
                    $this->session->unset_userdata('dob');
                    $this->session->unset_userdata('user_type');
                   
}

}