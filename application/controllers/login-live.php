<?php
/**
 * Login
 * User: Mukesh
 * Date: 10/21/2015
 *
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('user_id')){
            redirect('home');
        }
        $this->load->database();
        //load the login model
        $this->load->model('login_model');
        $this->load->model('user_model');
    }

    public function index()
    {
        if (isset($_POST["submit"]) && !empty($_POST["submit"])) {

            //get the posted values
            $username = $this->input->post("email_id");
            $password = $this->input->post("password");

            //set validations
            $this->form_validation->set_rules("email_id", "Email Id", "trim|required");
            $this->form_validation->set_rules("password", "Password", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                //validation fails
                load_front_view('home');
            } else {
                //validation succeeds
                if ($this->input->post('submit') == "Login") {
                    //check if username and password is correct
                    $usr_result = $this->login_model->get_user($username, $password);
if($usr_result){
                    $user_id = $usr_result->user_id;
                    $user_name = $usr_result->first_name." ".$usr_result->last_name;
                    $user_email = $usr_result->email_id;
                    $user_type = $usr_result->user_type;
                    $token = $usr_result->token;

}

                    if (!empty($usr_result)) //active user record is present
                    {
                        $data = array(
                            'user_id' => $user_id,
                            'logged_in_date_time' => date('Y-m-d H:i:s')
                        );
                        //set the session variables
                        $sessiondata = array(
                            'user_id' => $user_id,
                            'user_name' => $user_name,
                            'user_email' => $user_email,
                            'user_type' => $user_type,
                            'loginuser' => TRUE
                        );
                        $this->session->set_userdata($sessiondata);

                        if ($token) {
                            if ($user_type == 'Consultant') {
                                $this->session->set_flashdata('succ_msg', 'Logged In as Consultant');
                                redirect($_SERVER['HTTP_REFERER']);
                            } else {
                                $this->session->set_flashdata('succ_msg', 'Logged In as Post Graduates');
                                redirect($_SERVER['HTTP_REFERER']);
                            }
                        }else{
                            $this->session->set_flashdata('succ_msg', 'Please change your password');
                            redirect('profile/update_password');
                        }
                    } else {
                        $this->session->set_flashdata('err_msg', 'Invalid username and password!');
                        redirect('home');
                    }
                } else {
                    redirect('home');
                }
            }
        }else{
            redirect('home');
        }
    }
    /*
     * forgot password
     */
    public function forgot_password()
    {
        if (isset($_POST["submit"]) && !empty($_POST["submit"])) {
            $this->form_validation->set_rules("email", "Email", "trim|required");
            if ($this->form_validation->run() == FALSE) {
                //$this->session->set_flashdata('err_msg', validation_errors());
                load_front_view('forgot_password');
            }else{
            $user_email=$this->input->post("email");
            $user_detials = $this->user_model->get_details_by_email($user_email);

            if (!empty($user_detials)) {
                $username = ucwords($user_detials->first_name . " " . $user_detials->last_name);
            $Characteres = 'ABCD0123456789EFGHIJKLMOPQRSTUVXWYZ';
            $Characteres_length = strlen($Characteres);
            $Characteres_length--;
            $pwd=NULL;
            for($x=1;$x<=4;$x++){
                $Posicao = rand(0,$Characteres_length);
                $pwd .= substr($Characteres,$Posicao,1);
            }

            $data = array(
                'password' =>md5(sha1($pwd))
            );

            $sid = $this->user_model->update_password($user_email,$data);

            if($sid){
                $to=$user_email;
                $subject="Reset Password";
                $message=reset_password_msg($username,$pwd);
                send_email($to,$subject,$message);
                $this->session->set_flashdata('succ_msg', 'New password has been sent to given email id');
            }else{
            $this->session->set_flashdata('err_msg', 'Sorry !!! Please Contact Administrator.');
            }
            }else{
                $this->session->set_flashdata('err_msg', 'Sorry !!! Record not available.');
            }
            
            redirect('home');
        }}
        else{
            load_front_view('forgot_password');
        }
    }

}