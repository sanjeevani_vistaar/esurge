<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 11:07 AM
 */
class Contact_us extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('admin_roles_model');
        $this->load->model('admin_model');
        //$this->output->enable_profiler(TRUE);
    }

    /*
     * display contact us page
     */
    public function index()
    {
        load_front_view('contact_us');
    }

    /*
     * display contact us page
     */
    public function send_msg()
    {
        $to=reciever_emailid();
        $msg=$this->input->post("msg");
        $name=$this->input->post("name");
        $email=$this->input->post("email");
        $subject="New Request/Query - Esurge";
        $message=contactus_msg($name,$email,$msg);
        send_contact_email($to,$subject,$message);
        $this->session->set_flashdata('succ_msg', 'Thank you for the message. Our support team will reply you shortly');
        redirect('contact_us');
    }
}
