<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/30/2015
 * Time: 10:39 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_contents extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('sitecontents_model');
    }

    public function term_of_use()
    {
        $arraData=array();
        $arraData['term_of_use'] = $this->sitecontents_model->get_term_of_use();
        $this->load->view('front/ajax_term_of_use',$arraData);
    }
    public function privacy_policy()
    {
        $arraData=array();
        $arraData['privacy_policy'] = $this->sitecontents_model->get_privacy_policy();
        $this->load->view('front/ajax_privacy_policy',$arraData);
    }

}