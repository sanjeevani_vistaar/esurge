<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/16/2015
 * Time: 3:56 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Webcast extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('webcast_model');
        $this->load->model('category_model');
        //$this->output->enable_profiler(TRUE);
    }
    public function index()
    {
        redirect('webcast/videos_list');
    }
    /*
     * display index
     */
    public function videos_list()
    {
        $arrData=array();
        $record_per_page = RECORD_PER_PAGE;
        $total_records = $this->webcast_model->get_all_videos();
        $total_records=count($total_records);
        $arrData['ui_pagging'] = zebra_paggination($total_records,$record_per_page);
        $current_page = $this->zebra_pagination->get_page();
        $current_page = $current_page -1;
        $start_index = $current_page * $record_per_page;
        $arrData['all_videos']=$this->webcast_model->get_all_videos_page($start_index,$record_per_page);
        $arrData['categories'] = $this->category_model->get_all_records();
        $arrData['selected_cat'] = 'all';
        load_front_view('webcast',$arrData);
    }
    /*
     * display videos list by category
     */
    public function get_videos($cat_id)
    {
        $arrData=array();
        $record_per_page = RECORD_PER_PAGE;
        $total_records = $this->webcast_model->get_cat_videos($cat_id);
        $total_records=count($total_records);
        $arrData['ui_pagging'] = zebra_paggination($total_records,$record_per_page);
        $current_page = $this->zebra_pagination->get_page();
        $current_page = $current_page -1;
        $start_index = $current_page * $record_per_page;

        $arrData['all_videos']=$this->webcast_model->get_cat_videos_page($cat_id,$start_index,$record_per_page);
        $arrData['categories'] = $this->category_model->get_all_records();
        $arrData['selected_cat'] = $cat_id;
        load_front_view('webcast',$arrData);
    }
    /*
     * display videos list by category
     */
    public function get_emb_code()
    {
        $vid_id=$_POST['id'];
        $emb_code = $this->webcast_model->get_video_emb_code($vid_id);
        echo $emb_code->vid_emb_code;
    }
    /*
     * display videos list by category
     */
    public function get_url()
    {
        $vid_url=$_POST['id'];
        $vid_id=substr($vid_url, -11);
        $src="https://www.youtube.com/embed/".$vid_id."?autoplay=1";
       echo "<iframe width='598' height='315' src='".$src."' frameborder='0' allowfullscreen></iframe>";
    }
}