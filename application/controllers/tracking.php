<?php
/**
 * Login
 * User: Mukesh
 * Date: 10/21/2015
 *
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tracking extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_id')){
            $this->session->set_flashdata('err_msg', 'Sorry !!! Unauthorised Access');
            redirect('home');
        }
        $this->load->database();

    }

    public function index(){
       $data = array(
            'user_id' => $this->session->userdata('user_id'),
            'view_page' => $_POST['page_view'],
            'page_details' => $_POST['page_url'],
            'view_date' => date('Y-m-d'),
            'view_time' => date('H:i:s'),
        );
        $logid = $this->tracking_model->insert_view_details($data);
    }
}