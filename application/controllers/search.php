<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 11:07 AM
 */
class Search extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('search_model');
        //$this->output->enable_profiler(TRUE);
    }

    /*
     * display contact us page
     */
    public function index1()
    {
        load_front_view('search');
    }
     /*
     * display results
     */
    public function index()
    {
        
        if (isset($_POST["submit"])) {
            $tags=strtolower($_POST['tags']);
            $sessiondata = array(
                            'tags' => $tags
                            );
            $this->session->set_userdata($sessiondata);
        }else
        {
            $tags=strtolower($this->session->userdata('tags'));
        }

        $arrData=array();
        $arrData['news']=$this->search_model->search_records('news',$tags);
        $arrData['videos']=$this->search_model->search_records('videos',$tags);
        $arrData['pg_icon']=$this->search_model->search_records('webcast',$tags);
        $arrData['uros']=$this->search_model->search_records('uro',$tags);
        $arrData['tags']=$tags;
        load_front_view('search_results',$arrData);
    }
}
