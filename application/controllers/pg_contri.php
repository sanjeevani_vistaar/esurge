<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 11:07 AM
 */
class Pg_contri extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        //$this->output->enable_profiler(TRUE);
    }

    /*
     * display contact us page
     */
    public function index()
    {
        load_front_view('pg_contri');
    }
}
