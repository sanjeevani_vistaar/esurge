<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 10:03 AM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Polls extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('poll_model');
       //$this->output->enable_profiler(TRUE);
//        if ($this->session->userdata('user_id')) {
//            $access_by = access_by('poll');
//            $user_access=$this->session->userdata('user_type');
//            $userArr=explode(',',$access_by);
//            if (!in_array($user_access,$userArr)) {
//                $this->session->set_flashdata('err_msg', 'Sorry!! You do not have access to News Section.Please contact administrator.');
//                redirect('home');
//            }
//        }
    }
    public function index()
    {
        redirect('polls/category');
    }
    /*
     * display category list
     */
    public function category()
    {
        $arrData['poll_cat'] = $this->poll_model->get_all_cat();
        load_front_view('poll_cat',$arrData);
    }
    /*
     * display list
     */
    public function question($catid,$no=0)
    {
        $arrData['polls'] = $this->poll_model->get_all_polls($catid,$no);
        $arrData['cat_details'] = $this->poll_model->get_cat_records($catid);
        $all_polls = $this->poll_model->count_all_polls($catid);
        $arrData['no'] = $no;
        $arrData['total_no'] = $all_polls;
        $arrData['cat_id'] = $catid;
        //load_front_view('puzzle',$arrData);
        $this->load->view('front/polls',$arrData);
    }
    public function save_records()
    {
        $id=$this->input->post('id');
        $cat_id=$this->input->post('cat_id');
        $selectedval=$this->input->post('selected_opt');
        if ($this->session->userdata('user_id')){
            $user=$this->session->userdata('user_id');
        }else{
            $user='guest';
        }
        $data = array(
            'poll_id' => $id,
            'cat_id' => $cat_id,
            'user_id' => $user,
            'selected_option' => $selectedval,
            'played_date' => date('Y-m-d')
        );

        $sid = $this->poll_model->insert_poll_report($data);
    }
}