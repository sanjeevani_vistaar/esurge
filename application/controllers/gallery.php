<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/3/2016
 * Time: 11:07 AM
 */
class Gallery extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('images_model');
        //$this->output->enable_profiler(TRUE);
        if ($this->session->userdata('user_id')) {
            $access_by = access_by('gallery');
            $user_access=$this->session->userdata('user_type');
            $userArr=explode(',',$access_by);
            if (!in_array($user_access,$userArr)) {
                $this->session->set_flashdata('err_msg', 'Sorry!! You do not have access to Videos Section.Please contact administrator.');
                redirect('home');
            }
        }
    }

    public function index()
    {
        redirect('gallery/images_list');
    }
    /*
     * display videos list
     */
    public function images_list()
    {
        $arrData=array();

        $record_per_page = RECORD_PER_PAGE;
        $total_records = $this->images_model->get_all_images();
        $total_records=count($total_records);
        $arrData['ui_pagging'] = zebra_paggination($total_records,$record_per_page);
        $current_page = $this->zebra_pagination->get_page();
        $current_page = $current_page -1;
        $start_index = $current_page * $record_per_page;


        $arrData['all_images']=$this->images_model->get_all_images_page($start_index,$record_per_page);
        load_front_view('gallery',$arrData);
    }
}
