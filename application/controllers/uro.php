<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 1/4/2016
 * Time: 4:33 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Uro extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('uro_model');
        $this->load->model('category_model');
        //$this->output->enable_profiler(TRUE);
        if ($this->session->userdata('user_id')) {
            $access_by = access_by('uro');
            $user_access=$this->session->userdata('user_type');
            $userArr=explode(',',$access_by);
            if (!in_array($user_access,$userArr)) {
                $this->session->set_flashdata('err_msg', 'Sorry!! You do not have access to Uro Slides Section.Please contact administrator.');
                redirect('home');
            }
        }
    }
    public function index()
    {
        redirect('uro/slides_list');
    }
    /*
     * display index
     */
    public function slides_list()
    {
        $arrData=array();
        $record_per_page = RECORD_PER_PAGE;
        $total_records = $this->uro_model->all_records();
        $total_records=count($total_records);
        $arrData['ui_pagging'] = zebra_paggination($total_records,$record_per_page);
        $current_page = $this->zebra_pagination->get_page();
        $current_page = $current_page -1;
        $start_index = $current_page * $record_per_page;

        $arrData['all_uros']=$this->uro_model->all_records_page($start_index,$record_per_page);
        $arrData['categories'] = $this->category_model->get_uros_records();
        $arrData['selected_cat'] = 'all';
        load_front_view('uro',$arrData);
    }
 /*
    * display uro list by category
    */
    public function get_uros($cat_id)
    {
        //$cat_id=$_POST['id'];
        $arrData=array();
        $record_per_page = RECORD_PER_PAGE;
        $total_records = $this->uro_model->get_cat_uros($cat_id);
        $total_records=count($total_records);
        $arrData['ui_pagging'] = zebra_paggination($total_records,$record_per_page);
        $current_page = $this->zebra_pagination->get_page();
        $current_page = $current_page -1;
        $start_index = $current_page * $record_per_page;

        $arrData['all_uros']=$this->uro_model->get_cat_uros_page($cat_id,$start_index,$record_per_page);
        $arrData['categories'] = $this->category_model->get_uros_records();
        $arrData['selected_cat'] = $cat_id;
       // $this->load->view('front/ajax_uros',$arrData);
        load_front_view('uro',$arrData);
    }
}