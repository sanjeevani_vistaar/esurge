<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/14/2015
 * Time: 4:05 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration_code extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('registrationcode_model');
        //$this->output->enable_profiler(TRUE);
    }

   /*
     * display registration code generate form
     */
    public function gererate_registration_code()
    {
        if (isset($_POST["submit"])) {
            //get the posted values
            $fullname = $this->input->post("fullname");
            $email = $this->input->post("email");
            $mobile = $this->input->post("mobile");

            //set validations
            $this->form_validation->set_rules("fullname", "Fullname", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required");
            $this->form_validation->set_rules("mobile", "Mobile", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                //validation fails
                load_admin_view('registration_code/generate_registration_code_form');
            } else {
                $data = array(
                    'full_name' =>$fullname,
                    'email_id' =>$email,
                    'mobile_no' =>$mobile,
                    'registration_code'=>REGISTRATION_CODE_PREFIX,
                    'status' =>1,
                    'created_on' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->registrationcode_model->insert_registration_code($data);
                if($sid){
                    $data = array(
                        'registration_code'=>REGISTRATION_CODE_PREFIX.$sid
                    );
                    $this->registrationcode_model->update_registration_code($sid,$data);
                }
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('index.php/admin/registration_code/codes_list');
            }
        }else{
            load_admin_view('registration_code/generate_registration_code_form');
        }
    }

    /*
     * Display all registration codes
     */
    public function codes_list()
    {
        $all_records = $this->registrationcode_model->get_all_regisration_codes();
        load_admin_view('registration_code/registration_codes_list',array('arrData' => $all_records));
    }


}