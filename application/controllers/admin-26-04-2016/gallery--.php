<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/21/2015
 * Time: 10:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller
{
    private $videos_directory = "uploads/gallery/videos";
    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        } elseif ($this->session->userdata('admintype') == 2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('images_model');
        $this->load->model('videos_model');
        $this->load->model('category_model');
        //$this->output->enable_profiler(TRUE);
    }

    /*
     * display upload image form and save
     */
    public function upload_image()
    {
        $arrData = array();
        $arrData['categories'] = $this->category_model->get_all_records();
        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("desc", "Description", "trim|required");
            $this->form_validation->set_rules("category", "Category", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                //validation fails
                load_admin_view('gallery/upload_img_form', $arrData);
            } else {
                if ($_FILES['img_path']['name']) {

                    $new_file_name = "surge_img-" . time();

                    $config['upload_path'] = './uploads/gallery/images/'; /* NB! create this dir! */
                    $config['allowed_types'] = 'gif|jpg|jpeg|png';/* Passing the extension to be upload */
                    $config['file_name'] = $new_file_name;
                    //Loading library for uploading a file with configuration setting
                    $this->load->library('upload', $config);


                    //Checking whether file is uploaded
                    if (!$this->upload->do_upload('img_path')) {
                        $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                        redirect('admin/gallery/upload_image');
                    } else {
                        $img_data = $this->upload->data();
                        $img_path = "/uploads/gallery/images/" . $img_data['file_name'];
                    }
                }else{
                    $this->session->set_flashdata('err_msg', 'Please select image');
                    redirect('admin/gallery/upload_image');
                }

                $data = array(
                    'cat_id' => $this->input->post("category"),
                    'img_title' => $this->input->post("title"),
                    'img_desc' => $this->input->post("desc"),
                    'img_path' => $img_path,
                    'tags' => $this->input->post("tags"),
                    'status' => $this->input->post("status"),
                    'created_on' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->images_model->insert_image($data);
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('admin/gallery/images_list');

            }
        } else {
            load_admin_view('gallery/upload_img_form', $arrData);
        }
    }

    /*
      * display edit image form and save
      */
    public function edit_image($id)
    {
        $arrData = array();
        $arrData['image_details'] = $this->images_model->get_image_records($id);
        $arrData['categories'] = $this->category_model->get_all_records();
        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("desc", "Description", "trim|required");
            $this->form_validation->set_rules("category", "Category", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                //validation fails
                load_admin_view('gallery/edit_image_form');
            } else {
                if ($_FILES['img_path']['name']) {

                    $new_file_name = "surge_img-" . time();

                    $config['upload_path'] = './uploads/gallery/images/'; /* NB! create this dir! */
                    $config['allowed_types'] = 'gif|jpg|jpeg|png';/* Passing the extension to be upload */
                    $config['file_name'] = $new_file_name;
                    //Loading library for uploading a file with configuration setting
                    $this->load->library('upload', $config);


                    //Checking whether file is uploaded
                    if (!$this->upload->do_upload('img_path')) {
                        $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                        redirect('admin/gallery/edit_image/' . $id);
                    } else {
                        $img_data = $this->upload->data();
                        $img_path = "/uploads/gallery/images/" . $img_data['file_name'];
                        $data['img_path'] = $img_path;
                    }
                }

                $data['cat_id'] = $this->input->post("category");
                $data['img_title'] = $this->input->post("title");
                $data['img_desc'] = $this->input->post("desc");
                $data['tags'] = $this->input->post("tags");
                $data['status'] = $this->input->post("status");
                $data['modified_on'] = date('Y-m-d');
                $data['modified_by'] = $this->session->userdata('admin_id');

                $sid = $this->images_model->update_image($id, $data);
                $this->session->set_flashdata('succ_msg', 'Record updated Successfully!');
                redirect('admin/gallery/images_list');
            }
        } else {
            load_admin_view('gallery/edit_image_form', $arrData);
        }
    }

    /*
     * Delete image
     */
    public function delete_image($id)
    {
        $data = array(
            'is_deleted' => 1,
            'status' => 0
        );
        $sid = $this->images_model->update_image($id, $data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/gallery/images_list');
    }

    /*
      * display upload video form and save
      */
    public function upload_video()
    {
        $arrData = array();
        $arrData['categories'] = $this->category_model->get_all_records();
        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("desc", "Description", "trim|required");
           // $this->form_validation->set_rules("category", "Category", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                //validation fails
                load_admin_view('gallery/upload_video_form', $arrData);
            } else {
                if ($_FILES['vid_path']['name']) {

                    $new_file_name = "surge_video-" . time();

                    $config['upload_path'] = './uploads/gallery/videos/'; /* NB! create this dir! */
                    $config['allowed_types'] = '*';/* Passing the extension to be upload */
                    $config['file_name'] = $new_file_name;
                    //Loading library for uploading a file with configuration setting
                    $this->load->library('upload', $config);


                    //Checking whether file is uploaded
                    if (!$this->upload->do_upload('vid_path')) {
                        $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                        redirect('admin/gallery/upload_video');
                    } else {
                        $vid_data = $this->upload->data();
                        $vid_path = "/uploads/gallery/videos/" . $vid_data['file_name'];
                    }
                }else{
                    $this->session->set_flashdata('err_msg', 'Please select Video');
                    redirect('admin/gallery/upload_video');
                }
                if ($_FILES['img_path']['name']) {

                    $new_file_name = "surge_video-" . time();

                    $config1['upload_path'] = './uploads/gallery/videos/images'; /* NB! create this dir! */
                    $config1['allowed_types'] = 'gif|jpg|jpeg|png';/* Passing the extension to be upload */
                    $config1['file_name'] = $new_file_name;
                    //Loading library for uploading a file with configuration setting
                    $this->load->library('upload', $config1);
 $this->upload->initialize($config1);


                    //Checking whether file is uploaded
                    if (!$this->upload->do_upload('img_path')) {
                        $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                        redirect('admin/gallery/upload_video');
                    } else {
                        $img_data = $this->upload->data();
                        $img_path = "/uploads/gallery/videos/images/" . $img_data['file_name'];
                    }
                }else{
                    $this->session->set_flashdata('err_msg', 'Please select image');
                    redirect('admin/gallery/upload_video');
                }

                $data = array(
                    'cat_id' => implode(',',$this->input->post("category")),
                    'vid_title' => $this->input->post("title"),
                    'vid_desc' => $this->input->post("desc"),
                    'vid_path' => $vid_path,
                    'img_path' => $img_path,
                    'tags' => $this->input->post("tags"),
                    'status' => $this->input->post("status"),
                    'created_on' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->videos_model->insert_video($data);
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('admin/gallery/videos_list');

            }
        } else {
            load_admin_view('gallery/upload_video_form', $arrData);
        }
    }


    /*
      * display edit video form and save
      */
    public function edit_video($id)
    {
        $arrData=array();
        $arrData['video_details']=$this->videos_model->get_video_records($id);
        $arrData['categories']=$this->category_model->get_all_records();
        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("desc", "Description", "trim|required");
            //$this->form_validation->set_rules("category", "Category", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                //validation fails
                load_admin_view('gallery/edit_video_form');
            } else {
                if ($_FILES['vid_path']['name']) {

                    $new_file_name = "surge_vid-" . time();

                    $config['upload_path'] = './uploads/gallery/videos/'; /* NB! create this dir! */
                    $config['allowed_types'] = '*';/* Passing the extension to be upload */
                    $config['file_name'] = $new_file_name;
                    //Loading library for uploading a file with configuration setting
                    $this->load->library('upload', $config);


                    //Checking whether file is uploaded
                    if (!$this->upload->do_upload('vid_path')) {
                        $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                        redirect('admin/gallery/edit_video/'.$id);
                    } else {
                        $vid_data = $this->upload->data();
                        $vid_path = "/uploads/gallery/videos/" . $vid_data['file_name'];
                        $data['vid_path']=$vid_path;
                    }
                }
                if ($_FILES['img_path']['name']) {

                    $new_file_name = "surge_video-" . time();

                    $config1['upload_path'] = './uploads/gallery/videos/images'; /* NB! create this dir! */
                    $config1['allowed_types'] = 'gif|jpg|jpeg|png';/* Passing the extension to be upload */
                    $config1['file_name'] = $new_file_name;
                    //Loading library for uploading a file with configuration setting
                    $this->load->library('upload', $config1);
 $this->upload->initialize($config1);


                    //Checking whether file is uploaded
                    if (!$this->upload->do_upload('img_path')) {
                        $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                        redirect('admin/gallery/upload_video');
                    } else {
                        $img_data = $this->upload->data();
                        $img_path = "/uploads/gallery/videos/images/" . $img_data['file_name'];
                        $data['img_path'] = $img_path;
                    }
                }

                $data['cat_id']=implode(',',$this->input->post("category"));
                $data['vid_title']=$this->input->post("title");
                $data['vid_desc']=$this->input->post("desc");
                $data['tags']=$this->input->post("tags");
                $data['status']=$this->input->post("status");
                $data['modified_on']=date('Y-m-d');
                $data['modified_by']=$this->session->userdata('admin_id');

                $sid = $this->videos_model->update_video($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record updated Successfully!');
                redirect('admin/gallery/videos_list');
            }
        }else{
            load_admin_view('gallery/edit_video_form',$arrData);
        }
    }

    /*
     * Delete video
     */
    public function delete_video($id)
    {
        $data = array(
            'is_deleted' =>1,
            'status' =>0
        );
        $sid = $this->videos_model->update_video($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/gallery/videos_list');
    }

    /*
    * Display all categories
    */
    public function categories_list()
    {
        $all_records = $this->category_model->get_all_records();
        load_admin_view('gallery/categories_list',array('arrData' => $all_records));
    }

    /*
     * Display all Images
     */
    public function images_list()
    {
        $all_records = $this->images_model->get_all_images();
        load_admin_view('gallery/images_list',array('arrData' => $all_records));
    }

    /*
    * Display all videos
    */
    public function videos_list()
    {
        $all_records = $this->videos_model->get_all_videos();
        load_admin_view('gallery/videos_list',array('arrData' => $all_records));
    }

    /*
      * display create category form and save
      */
    public function create_category()
    {

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("name", "Category name", "trim|required");


            if ($this->form_validation->run() == FALSE) {
                //validation fails
                load_admin_view('gallery/create_category_form');
            } else {
                $data = array(
                    'cat_name' =>$this->input->post("name"),
                    'created_on' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->category_model->insert_category($data);
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('admin/gallery/categories_list');
            }
        }else{
            load_admin_view('gallery/create_category_form');
        }
    }

    /*
      * display edit category form and save
      */
    public function edit_category($id)
    {
        $arrData=array();
        $arrData['cat_details']=$this->category_model->get_cat_records($id);

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("name", "Category name", "trim|required");


            if ($this->form_validation->run() == FALSE) {
                //validation fails
                load_admin_view('gallery/edit_category_form',$arrData);
            } else {
                $data = array(
                    'cat_name' =>$this->input->post("name"),
                    'modified_on' => date('Y-m-d'),
                    'modified_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->category_model->update_category($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                redirect('admin/gallery/categories_list');
            }
        }else{
            load_admin_view('gallery/edit_category_form',$arrData);
        }
    }

    /*
     * Delete category
     */
    public function delete_category($id)
    {
        $data = array(
            'is_deleted' =>1
        );
        $sid = $this->category_model->update_category($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/gallery/categories_list');
    }
    /*
     * browse video from server folder
     *
     */
    public function browse_videos(){
        $arrData = array();
        $files = scandir($this->videos_directory,1);
        $files = array_diff($files,array('.','..'));
        foreach($files as $file){
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $video_arr=array('mp4');
            if(in_array($ext,$video_arr))
            {
                $arrData['videos_list'][$file] = filemtime($this->videos_directory.'/'.$file);
            }
        }
        $arrData['dir_path'] = $this->videos_directory;
        $this->load->view('admin/gallery/browse_videos',$arrData);
    }

    /*
     * save image path in session from server folder
     */
    public function session_path()
    {
        $path=$_POST['path'];
        $sessionVidData = array(
            'video_path' => $path,
        );
        $this->session->set_userdata($sessionVidData);
    }

}