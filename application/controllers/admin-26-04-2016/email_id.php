<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_id extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('emailid_model');
        //$this->output->enable_profiler(TRUE);
    }
     /*
      * display create emailid form and save
      */
    public function add_emailid()
    {
        $this->session->set_flashdata('err_msg', 'You can not add new email id.');
        redirect('admin/email_id/all_list');
        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("emailid", "Email", "trim|required");


            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/email_id/add_emailid');
            } else {
                $data = array(
                    'email_id' =>$this->input->post("emailid"),
                    'type' =>$this->input->post("type"),
                    'created_on' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->emailid_model->insert_emailid($data);
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('admin/email_id/all_list');
            }
        }else{
            load_admin_view('email_id/create_email_form');
        }
    }

    /*
      * display edit emailid form and save
      */
    public function edit_emailid($id)
    {
        $arrData=array();
        $arrData['emailid_details']=$this->emailid_model->get_emailid_records($id);

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("emailid", "Email Id", "trim|required");


            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/email_id/edit_emailid/'.$id,$arrData);
            } else {
                $data = array(
                    'email_id' =>$this->input->post("emailid"),
                    'modified_on' => date('Y-m-d'),
                    'modified_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->emailid_model->update_emailid($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                redirect('admin/email_id/all_list');
            }
        }else{
            load_admin_view('email_id/edit_email_form',$arrData);
        }
    }

    /*
     * Delete emailid
     */
    public function delete_emailid($id)
    {
        $this->session->set_flashdata('err_msg', 'You can not delete any email id.');
        redirect('admin/email_id/all_list');
        $data = array(
            'is_deleted' =>1
        );
        $sid = $this->emailid_model->update_emailid($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/email_id/all_list');
    }

    /*
     * Display all emailids
     */
    public function all_list()
    {
        $all_records = $this->emailid_model->get_all_records();
        load_admin_view('email_id/email_list',array('arrData' => $all_records));
    }

}