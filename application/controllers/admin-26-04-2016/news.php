<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('news_model');
        //$this->output->enable_profiler(TRUE);
    }
     /*
      * display create news form and save
      */
    public function create_news()
    {

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("title", "Title", "trim|required");
           $this->form_validation->set_rules("desc", "Brief Description", "trim|required");
            $this->form_validation->set_rules("short_desc", "Short Description", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            $sessiondata = array(
                'title' => $this->input->post("title"),
                'desc' => $this->input->post("desc"),
                'short_desc' => $this->input->post("short_desc"),
                'link' => $this->input->post("link"),
                'tags' => $this->input->post("tags"),
                'status' => $this->input->post("status")
            );
            $this->session->set_userdata($sessiondata);

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                load_admin_view('news/create_news_form');
            } else {
                if ($_FILES['img_path']['name']) {
                    $ext = pathinfo($_FILES['img_path']['name'], PATHINFO_EXTENSION);
                    $allowed_ext=array('jpg','jpeg','png');
                    if(!in_array($ext,$allowed_ext))
                    {
                        $this->session->set_flashdata('err_msg', 'Selected File extension not allowed !');
                        redirect('admin/news/create_news');
                    }
                    $image_info = getimagesize($_FILES["img_path"]["tmp_name"]);
                    $image_width = $image_info[0];
                    $image_height = $image_info[1];
                   if($image_width != 284 && $image_height !=148){
                       $this->session->set_flashdata('err_msg', 'Uploaded file does not fit into the allowed dimensions !');
                       redirect('admin/news/create_news');
                   }
                    $new_file_name = "news-" . time();

                    $config['upload_path'] = './uploads/news/'; /* NB! create this dir! */
                    $config['allowed_types'] = '*';/* Passing the extension to be upload */
                    $config['file_name'] = $new_file_name;
                    $config['max_width']  = '284';
                    $config['max_height']  = '148';
                    //Loading library for uploading a file with configuration setting
                    $this->load->library('upload', $config);

                    //Checking whether file is uploaded
                    if (!$this->upload->do_upload('img_path')) {
                        $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                        redirect('admin/news/create_news');
                    } else {
                        $img_data = $this->upload->data();
                        $img_path = "/uploads/news/" . $img_data['file_name'];

                    }
                }
                $data = array(
                    'news_title' => $this->input->post("title"),
                    'short_desc' => $this->input->post("short_desc"),
                    'news_desc' => $this->input->post("desc"),
                    'tags' => $this->input->post("tags"),
                    'news_img_path' => $img_path,
                    'status' => $this->input->post("status"),
                    'created_on' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('admin_id')
                );

                $sid = $this->news_model->insert_news($data);
                if ($sid){
                    $this->clear_session();
                    $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
            }else{
                $this->session->set_flashdata('err_msg', 'Unable to save !');
            }
                    redirect('admin/news/all_list');

            }
        }else{
            load_admin_view('news/create_news_form');
        }
    }

    /*
      * display edit news form and save
      */
    public function edit_news($id)
    {
        $arrData=array();
        $arrData['news_details']=$this->news_model->get_news_records($id);

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("desc", "Brief Description", "trim|required");
            $this->form_validation->set_rules("short_desc", "Short Description", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/news/edit_news/'.$id);
            } else {
                if ($_FILES['img_path']['name']) {
                    $ext = pathinfo($_FILES['img_path']['name'], PATHINFO_EXTENSION);
                    $allowed_ext=array('jpg','jpeg','png');
                    if(!in_array($ext,$allowed_ext))
                    {
                        $this->session->set_flashdata('err_msg', 'Selected File extension not allowed !');
                        redirect('admin/news/edit_news/'.$id);
                    }
                    $image_info = getimagesize($_FILES["img_path"]["tmp_name"]);
                    $image_width = $image_info[0];
                    $image_height = $image_info[1];
                    if($image_width != 284 && $image_height !=148){
                        $this->session->set_flashdata('err_msg', 'Uploaded file does not fit into the allowed dimensions !');
                        redirect('admin/news/create_news');
                    }
                    $new_file_name = "news-" . time();

                    $config['upload_path'] = './uploads/news/'; /* NB! create this dir! */
                    $config['allowed_types'] = '*';/* Passing the extension to be upload */
                    $config['file_name'] = $new_file_name;
                    $config['max_width']  = '284';
                    $config['max_height']  = '148';
                    //Loading library for uploading a file with configuration setting
                    $this->load->library('upload', $config);


                    //Checking whether file is uploaded
                    if (!$this->upload->do_upload('img_path')) {
                        $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                        redirect('admin/news/edit_news/'.$id);
                    } else {
                        $img_data = $this->upload->data();
                        $img_path = "/uploads/news/" . $img_data['file_name'];
                        $data['news_img_path']=$img_path;
                    }
                }

                $data['news_title']=$this->input->post("title");
                $data['short_desc']=$this->input->post("short_desc");
                $data['news_desc']=$this->input->post("desc");
                $data['tags']=$this->input->post("tags");
                $data['status']=$this->input->post("status");
                $data['modified_on']=date('Y-m-d');
                $data['modified_by']=$this->session->userdata('admin_id');

                $sid = $this->news_model->update_news($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record updated Successfully!');
                redirect('admin/news/all_list');
            }
        }else{
            load_admin_view('news/edit_news_form',$arrData);
        }
    }

    /*
     * Delete news
     */
    public function delete_news($id)
    {
        $data = array(
            'is_deleted' =>1,
            'status' =>0
        );
        $sid = $this->news_model->update_news($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/news/all_list');
    }

    /*
     * Display all news
     */
    public function all_list()
    {
        $this->clear_session();
        $all_records = $this->news_model->get_all_news_admin();
        load_admin_view('news/news_list',array('arrData' => $all_records));
    }
    public function clear_session()
    {
        $this->session->unset_userdata('title');
        $this->session->unset_userdata('desc');
        $this->session->unset_userdata('short_desc');
        $this->session->unset_userdata('link');
        $this->session->unset_userdata('status');
        $this->session->unset_userdata('tags');
    }

}