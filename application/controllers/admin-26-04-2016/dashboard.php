<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/14/2015
 * Time: 4:05 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('user_model');
        $this->load->model('admin_model');
        $this->load->model('videos_model');
        $this->load->model('pg_model');
        $this->load->model('uro_model');
        $this->load->model('journals_model');
       //$this->output->enable_profiler(TRUE);
    }

    /*
     * display index
     */
    public function index()
    {
        $arrData['all_users']=$this->user_model->get_all_users();
        $arrData['all_videos']=$this->videos_model->get_all_videos_admin();
        $arrData['all_pg_icon']=$this->pg_model->get_all_videos_admin();
        $arrData['all_uro']=$this->uro_model->get_all_records();
        $arrData['all_journals']=$this->journals_model->get_all_journals_admin();
        load_admin_view('dashboard1',$arrData);
    }


}