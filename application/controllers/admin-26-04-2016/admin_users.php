<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_users extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
        $this->load->model('admin_roles_model');
        $this->load->model('admin_model');
        //$this->output->enable_profiler(TRUE);
    }
     /*
      * display create admin form and save
      */
    public function create_admin()
    {
        $arrData=array();
        $roles=$this->admin_roles_model->get_all_records();
        $arrData['admin_roles']=$roles;
        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("firstname", "First name", "trim|required");
            $this->form_validation->set_rules("lastname", "Last name", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required|callback_email_check");
            $this->form_validation->set_rules("mobile", "Mobile", "trim|required|callback_number_check");
            $this->form_validation->set_rules("password", "Password", "trim|required|matches[conf-password]");
            $this->form_validation->set_rules("conf-password", "Confirm Password", "trim|required");
            $this->form_validation->set_rules("role", "Role", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            $sessiondata = array(
                'fname' => $this->input->post("firstname"),
                'lname' => $this->input->post("lastname"),
                'email' => $this->input->post("email"),
                'mobile' => $this->input->post("mobile"),
                'role' => $this->input->post("role")
            );
            $this->session->set_userdata($sessiondata);

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/admin_users/create_admin');
                //load_admin_view('admin_users/create_admin_form',$arrData);
            } else {
                $data = array(
                    'first_name' =>$this->input->post("firstname"),
                    'last_name' =>$this->input->post("lastname"),
                    'email_id' =>$this->input->post("email"),
                    'password' =>md5(sha1($this->input->post("password"))),
                    'mobile_no' =>$this->input->post("mobile"),
                    'role_id'=>$this->input->post("role"),
                    'status' =>$this->input->post("status"),
                    'created_on' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->admin_model->insert_admin($data);
                $this->clear_session();
                $this->session->set_flashdata('succ_msg', 'Record Added Successfully!');
                redirect('admin/admin_users/all_list');
            }
        }else{
            load_admin_view('admin_users/create_admin_form',$arrData);
        }
    }

    /*
      * display create admin form and save
      */
    public function edit_admin($id)
    {
        $arrData=array();
        $arrData['admin_roles']=$this->admin_roles_model->get_all_records();
        $arrData['admin_details']=$this->admin_model->get_admin_records($id);

        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("firstname", "First name", "trim|required");
            $this->form_validation->set_rules("lastname", "Last name", "trim|required");
            if($arrData['admin_details'][0]->email_id == $this->input->post("email"))
            {
                $this->form_validation->set_rules("email", "Email", "trim|required");
            }else {
                $this->form_validation->set_rules("email", "Email", "trim|required|callback_email_check");
            }
            $this->form_validation->set_rules("mobile", "Mobile", "trim|required|callback_number_check");
            if($this->input->post("password")) {
                $this->form_validation->set_rules("password", "Password", "trim|required|matches[conf-password]");
                $this->form_validation->set_rules("conf-password", "Confirm Password", "trim|required");
            }
            $this->form_validation->set_rules("role", "Role", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/admin_users/edit_admin/'.$id);
                //load_admin_view('admin_users/edit_admin_form',$arrData);
            } else {
                $data=array();
                $data['first_name'] = $this->input->post("firstname");
                $data['last_name'] = $this->input->post("lastname");
                $data['email_id'] = $this->input->post("email");
                $data['mobile_no'] = $this->input->post("mobile");
                $data['role_id'] = $this->input->post("role");
                $data['status'] = $this->input->post("status");
                $data['modified_on'] = date('Y-m-d');
                $data['modified_by'] = $this->session->userdata('admin_id');

                if($this->input->post("password")) {
                    $data['password']=md5(sha1($this->input->post("password")));
                }
                $sid = $this->admin_model->update_admin($id,$data);
                $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                redirect('admin/admin_users/all_list');
            }
        }else{
            load_admin_view('admin_users/edit_admin_form',$arrData);
        }
    }
//check emai id already exsists or not
    public function email_check($email)
    {
        $already_exsist=$this->admin_model->check_exsisting_email($email);
        if (!empty($already_exsist))
        {
            $this->form_validation->set_message('email_check', 'Email Id Already Registred.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
//check mobile number
    public function number_check($no)
    {
        if(!preg_match('/^\d{10}$/', $no))
        {
            $this->form_validation->set_message('number_check', 'Please Enter Valid Mobile Number.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    /*
     * Delete admin
     */
    public function delete_admin($id)
    {
        $data = array(
            'is_deleted' =>1,
            'status' =>0
        );
        $sid = $this->admin_model->update_admin($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/admin_users/all_list');
    }

    /*
     * Display all Admin users
     */
    public function all_list()
    {
        $this->clear_session();
        $all_records = $this->admin_model->get_all_admin();
        load_admin_view('admin_users/admin_list',array('arrData' => $all_records));
    }

    /*
     * Display Admin users profile
     */
    public function update_profile()
    {
        $arrData=array();
        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("firstname", "First name", "trim|required");
            $this->form_validation->set_rules("lastname", "Last name", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required");
            $this->form_validation->set_rules("mobile", "Mobile", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/admin_users/update_profile/',$arrData);
            } else {
                $data = array(
                    'first_name' =>$this->input->post("firstname"),
                    'last_name' =>$this->input->post("lastname"),
                    'email_id' =>$this->input->post("email"),
                    'mobile_no' =>$this->input->post("mobile"),
                    'modified_on' => date('Y-m-d'),
                    'modified_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->admin_model->update_admin($this->session->userdata('admin_id'),$data);
                $this->session->set_flashdata('succ_msg', 'Record Updated Successfully!');
                redirect('admin/admin_users/update_profile/');
            }
        }else {
            $arrData['admin_details'] = $this->admin_model->get_admin_records($this->session->userdata('admin_id'));
            load_admin_view('admin_users/admin_profile', $arrData);
        }
    }
     /*
     * update password
     */
    public function update_password()
    {
        $arrData['admin_details'] = $this->admin_model->get_admin_records($this->session->userdata('admin_id'));
        if (isset($_POST["submit"])) {

            //set validations
            $this->form_validation->set_rules("password", "Password", "trim|required|matches[conf-password]");
            $this->form_validation->set_rules("conf-password", "Confirm Password", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('admin/admin_users/update_profile/',$arrData);
            } else {
                $data = array(
                    'password' =>md5(sha1($this->input->post("password"))),
                    'modified_on' => date('Y-m-d'),
                    'modified_by' => $this->session->userdata('admin_id')
                );
                $sid = $this->admin_model->update_admin($this->session->userdata('admin_id'),$data);
                $this->session->set_flashdata('succ_msg1', 'Record Updated Successfully!');
                redirect('admin/admin_users/update_profile/');
            }
        }
    }
    public function clear_session()
    {
        $this->session->unset_userdata('fname');
        $this->session->unset_userdata('lname');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('mobile');
        $this->session->unset_userdata('role');
    }
}