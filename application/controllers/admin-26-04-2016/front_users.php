<?php
/**
 * Created by PhpStorm.
 * User: Mukesh
 * Date: 12/15/2015
 * Time: 10:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Front_users extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // check if user not loggedin then redirect to login page
        if(!$this->session->userdata('admin_id')){
            redirect('admin/login');
        }elseif($this->session->userdata('admintype')==2) {
            echo "Sorry...Unauthorised page view.Please contact Super Admin for access the page";
            exit;
        }
        $this->load->database();
       $this->load->model('user_model');
        $this->load->model('state_model');
        //$this->output->enable_profiler(TRUE);
    }


    /*
     * Delete user
     */
    public function delete_user($id)
    {
        $data = array(
            'is_deleted' =>1,
            'status' =>0
        );
        $sid = $this->user_model->update_user($id,$data);
        $this->session->set_flashdata('succ_msg', 'Record Deleted Successfully!');
        redirect('admin/front_users/all_list');
    }

    /*
     * Display all users
     */
    public function all_list($state_id=NULL)
    {
        $arrData['states']=$this->state_model->get_all_records();
        $arrData['all_records'] = $this->user_model->get_all_users_admin($state_id);
        $arrData['selected_state']=$state_id;
        $arrData['to_date']='';
        $arrData['from_date']='';
        load_admin_view('front_users/users_list',$arrData);
    }
    /*
     * Display all users
     */
    public function all_list_by_date($to_date=NULL,$from_date=NULL)
    {
        $arrData['states']=$this->state_model->get_all_records();
        $arrData['all_records'] = $this->user_model->get_all_users_by_date($to_date,$from_date);
        $arrData['to_date']=$to_date;
        $arrData['from_date']=$from_date;
        load_admin_view('front_users/users_list',$arrData);
    }

    /*
     * Change status
     */
    public function change_status($id)
    {
        $status=$this->user_model->get_user_status($id);
        $old_status=$status->status;
        if($old_status){
            $new_status=0;
        }else{
            $new_status=1;
        }
        $data = array(
            'status' =>$new_status
        );

        $sid = $this->user_model->update_user($id,$data);
        $this->session->set_flashdata('succ_msg', 'Status Changed Successfully!');
        redirect('admin/front_users/all_list');
    }
}