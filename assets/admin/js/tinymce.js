/**
 * Created by Mukesh on 1/20/2016.
 */
$(document).ready(function() {
    //O2k7 skin
    tinyMCE.init({
        // General options

        /*
         content_css : "css/custom_content.css",
         theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px,20px",
         font_size_style_values : "10px,12px,13px,14px,16px,18px,20px",
         */


        mode: "specific_textareas",
        width: "300",
        height: "200",


        editor_selector: "tiny_mce",
        //elements : id,
        theme: "advanced",
        skin: "o2k7",
        skin_variant: "silver",
        plugins: "lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave,jbimages",
        // Theme options
        theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",

        //theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,link,unlink,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,jbimages,cleanup,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,

        plugin_preview_width: "622",
        plugin_preview_height: "1024",


        //theme_advanced_fonts : 'calibri',


        theme_advanced_fonts: "SourceSansProRegular=SourceSansProRegular;" +
        "Andale Mono=andale mono,times;" +

        "Arial=arial,helvetica,sans-serif;" +
        "Arial Black=arial black,avant garde;" +
        "Book Antiqua=book antiqua,palatino;" +
        "Calibri = calibri;" +
        "Comic Sans MS=comic sans ms,sans-serif;" +
        "Courier New=courier new,courier;" +
        "Georgia=georgia,palatino;" +
        "Helvetica=helvetica;" +
        "Impact=impact,chicago;" +
        "Symbol=symbol;" +
        "Tahoma=tahoma,arial,helvetica,sans-serif;" +
        "Terminal=terminal,monaco;" +
        "Times New Roman=times new roman,times;" +
        "Trebuchet MS=trebuchet ms,geneva;" +
        "Verdana=verdana,geneva;" +
        "Webdings=webdings;" +
        "Wingdings=wingdings,zapf dingbats;",


        // Example content CSS (should be your site CSS)
        content_css: "/assets/site/css/style.css",
        // Drop lists for link/image/media/template dialogs
        //template_external_list_url : "lists/template_list.js",
        external_link_list_url: "lists/link_list.js",
        external_image_list_url: "lists/image_list.js",
        media_external_list_url: "lists/media_list.js",

        // Replace values for the template plugin
        template_replace_values: {},


        template_templates: [
            {
                title: "Left Quote",
                src: "/assets/plugins/tiny_mce/templates/lQuote.html",
                description: "Add Quote to the Left side"
            },
            {
                title: "Right Quote",
                src: "/assets/plugins/tiny_mce/templates/RQuote.html",
                description: "Add Quote to the Right side"
            },
            {
                title: "Inline Quote",
                src: "/assets/plugins/tiny_mce/templates/hQuote.html",
                description: "Add Quote inline"
            },
        ]
    });

})

